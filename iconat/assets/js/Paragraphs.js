var Books = function () {
    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true,
            language: $('html').attr('lang'),
            format: 'yyyy-mm-dd',
        });
    }

    var ShowWithControl = function () {

        if ($("#datatable_ajax").length) {

            var grid = new Datatable();


            grid.init({

                src: $("#datatable_ajax"),

                onSuccess: function (grid, response) {

                    // grid:        grid object

                    // response:    json object of server side ajax response

                    // execute some code after table records loaded

                },

                onError: function (grid) {

                    // execute some code on network or other general error

                },

                onDataLoad: function (grid) {

                    // execute some code on ajax data load

                },


                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    "language": {

                        "url": APP_URL_Without_Lang + "/assets/global/plugins/datatables/languages/" + Lang.get("global.currentLang") + ".json"

                    },

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout

                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).

                    // So when dropdowns used the scrollable div should be removed.

                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",


                    // save datatable state(pagination, sort, etc) in cookie.

                    "bStateSave": true,


                    // save custom filters to the state

                    "fnStateSaveParams": function (oSettings, sValue) {

                        $("#datatable_ajax tr.filter .form-control").each(function () {

                            sValue[$(this).attr('name')] = $(this).val();

                        });


                        return sValue;

                    },


                    // read the custom filters from saved state and populate the filter inputs

                    "fnStateLoadParams": function (oSettings, oData) {

                        //Load custom filters

                        $("#datatable_ajax tr.filter .form-control").each(function () {

                            var element = $(this);

                            if (oData[element.attr('name')]) {

                                element.val(oData[element.attr('name')]);

                            }

                        });


                        return true;

                    },


                    "lengthMenu": [

                        [10, 20, 50, 100, 150, -1],

                        [10, 20, 50, 100, 150, "All"] // change per page values here

                    ],

                    "columnDefs": [{

                        "targets": '_all',

                        "searchable": false,

                        className: "text-center"

                    }],

                    "pageLength": 20, // default record count per page

                    "ajax": {

                        "url": APP_URL + "/Manage/Paragraphs/GetData", // ajax source

                    },

                    "ordering": false,

                    "order": [

                        [1, "asc"]

                    ]// set first column as a default sort by asc

                }

            });


            // handle group actionsubmit button click

            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {

                e.preventDefault();

                var action = $(".table-group-action-input", grid.getTableWrapper());

                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                    grid.setAjaxParam("customActionType", "group_action");

                    grid.setAjaxParam("customActionName", action.val());

                    grid.setAjaxParam("id", grid.getSelectedRows());

                    grid.getDataTable().ajax.reload();

                    grid.clearAjaxParams();

                } else if (action.val() == "") {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'Please select an action',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                } else if (grid.getSelectedRowsCount() === 0) {

                    App.alert({

                        type: 'danger',

                        icon: 'warning',

                        message: 'No record selected',

                        container: grid.getTableWrapper(),

                        place: 'prepend'

                    });

                }

            });


            //grid.setAjaxParam("customActionType", "group_action");

            //grid.getDataTable().ajax.reload();

            //grid.clearAjaxParams();
        }


    }

    var Validation = function (FormMod) {

        // for more info visit the official plugin documentation: 

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#DForm');

        if (form1.length) {
            form1.on('submit', function () {
                Worksheets_Details();

            })
            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form1.on('submit', function () {
                    for (var instanceName in CKEDITOR.instances) {
                        CKEDITOR.instances[instanceName].updateElement();
                    }
            })

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {


                    order: {
                        required: true,
                        digits: true
                    },


                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    success1.hide();

                    error1.show();

                    // App.scrollTo(error1, -200);

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    success1.show();

                    error1.hide();


                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr("par1"));

                    data.append('par2', form1.attr("par2"));

                    data.append('lang', $("html").attr("lang"));

                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Manage/Paragraphs/Store",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                            FormMod.showLoading(true);

                        },

                        complete: function () {

                            FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {
                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;

                                //FormMod.close();
                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);
                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);

                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            }

                        }

                    });

                }


            });
            function Worksheets_Details() {

                $('.w_items').each(function (index) {
                    var itt = $(this);
                    var do_rule = false;
                    $('input,select', $(this)).each(function () {
                        if ($(this).val().length != 0) {
                            do_rule = true;
                        }
                    });
                    if (do_rule === true) {
                        $("select[name*='att_id']", itt).rules('add', {
                            "required": true,
                        });
                        $("input[name*='title']", itt).rules('add', {
                            "required": true,
                        });
                        $("input[name*='worksheet_file']", itt).rules('add', {
                            extension: "pdf"
                        });

                    }
                });

            }
        }


    }


    var open_modal = function () {

        $(document).on('click', '.AEObject', function () {
            var el = $(this);
            var FormMod = $.confirm({
                rtl: App.isRTL(),
                escapeKey: 'cancel',
                closeIcon: true,
                content: function () {
                    var self = this;
                    return $.ajax({
                        url: APP_URL + "/Manage/Paragraphs/AE",
                        data: {par1: el.attr('par1'),par2: el.attr('par2')},
                        dataType: 'json',
                        method: 'get'
                    }).done(function (response) {
                        self.setContent(response.content);
                        self.setTitle(response.title);
                    }).fail(function () {
                        self.setTitle(Lang.get("global.error"));
                        self.setContent(Lang.get("global.something_went_error_rep_sent"));
                    }).always(function () {

                    });
                },
                onContentReady: function () {
                    App.initAjax();
                    Validation(FormMod);
                    initPickers();
                    repeater();

                },
                columnClass: 'col-md-12',
                backgroundDismiss: false,
                backgroundDismissAnimation: 'shake',
                buttons: {
                    formSubmit: {
                        text: Lang.get('global.submit'),
                        btnClass: 'btn-blue',
                        action: function () {
                            $('#DForm').submit();
                            return false;
                        }
                    },
                    cancel: {
                        text: Lang.get('global.cancel'),
                        action: function () {

                        }
                    },

                },

            });


        });


    }


    var delete_row = function () {


        $(document).on('click', '.Delete', function () {
            var el = $(this);

            App.DeleteItemAjax(el, "/Manage/Paragraphs/Delete", '#datatable_ajax', '#datatable_ajax');

        });

        $(document).on('click', '.Delete_File', function () {
            var el = $(this);


            var x = $.confirm({
                title: el.attr('Dtitle'),
                content: el.attr('Dcontent'),
                type: 'red',
                buttons: {
                    confirm: {
                        text: Lang.get('global.yes'),
                        btnClass: 'btn-danger',
                        keys: ['enter'],
                        action: function () {
                            $.ajax({
                                type: "POST",
                                url: APP_URL + "/Manage/Paragraphs/DeletePSound",
                                data: {id: el.attr('par1'),par1: $("#DForm").attr('par1')},
                                //async: false,
                                dataType: "JSON",
                                beforeSend: function () {
                                        App.blockUI({
                                            target: "#List_File",
                                            overlayColor: 'none',
                                            cenrerY: true,
                                            animate: true
                                        });
                                },
                                complete: function () {
                                    App.unblockUI("#List_File");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $.alert({
                                        title: Lang.get('global.error'),
                                        content: xhr.status + " - " + thrownError,
                                        type: 'red',
                                        rtl: App.isRTL(),
                                        closeIcon: true,
                                        buttons: {
                                            cancel: {
                                                text: Lang.get('global.ok'),
                                                action: function () {
                                                }
                                            }
                                        }
                                    });
                                },
                                success: function (response) {
                                    if (response !== null && response.hasOwnProperty("Success")) {

                                        var Res = response['Success'];
                                        $.alert({
                                            title: Res.title,
                                            content: Res.content,
                                            autoClose: 'cancel|10000',
                                            type: 'green',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                        $("#List_File").html("");
                                                    }
                                                }
                                            }
                                        });



                                    } else if (response !== null && response.hasOwnProperty("Errors")) {

                                        var Error = response['Errors'];
                                        $.alert({
                                            title: Error.title,
                                            content: Error.content,
                                            type: 'red',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: Lang.get('global.no'),
                        keys: ['esc'],
                        action: function () {
                        }
                    },
                }
            });

        });


    }
    var repeater = function () {

        $('.mt-repeater').each(function () {
            $(this).repeater({
                show: function () {
                    $(this).slideDown();

                    $('.date-picker').datepicker({
                        rtl: App.isRTL(),
                        autoclose: true,
                        language: $('html').attr('lang'),
                        format: 'yyyy-mm-dd',
                    });

                    // $(this).find('.BarCode_Select').each(function (i, obj) {
                    //     BarCode_Select($(obj));
                    // });
                    // $(this).find('.ItemNumber_Select').each(function (i, obj) {
                    //     ItemNumber_Select($(obj));
                    // });
                    // $(this).find('.Item_Select').each(function (i, obj) {
                    //     Item_Select($(obj));
                    // });

                },
                hide: function (deleteElement) {
                    var tt = $(this);
                    var el = $(this).find(".mt-repeater-del-right");
                    if (el.attr('par1')) {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        $.ajax({
                                            type: "POST",
                                            url: APP_URL + "/Manage/Paragraphs/DeleteWorkSheet",
                                            data: {id: el.attr('par1'), par2: el.attr('par2'),par3: el.attr('par3'),lang:$("html").attr("lang")},
                                            //async: false,
                                            dataType: "JSON",
                                            beforeSend: function () {
                                                App.blockUI({
                                                    target: '#DForm',
                                                    overlayColor: 'none',
                                                    cenrerY: true,
                                                    animate: true
                                                });
                                            },
                                            complete: function () {
                                                App.unblockUI('#DForm');
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $.alert({
                                                    title: Lang.Error,
                                                    content: xhr.status + " - " + thrownError,
                                                    type: 'red',
                                                    rtl: App.isRTL(),
                                                    closeIcon: true,
                                                    buttons: {
                                                        cancel: {
                                                            text: Lang.get('global.ok'),
                                                            action: function () {
                                                            }
                                                        }
                                                    }
                                                });
                                            },
                                            success: function (response) {
                                                if (response !== null && response.hasOwnProperty("Success")) {
                                                    tt.slideUp(deleteElement);

                                                    var Res = response['Success'];
                                                    $.alert({
                                                        title: Res.title,
                                                        content: Res.content,
                                                        autoClose: 'cancel|10000',
                                                        type: 'green',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                } else if (response !== null && response.hasOwnProperty("Errors")) {

                                                    var Error = response['Errors'];
                                                    $.alert({
                                                        title: Error.title,
                                                        content: Error.content,
                                                        type: 'red',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                }
                                            }
                                        });
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    } else {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        tt.slideUp(deleteElement);
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    }


                },
                ready: function (setIndexes) {
                    setIndexes;

                }

            });
        });

    };
    return {


        //main function to initiate the module

        init: function () {

            initPickers();
            ShowWithControl();

            open_modal();

            delete_row();

        }


    };


}();


jQuery(document).ready(function () {

    Books.init();

});