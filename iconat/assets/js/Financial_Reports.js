var Financial = function () {


    var withdraw = function () {


        $(document).on('click', '#Withdraw', function () {
            var el = $(this);

            $.confirm({
                title: el.attr('Dtitle'),
                content: el.attr('Dcontent'),
                type: 'orange',
                buttons: {
                    confirm: {
                        text: Lang.get('global.yes'),
                        btnClass: 'btn-green',
                        keys: ['enter'],
                        action: function () {
                            $.ajax({
                                type: "POST",
                                url: APP_URL + "/FinancialReports/Withdraw",
                                data: {},
                                //async: false,
                                dataType: "JSON",
                                beforeSend: function () {
                                    App.blockUI({
                                        target: "#Withdraw",
                                        overlayColor: 'none',
                                        cenrerY: true,
                                        animate: true
                                    });


                                },
                                complete: function () {
                                    App.unblockUI("#Withdraw");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $.alert({
                                        title: Lang.get('global.error'),
                                        content: xhr.status + " - " + thrownError,
                                        type: 'red',
                                        rtl: App.isRTL(),
                                        closeIcon: true,
                                        buttons: {
                                            cancel: {
                                                text: Lang.get('global.ok'),
                                                action: function () {
                                                }
                                            }
                                        }
                                    });
                                },
                                success: function (response) {
                                    if (response !== null && response.hasOwnProperty("Success")) {

                                        var Res = response['Success'];
                                        $.alert({
                                            title: Res.title,
                                            content: Res.content,
                                            autoClose: 'cancel|10000',
                                            type: 'green',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                        location.reload();
                                                    }
                                                }
                                            }
                                        });

                                    } else if (response !== null && response.hasOwnProperty("Errors")) {


                                        var Error = response['Errors'];
                                        $.alert({
                                            title: Error.title,
                                            content: Error.content,
                                            type: 'red',
                                            rtl: App.isRTL(),
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: Lang.get('global.no'),
                        keys: ['esc'],
                        action: function () {
                        }
                    },
                }
            });

        });


    }

    return {


        //main function to initiate the module

        init: function () {


            withdraw();

        }


    };


}();


jQuery(document).ready(function () {

    Financial.init();

});