var GeneralMethods = function () {
    var screen_width = function () {
        document.cookie = "screen_width=" + $(window).width();
    }

    var Qty_Changes = function (el) {
        $(document).on('click', '.qty_changes', function () {

            var el = $(this);

            var I = el.closest(el.parent()).find(".qty");

            var CurrentQty = I.val();

            if (!(Math.floor(CurrentQty) == CurrentQty && $.isNumeric(CurrentQty))) {
                CurrentQty = 0;
            }

            if (el.attr("r") == 'm') {
                CurrentQty--;
            } else if (el.attr("r") == 'p') {
                CurrentQty++;
            }
            I.val(CurrentQty);


            $.ajax({
                type: "POST",
                url: APP_URL + "/Product/ChangeQTY",
                data: {product_id: el.attr('p'), process: el.attr("r")},
                //async: false,
                dataType: "JSON",
                beforeSend: function () {

                    $.blockUI({
                        target: ".cart_changes",
                        message: '<h1>' + Lang.get('global.loading') + '</h1>',
                        overlayColor: 'none',
                        cenrerY: true,
                        animate: true
                    });

                },
                complete: function () {
                    $.unblockUI(".cart_changes");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.alert({
                        title: Lang.get('global.error'),
                        content: xhr.status + " - " + thrownError,
                        type: 'red',
                        rtl: $("html").attr("dir") === 'rtl',
                        closeIcon: true,
                        buttons: {
                            cancel: {
                                text: Lang.get('global.ok'),
                                action: function () {
                                }
                            }
                        }
                    });
                },
                success: function (response) {
                    if (response !== null && response.hasOwnProperty("Success")) {

                        var Resp = response.Success;
                        // FormMod.close();
                        $("#CartMenu").html(Resp.cart_mega);
                        $("#cart_details").html(Resp.cart_details);


                    } else if (response !== null && response.hasOwnProperty("Errors")) {


                        var Error = response['Errors'];
                        $.alert({
                            title: Error.title,
                            content: Error.content,
                            type: 'red',
                            rtl: $("html").attr("dir") == 'rtl',
                            closeIcon: true,
                            buttons: {
                                cancel: {
                                    text: Lang.get('global.ok'),
                                    action: function () {
                                    }
                                }
                            }
                        });

                    }
                }
            });

        });
    }

    var UserValidation = function () {
        var form1 = $('#UserForm');
        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    name: {
                        minlength: 3,
                        required: true,
                    },
                    password: {
                        required: true,
                    },
                    email: {
                        email: true,
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('Ref', form1.attr("Ref"));
                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Users/CreateUser",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                           // FormMod.showLoading(true);
                        },

                        complete: function () {
                           //FormMod.hideLoading(true);
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {




                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                               // FormMod.buttons.formSubmit.hide();
                               //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                               // //FormMod.setColumnClass('col-md-6');
                               // FormMod.setTitle(Resp.title);
                               // FormMod.setContent(Resp.content);


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                //FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                if (Resp.redirect == true) {
                                                    location.reload();
                                                }
                                            }
                                        },
                                    },
                                });


                                //FormMod.buttons.formSubmit.hide();
                                //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                //FormMod.setColumnClass('col-md-6');
                                //FormMod.setTitle(Resp.title);
                                //FormMod.setContent(Resp.content);


                            }

                        }

                    });

                }


            });
        }


    }


    var LoginModalValidation = function (FormMod) {

        var FormLogin = $('#LoginForm');
        if (FormLogin.length) {

            FormLogin.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    password: {
                        required: true,
                    },
                    email: {
                        email: true,
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(FormLogin[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('Ref', FormLogin.attr("Ref"));
                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Users/LoginFromModal",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                            FormMod.showLoading(true);
                        },

                        complete: function () {
                            FormMod.hideLoading(true);
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();


                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                if (Resp.hasOwnProperty("redirect")) {
                                                    location.reload();
                                                }
                                            }
                                        },
                                    },
                                });


                            }

                        }

                    });

                }


            });
        }


    }

    var ShippingInformationFormValidation = function () {
        $('.collapse').on('shown.bs.collapse', function (event) {
            event.stopPropagation();
            $('html, body').stop().animate({
                'scrollTop': $(this).offset().top - 200
            }, 500, 'swing');
        });
        var form1 = $('#ShippingInformationForm');
        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    contact_name: {
                        required: true,
                    },
                    country: {
                        required: true,
                    },
                    street_address: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    region: {
                        required: true,
                    },
                    mobile: {
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Cart/Set_Shipping_Information",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<h1>' + Lang.get('global.loading') + '</h1>',
                                overlayColor: 'none',
                                cenrerY: true,
                                animate: true
                            });
                        },

                        complete: function () {
                            $.unblockUI();
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Error = response.Errors;
                                $.alert({
                                    title: Error.title,
                                    content: Error.content,
                                    type: 'red',
                                    rtl: $("html").attr("dir") == 'rtl',
                                    closeIcon: true,
                                    buttons: {
                                        cancel: {
                                            text: Lang.get('global.ok'),
                                            action: function () {
                                            }
                                        }
                                    }
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'go_to_shipping_method',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-green',
                                            action: function () {
                                                // if(Resp.redirect == true){
                                                //     location.reload();
                                                // }
                                            }
                                        },
                                        go_to_shipping_method: {
                                            text: Resp.go_to_shipping_method,
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                $('#shipping_information_collapse').collapse("hide");
                                                $('#deliverymethods_collapse').collapse("show");


                                            }
                                        },
                                    },
                                });


                            }

                        }

                    });

                }


            });


        }


    }

    var DeliveryMethodsValidation = function () {
        var form1 = $('#DeliveryMethod');
        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    delivery_method: {
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_select_a_delivery_method'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    // var cont = $(element).parent('.input-group');
                    //
                    // if (cont.size() > 0) {
                    //
                    //     cont.after(error);
                    //
                    // } else {
                    //
                    //     element.after(error);
                    //
                    // }

                },


                highlight: function (element) { // hightlight error inputs
                    // $("#PSYPM")
                    //     .addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    // $("#PSYPM").removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    // label
                    //
                    //     .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Cart/SetDeliveryMethod",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<h1>' + Lang.get('global.loading') + '</h1>',
                                overlayColor: 'none',
                                cenrerY: true,
                                animate: true
                            });
                        },

                        complete: function () {
                            $.unblockUI();
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Error = response.Errors;
                                $.alert({
                                    title: Error.title,
                                    content: Error.content,
                                    type: 'red',
                                    rtl: $("html").attr("dir") == 'rtl',
                                    closeIcon: true,
                                    buttons: {
                                        cancel: {
                                            text: Lang.get('global.ok'),
                                            action: function () {
                                            }
                                        }
                                    }
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'go_to_cart_details',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-green',
                                            action: function () {
                                                // if(Resp.redirect == true){
                                                //     location.reload();
                                                // }
                                            }
                                        },
                                        go_to_cart_details: {
                                            text: Resp.go_to_cart_details,
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                $('#deliverymethods_collapse').collapse("hide");
                                                $('#cart_details_collapse').collapse("show");

                                            }
                                        },
                                    },
                                });


                            }

                        }

                    });

                }


            });


        }


    }

    var PaymentMethodValidation = function () {
        var form1 = $('#PaymentMethod');
        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    paymethod: {
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.Please_select_your_payment_method'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    // var cont = $(element).parent('.input-group');
                    //
                    // if (cont.size() > 0) {
                    //
                    //     cont.after(error);
                    //
                    // } else {
                    //
                    //     element.after(error);
                    //
                    // }

                },


                highlight: function (element) { // hightlight error inputs
                    $("#PSYPM")
                        .addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $("#PSYPM").removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Cart/SetPaymentMethod",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                            $.blockUI({
                                message: '<h1>' + Lang.get('global.loading') + '</h1>',
                                overlayColor: 'none',
                                cenrerY: true,
                                animate: true
                            });
                        },

                        complete: function () {
                            $.unblockUI();
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Error = response.Errors;
                                $.alert({
                                    title: Error.title,
                                    content: Error.content,
                                    type: 'red',
                                    rtl: $("html").attr("dir") == 'rtl',
                                    closeIcon: true,
                                    buttons: {
                                        cancel: {
                                            text: Lang.get('global.ok'),
                                            action: function () {
                                            }
                                        }
                                    }
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-green',
                                            action: function () {
                                                // if(Resp.redirect == true){
                                                //     location.reload();
                                                // }
                                            }
                                        },
                                        go_to_next: {
                                            text: Resp.go_to_next,
                                            btnClass: 'btn-blue',
                                            action: function () {
                                                Checkout_Now();
                                            }
                                        },
                                    },
                                });


                            }

                        }

                    });

                }


            });

            $('#PaymentMethod input:radio[name="paymethod"]').change(
                function () {
                    if (this.checked) {
                        var v = this;

                        $.ajax({
                            type: "POST",
                            url: APP_URL + "/CheckOut/GetPaymentForm",
                            data: {method_id: v.value},
                            //async: false,
                            dataType: "JSON",
                            beforeSend: function () {
                                $.blockUI({
                                    message: '<h1>' + Lang.get('global.loading') + '</h1>',
                                    overlayColor: 'none',
                                    cenrerY: true,
                                    animate: true
                                });

                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: xhr.status + " - " + thrownError,
                                    type: 'red',
                                    rtl: $("html").attr("dir") === 'rtl',
                                    closeIcon: true,
                                    buttons: {
                                        cancel: {
                                            text: Lang.get('global.ok'),
                                            action: function () {
                                            }
                                        }
                                    }
                                });
                            },
                            success: function (response) {
                                if (response !== null && response.hasOwnProperty("Success")) {

                                    var Resp = response.Success;

                                    if (Resp.hasOwnProperty("form")) {
                                        $(v).closest($(v).parent()).find(".F").html(Resp.form);
                                    }

                                } else if (response !== null && response.hasOwnProperty("Errors")) {


                                    var Error = response['Errors'];
                                    $.alert({
                                        title: Error.title,
                                        content: Error.content,
                                        type: 'red',
                                        rtl: $("html").attr("dir") == 'rtl',
                                        closeIcon: true,
                                        buttons: {
                                            cancel: {
                                                text: Lang.get('global.ok'),
                                                action: function () {
                                                    window.location.href = Error.Redirect;
                                                }
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                });


        }


    }

    var OpenRegisterModal = function (Ref) {
        var el = $(this);
        var FormMod = $.confirm({
            rtl: $('html').attr('dir') == 'rtl',
            escapeKey: 'cancel',
            theme: 'register',
            closeIcon: true,
            content: function () {
                var self = this;
                return $.ajax({
                    url: APP_URL + "/Users/RegisterModal",
                    data: {Ref: Ref},
                    dataType: 'json',
                    method: 'get'
                }).done(function (response) {
                    if (response.hide_submit == true) {
                        FormMod.buttons.submit.hide();
                    }
                    self.setContent(response.content);
                    self.setTitle(response.title);
                }).fail(function () {
                    self.setTitle(Lang.get("global.error"));
                    self.setContent(Lang.get("global.something_went_error_rep_sent"));
                }).always(function () {

                });
            },
            onContentReady: function () {
                //UserValidation(FormMod);
            },
            columnClass: 'col-md-12',
            backgroundDismiss: false,
            backgroundDismissAnimation: 'shake',
            buttons: {
                submit: {
                    text: Lang.get('global.register'),
                    btnClass: 'btn-green',
                    action: function () {
                        $('#UserForm').submit();
                        return false;
                    }
                },
                cancel: {
                    text: Lang.get('global.close'),
                    btnClass: 'btn-orange',
                    action: function () {

                    }
                },

            },

        });
    }

    var OpenLoginModal = function (Ref) {
        var el = $(this);

        var FormMod = $.confirm({
            rtl: $('html').attr('dir') == 'rtl',
            escapeKey: 'cancel',
            theme: 'register',
            closeIcon: true,
            content: function () {
                var self = this;
                return $.ajax({
                    url: APP_URL + "/Users/LoginModal",
                    data: {Ref: Ref},
                    dataType: 'json',
                    method: 'get'
                }).done(function (response) {
                    if (response.hide_submit == true) {
                        FormMod.buttons.submit.hide();
                    }
                    self.setContent(response.content);
                    self.setTitle(response.title);
                }).fail(function () {
                    self.setTitle(Lang.get("global.error"));
                    self.setContent(Lang.get("global.something_went_error_rep_sent"));
                }).always(function () {

                });
            },
            onContentReady: function () {
                LoginModalValidation(FormMod);
            },
            columnClass: 'col-md-12',
            backgroundDismiss: false,
            backgroundDismissAnimation: 'shake',
            buttons: {
                submit: {
                    text: Lang.get('global.login'),
                    btnClass: 'btn-green',
                    keys: ['enter'],
                    action: function () {
                        $('#LoginForm').submit();
                        return false;
                    }
                },
                cancel: {
                    text: Lang.get('global.close'),
                    btnClass: 'btn-orange',
                    action: function () {

                    }
                },

            },

        });
    }

    var ContinueCheckOut = function () {
        $('#check_as_guest').change(function () {
            $("#register_a_new_membership").prop('checked', false);
        });
        $('#register_a_new_membership').change(function () {
            $("#check_as_guest").prop('checked', false);
        });
        $(document).on('click', '#ContinueCheckOut', function () {
            var el = $(this);


            if ($('#check_as_guest').is(':checked')) {

                $('#account_collapse').collapse("hide");
                $('#shipping_information_collapse').collapse("show");

            }

            else if ($('#register_a_new_membership').is(':checked')) {

                OpenRegisterModal("Checkout");
            }


        });
    }

    var RegisterClick = function () {

        $(document).on('click', '.RegisterC', function () {
            var el = $(this);
            OpenRegisterModal(el.attr("ref"));
        });
    }

    var LoginClick = function () {

        $(document).on('click', '.LoginC', function () {
            var el = $(this);
            OpenLoginModal(el.attr("ref"));
        });
    }


    var DeleteFromMenuCart = function () {
        $(document).on('click', '.DeleteCartItem', function () {
            var el = $(this);
            $.confirm({
                title: el.attr('Dtitle'),
                content: el.attr('Dcontent'),
                type: 'red',
                buttons: {
                    confirm: {
                        text: Lang.get('global.yes'),
                        btnClass: 'btn-danger',
                        keys: ['enter'],
                        action: function () {
                            $.ajax({
                                type: "POST",
                                url: APP_URL + "/Product/DeleteItem",
                                data: {product_id: el.attr('p')},
                                //async: false,
                                dataType: "JSON",
                                beforeSend: function () {

                                    $.blockUI({
                                        target: "#CartMenu",
                                        message: '<h1>' + Lang.get('global.loading') + '</h1>',
                                        overlayColor: 'none',
                                        cenrerY: true,
                                        animate: true
                                    });

                                },
                                complete: function () {
                                    $.unblockUI("#CartMenu");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $.alert({
                                        title: Lang.get('global.error'),
                                        content: xhr.status + " - " + thrownError,
                                        type: 'red',
                                        rtl: $("html").attr("dir") === 'rtl',
                                        closeIcon: true,
                                        buttons: {
                                            cancel: {
                                                text: Lang.get('global.ok'),
                                                action: function () {
                                                }
                                            }
                                        }
                                    });
                                },
                                success: function (response) {
                                    if (response !== null && response.hasOwnProperty("Success")) {

                                        var Resp = response.Success;
                                        // FormMod.close();
                                        $("#CartMenu").html(Resp.cart_mega);
                                        $("#cart_details").html(Resp.cart_details);

                                        //console.log(Resp.cart_mega);
                                        $.alert({
                                            title: Resp.title,
                                            content: Resp.content,
                                            rtl: $("html").attr("dir") == 'rtl',
                                            escapeKey: 'ok',
                                            closeIcon: true,
                                            type: 'green',
                                            buttons: {
                                                ok: {
                                                    text: Lang.get('global.ok'),
                                                    btnClass: 'btn-blue',
                                                    action: function () {

                                                    }
                                                },
                                            },
                                        });

                                    } else if (response !== null && response.hasOwnProperty("Errors")) {


                                        var Error = response['Errors'];
                                        $.alert({
                                            title: Error.title,
                                            content: Error.content,
                                            type: 'red',
                                            rtl: $("html").attr("dir") == 'rtl',
                                            closeIcon: true,
                                            buttons: {
                                                cancel: {
                                                    text: Lang.get('global.ok'),
                                                    action: function () {
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            });
                        }
                    },
                    cancel: {
                        text: Lang.get('global.no'),
                        keys: ['esc'],
                        action: function () {
                        }
                    },
                }
            });
        });
    }

    var Checkout_Now_Click = function () {
        var el = $(this);
        $(document).on('click', '#Checkout_Now', function () {
            Checkout_Now();
        });
    }


    var Checkout_Now = function () {
        $.ajax({
            type: "POST",
            url: APP_URL + "/Cart/CheckOutCart",
            data: {},
            //async: false,
            dataType: "JSON",
            beforeSend: function () {

                $.blockUI({
                    message: '<h1>' + Lang.get('global.loading') + '</h1>',
                    overlayColor: 'none',
                    cenrerY: true,
                    animate: true
                });

            },
            complete: function () {
                $.unblockUI();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.alert({
                    title: Lang.get('global.error'),
                    content: xhr.status + " - " + thrownError,
                    type: 'red',
                    rtl: $("html").attr("dir") === 'rtl',
                    closeIcon: true,
                    buttons: {
                        cancel: {
                            text: Lang.get('global.ok'),
                            action: function () {
                            }
                        }
                    }
                });
            },
            success: function (response) {
                if (response !== null && response.hasOwnProperty("Success")) {

                    var Resp = response.Success;

                    //console.log(Resp.cart_mega);
                    $.alert({
                        title: Resp.title,
                        content: Resp.content,
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'ok',
                        closeIcon: true,
                        type: 'green',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {
                                    window.location.href = Resp.Redirect;
                                }
                            },
                        },
                    });

                } else if (response !== null && response.hasOwnProperty("Errors")) {


                    var Error = response['Errors'];
                    $.alert({
                        title: Error.title,
                        content: Error.content,
                        type: 'red',
                        rtl: $("html").attr("dir") == 'rtl',
                        closeIcon: true,
                        buttons: {
                            cancel: {
                                text: Lang.get('global.ok'),
                                action: function () {
                                }
                            }
                        }
                    });

                } else if (response !== null && response.hasOwnProperty("Notes")) {


                    var Notes = response['Notes'];
                    var contents = "";
                    $.each(Notes, function (key, value) {
                        if (value.hasOwnProperty("content")) {
                            contents += "* " + value.content + "</br>";
                        }
                    });
                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: contents,
                        type: 'red',
                        rtl: $("html").attr("dir") == 'rtl',
                        closeIcon: true,
                        buttons: {
                            cancel: {
                                text: Lang.get('global.ok'),
                                action: function () {
                                }
                            }
                        }
                    });

                }
            }
        });

    }
    var AddToCart = function (FormMod) {

        $(document).one('click', '#AddToCart', function () {

            var CurrentQty = parseInt($('#qty').val());
            var product_id = $(this).attr("p");

            var data = new FormData();
            data.append('qty', CurrentQty);
            data.append('product_id', product_id);

            $.ajax({

                type: "POST",

                url: APP_URL + "/Product/AddToCart",

                data: data,

                //async: false,

                cache: false,

                contentType: false,

                processData: false,

                dataType: "JSON",

                beforeSend: function () {

                    FormMod.showLoading(true);

                },

                complete: function () {

                    FormMod.hideLoading(true);


                },

                error: function (data, xhr, ajaxOptions, thrownError) {

                    //  console.log(data);
                    var response = data.responseJSON;

                    if (response !== undefined && response.hasOwnProperty("errors")) {
                        var errors = response.errors;
                        var eee = '';
                        $.each(errors, function (key, value) {
                            //$('#' + key).parent().addClass('error');
                            eee += ' * ' + value + '</br>';
                        });
                        $.alert({
                            title: Lang.get('global.error'),
                            content: eee,
                            rtl: $("html").attr("dir") == 'rtl',
                            type: 'red',
                            buttons: {
                                ok: {
                                    text: Lang.get('global.ok'),
                                    btnClass: 'btn-blue',
                                    action: function () {

                                    }
                                },
                            },
                        });

                    } else {
                        $.alert({
                            title: Lang.get('global.error'),
                            content: Lang.get('global.something_went_error_rep_sent'),
                            rtl: $("html").attr("dir") == 'rtl',
                            type: 'red',
                        });
                    }

                },


                success: function (response) {

                    if (response !== null && response.hasOwnProperty("Errors")) {
                        var Resp = response.Errors;
                        //FormMod.close();
                        FormMod.buttons.formSubmit.hide();
                        FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                        FormMod.setColumnClass('col-md-6');
                        FormMod.setTitle(Resp.title);
                        FormMod.setContent(Resp.content);


                    } else if (response !== null && response.hasOwnProperty("Success")) {

                        var Resp = response.Success;

                        $("#CartMenu").html(Resp.cart_mega);
                        //console.log(Resp.cart_mega);
                        $.alert({
                            title: Resp.title,
                            content: Resp.content,
                            rtl: $("html").attr("dir") == 'rtl',
                            escapeKey: 'ok',
                            closeIcon: true,
                            type: 'green',
                            buttons: {
                                ok: {
                                    text: Lang.get('global.ok'),
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        FormMod.close();
                                    }
                                },
                            },
                        });


                        // FormMod.buttons.formSubmit.hide();
                        //  FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                        //  FormMod.setColumnClass('col-md-6');
                        //  FormMod.setTitle(Resp.title);
                        //  FormMod.setContent(Resp.content);


                    }

                }

            });

        });
    }
    var Product_Slider = function () {
        /*------ Product Single Slider --------*/
        $(this).find('.sync1, .sync2').each(function () {
            $(this).data('owlCarousel') ? $(this).data('owlCarousel').onResize() : null;
        });
        var sync1 = $(".sync1");
        var sync2 = $(".sync2");
        var navSpeedThumbs = 500;

        var arrow_l_ltr = 'left';
        var arrow_r_ltr = 'right';

        if ($("html").attr('dir') == 'rtl') {
            arrow_l_ltr = 'right';
            arrow_r_ltr = 'left';
        }

        sync2.owlCarousel({
            rtl: $("html").attr('dir') == 'rtl',
            items: 3,
            nav: true,
            navSpeed: navSpeedThumbs,
            responsive: {
                0: {items: 1},
                480: {items: 3}
            },
            responsiveRefreshRate: 200,
            navText: [
                "<i class='fa fa-long-arrow-" + arrow_l_ltr + "'></i> PREV",
                "NEXT <i class='fa fa-long-arrow-" + arrow_r_ltr + "'></i>"
            ]
        });
        sync1.owlCarousel({
            rtl: $("html").attr('dir') == 'rtl',
            items: 1,
            navSpeed: 1000,
            nav: false,
            onChanged: syncPosition,
            responsiveRefreshRate: 200

        });

        function syncPosition(el) {
            var current = this._current;
            sync2
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced");
            center(current);
        }

        sync2.on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).index();
            sync1.trigger("to.owl.carousel", [number, 1000]);
        });

        function center(num) {
            var sync2visible = sync2.find('.owl-item.active').map(function () {
                return $(this).index();
            });
            if ($.inArray(num, sync2visible) === -1) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("to.owl.carousel", [num - sync2visible.length + 2, navSpeedThumbs, true]);
                } else {
                    sync2.trigger("to.owl.carousel", Math.max(0, num - 1));
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("to.owl.carousel", [sync2visible[1], navSpeedThumbs, true]);
            } else if (num === sync2visible[0]) {
                sync2.trigger("to.owl.carousel", [Math.max(0, num - 1), navSpeedThumbs, true]);
            }
        }
    }

    var In_De_Nos = function () {
        $(document).on('click', '.QA', function () {
            var CurrentQty = parseInt($('#qty').val());

            if (!(Math.floor(CurrentQty) == CurrentQty && $.isNumeric(CurrentQty))) {
                CurrentQty = 0;
            }

            if ($(this).attr("r") == 'm') {
                CurrentQty--;
            } else if ($(this).attr("r") == 'p') {
                CurrentQty++;
            }
            $('#qty').val(CurrentQty);

        });
    }

    var OpenProductModal = function () {

        $(document).on('click', '.ViewProduct', function () {

            var el = $(this);
            var FormMod = $.confirm({
                rtl: $('html').attr('dir') == 'rtl',
                escapeKey: 'cancel',
                closeIcon: true,
                content: function () {
                    var self = this;
                    return $.ajax({
                        url: APP_URL + "/Product/ViewModal",
                        data: {p: el.attr('p')},
                        dataType: 'json',
                        method: 'get'
                    }).done(function (response) {
                        self.setContent(response.content);
                        self.setTitle(response.title);
                    }).fail(function () {
                        self.setTitle(Lang.get("global.error"));
                        self.setContent(Lang.get("global.something_went_error_rep_sent"));
                    }).always(function () {

                    });
                },
                onContentReady: function () {
                    //App.initAjax();
                    //Product_Slider(FormMod);
                    Product_Slider();
                    In_De_Nos();
                    AddToCart(FormMod);

                },
                columnClass: 'col-md-12',
                backgroundDismiss: false,
                backgroundDismissAnimation: 'shake',
                buttons: {
                    cancel: {
                        text: Lang.get('global.close'),
                        action: function () {

                        }
                    },

                },

            });


        });


    }

    var LoginValidation = function (FormMod) {
        var form1 = $('.LoginForm');
        if (form1.length) {

            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {},
                rules: {
                    password: {
                        required: true,
                    },
                    username: {
                        email: true,
                        required: true,
                    },
                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function (i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('lang', $("html").attr("lang"));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/User/ProcessLogin",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function () {
                            //FormMod.showLoading(true);
                        },

                        complete: function () {
                            //FormMod.hideLoading(true);
                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function (key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {
                                            }
                                        },
                                    },
                                });


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                location.reload();


                            }

                        }

                    });

                }


            });
        }


    }

    return {


        //main function to initiate the module

        init: function () {

            screen_width();
            Checkout_Now_Click();
            OpenProductModal();
            DeleteFromMenuCart();
            ContinueCheckOut();
            RegisterClick();
            LoginClick();
            LoginValidation();
            Qty_Changes();
            PaymentMethodValidation();
            ShippingInformationFormValidation();
            DeliveryMethodsValidation();

            UserValidation();


        }


    };


}();


jQuery(document).ready(function () {

    GeneralMethods.init();

});