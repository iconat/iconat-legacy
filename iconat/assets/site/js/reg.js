var UserFormRegister = function () {
   

    

    var Validation = function (FormMod) {

        // for more info visit the official plugin documentation: 

        // http://docs.jquery.com/Plugins/Validation


        var form1 = $('#UserForm');

        if (form1.length) {

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            // form1.on('submit', function () {
            //     for (var instanceName in CKEDITOR.instances) {
            //         CKEDITOR.instances[instanceName].updateElement();
            //     }
            // })

           

            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {},

                rules: {



                    'name': {

                        minlength: 1,
                        maxlength: 255,
                        required: true,

                    },
               
                    'email': {

                        required: true,
                        email: true,

                    },
                    'password': {

                        required: true,

                    },
                 


                },


                invalidHandler: function (event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get("global.error"),
                        content: Lang.get("global.please_recheck_fields"),
                        rtl: App.isRTL(),
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function () {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function (error, element) { // render error placement for each input type

                    if (element.parents('.mt-radio-list').size() > 0 || element.parents('.mt-checkbox-list').size() > 0) {
                        if (element.parents('.mt-radio-list').size() > 0) {
                            error.appendTo(element.parents('.mt-radio-list')[0]);
                        }
                        if (element.parents('.mt-checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.mt-checkbox-list')[0]);
                        }
                    } else if (element.parents('.mt-radio-inline').size() > 0 || element.parents('.mt-checkbox-inline').size() > 0) {
                        if (element.parents('.mt-radio-inline').size() > 0) {
                            error.appendTo(element.parents('.mt-radio-inline')[0]);
                        }
                        if (element.parents('.mt-checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.mt-checkbox-inline')[0]);
                        }
                    } else if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) {
                        error.appendTo(element.attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }

                },


                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                },


                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function (label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function (form) {




                    //upload files

                    var data = new FormData(form1[0]);

                    if ($('.file').length) {

                        jQuery.each(jQuery('.file')[0].files, function (i, file) {

                            data.append('file-' + i, file);

                        });

                    }

                    data.append('par1', form1.attr("par1"));

                    data.append('par2', form1.attr("par2"));

                    $.ajax({

                        type: "POST",

                        url: APP_URL + "/Users/CreateUser",

                        data: data,

                        //async: false,

                        cache: false,

                        contentType: false,

                        processData: false,

                        dataType: "JSON",

                        beforeSend: function () {

                            FormMod.showLoading(true);

                        },

                        complete: function () {

                            FormMod.hideLoading(true);


                        },

                        error: function (data, xhr, ajaxOptions, thrownError) {

                            App.ErrorHandel(data, xhr, ajaxOptions, thrownError);

                        },


                        success: function (response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                                // FormMod.buttons.formSubmit.hide();
                                // FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                // FormMod.setColumnClass('col-md-6');
                                // FormMod.setTitle(Resp.title);
                                // FormMod.setContent(Resp.content);

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'cancel',
                                    closeIcon: true,
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: App.isRTL(),
                                    escapeKey: 'cancel',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function () {

                                            }
                                        },
                                    },
                                });


                                FormMod.buttons.formSubmit.hide();
                                FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                FormMod.setColumnClass('col-md-6');
                                FormMod.setTitle(Resp.title);
                                FormMod.setContent(Resp.content);

                                if ($('#datatable_ajax').length) {
                                    $('#datatable_ajax').DataTable().ajax.reload();
                                }

                            }

                        }

                    });

                }


            });
          
          
        }


    }





   

    var repeater = function () {

        $('.mt-repeater').each(function () {
            $(this).repeater({
                show: function () {
                    $(this).slideDown();

                    $('.date-picker').datepicker({
                        rtl: App.isRTL(),
                        autoclose: true,
                        language: $('html').attr('lang'),
                        format: 'yyyy-mm-dd',
                    });

                    // $(this).find('.BarCode_Select').each(function (i, obj) {
                    //     BarCode_Select($(obj));
                    // });
                    // $(this).find('.ItemNumber_Select').each(function (i, obj) {
                    //     ItemNumber_Select($(obj));
                    // });
                    // $(this).find('.Item_Select').each(function (i, obj) {
                    //     Item_Select($(obj));
                    // });

                },
                hide: function (deleteElement) {
                    var tt = $(this);
                    var el = $(this).find(".mt-repeater-del-right");
                    if (el.attr('par1')) {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        $.ajax({
                                            type: "POST",
                                            url: APP_URL + "/Manage/UserFormRegister/DeleteChild",
                                            data: {id: el.attr('par1'), par2: el.attr('par2'),par3: el.attr('par3')},
                                            //async: false,
                                            dataType: "JSON",
                                            beforeSend: function () {
                                                App.blockUI({
                                                    target: '#UserForm',
                                                    overlayColor: 'none',
                                                    cenrerY: true,
                                                    animate: true
                                                });
                                            },
                                            complete: function () {
                                                App.unblockUI('#UserForm');
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $.alert({
                                                    title: Lang.Error,
                                                    content: xhr.status + " - " + thrownError,
                                                    type: 'red',
                                                    rtl: App.isRTL(),
                                                    closeIcon: true,
                                                    buttons: {
                                                        cancel: {
                                                            text: Lang.get('global.ok'),
                                                            action: function () {
                                                            }
                                                        }
                                                    }
                                                });
                                            },
                                            success: function (response) {
                                                if (response !== null && response.hasOwnProperty("Success")) {
                                                    tt.slideUp(deleteElement);

                                                    var Res = response['Success'];
                                                    $.alert({
                                                        title: Res.title,
                                                        content: Res.content,
                                                        autoClose: 'cancel|10000',
                                                        type: 'green',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                } else if (response !== null && response.hasOwnProperty("Errors")) {

                                                    var Error = response['Errors'];
                                                    $.alert({
                                                        title: Error.title,
                                                        content: Error.content,
                                                        type: 'red',
                                                        rtl: App.isRTL(),
                                                        closeIcon: true,
                                                        buttons: {
                                                            cancel: {
                                                                text: Lang.get('global.ok'),
                                                                action: function () {
                                                                }
                                                            }
                                                        }
                                                    });

                                                }
                                            }
                                        });
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    } else {
                        $.confirm({
                            title: el.attr('Dtitle'),
                            content: el.attr('Dcontent'),
                            type: 'red',
                            buttons: {
                                confirm: {
                                    text: el.attr('confirmButton'),
                                    btnClass: 'btn-danger',
                                    keys: ['enter'],
                                    action: function () {
                                        tt.slideUp(deleteElement);
                                    }
                                },
                                cancel: {
                                    text: el.attr('cancelButton'),
                                    keys: ['esc'],
                                    action: function () {
                                    }
                                },
                            }
                        });
                    }


                },
                ready: function (setIndexes) {
                    setIndexes;

                }

            });
        });

    };

    return {


        //main function to initiate the module

        init: function () {

            
         

           
       

        }


    };


}();


jQuery(document).ready(function () {

    UserFormRegister.init();

});