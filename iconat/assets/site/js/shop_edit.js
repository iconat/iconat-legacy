var ShopEditMethods = function() {


    var map;
    var marker;
    var infowindowPhoto = new google.maps.InfoWindow();
    var latPosition;
    var longPosition;


    function initialize() {



        $(document).on('click', '.initializeMarker', function() {
            initializeMarker();
        });


        var latitude = document.getElementById('latitude').value;
        var longitude = document.getElementById('longitude').value;
        var mapOptions = {
            scrollwheel: false,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: new google.maps.LatLng(latitude, longitude)
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);


        var pos = new google.maps.LatLng(latitude, longitude);
        marker = new google.maps.Marker({
            position: pos,
            draggable: true,
            animation: google.maps.Animation.DROP,
            map: map
        });


        google.maps.event.addListener(marker, 'click', function(event) {
            updatePosition();
        });

        google.maps.event.addListener(marker, 'dragend', function(event) {
            updatePosition();
        });

    }

    function initializeMarker() {

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function(position) {

                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                latPosition = position.coords.latitude;
                longPosition = position.coords.longitude;

                marker = new google.maps.Marker({
                    position: pos,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    map: map
                });

                map.setCenter(pos);
                updatePosition();

                google.maps.event.addListener(marker, 'click', function(event) {
                    updatePosition();
                });

                google.maps.event.addListener(marker, 'dragend', function(event) {
                    updatePosition();
                });
            });
        }

    }

    function updatePosition() {

        latPosition = marker.getPosition().lat();
        longPosition = marker.getPosition().lng();

        contentString = '<div id="iwContent">Lat: <span id="latbox">' + latPosition + '</span><br />Lng: <span id="lngbox">' + longPosition + '</span></div>';

        document.getElementById('latitude').value = latPosition;
        document.getElementById('longitude').value = longPosition;

        infowindowPhoto.setContent(contentString);
        infowindowPhoto.open(map, marker);
    }












    var MediaFiles = [];

    var Zone = function() {


        $("div#Dzonea").dropzone({
            url: APP_URL + "/Dropzone/Upload/Product",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            init: function() {
                this.on('error', function(file, errorMessage) {
                    // console.log("error");
                });
                this.on('sending', function(file, xhr, formData) {
                    // console.log("sending");
                });
                this.on('success', function(file, response) {
                    if (response !== null && response.hasOwnProperty("media")) {
                        MediaFiles.push(response.media);
                    }
                });
                this.on('complete', function(file) {
                    // console.log(file);
                });
            },

        });
    }













    $(document).on('click', '#deleteRecord', function() {
        var id = $(this).data("id");
        var pic = $(this);
        $.ajax({
            url: APP_URL + "/Manage/deleteImage/" + id,
            type: 'DELETE',
            data: {
                "id": id,
            },
            success: function() {
                console.log("it Works");



                $.alert({
                    title: 'تمت العملية بنجاح',
                    content: 'تم حدف الصورة',
                    rtl: App.isRTL(),
                    escapeKey: 'ok',
                    closeIcon: true,
                    type: 'green',
                    buttons: {
                        ok: {
                            text: Lang.get('global.ok'),
                            btnClass: 'btn-blue',
                            action: function() {

                                $(pic).parent().parent().remove();
                            }
                        },

                    },
                });


            }
        });

    });













    var ShopEditValidation = function() {
        var form1 = $('#DForm');





        if (form1.length) {
            form1.validate({
                lang: Lang.get('global.currentLang'),
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                messages: {},
                rules: {
                    // name: {
                    //     minlength: 3,
                    //     required: true,
                    // },


                },


                invalidHandler: function(event, validator) { //display error alert on form submit

                    $.alert({
                        title: Lang.get('global.sorry'),
                        content: Lang.get('global.please_recheck_fields'),
                        rtl: $("html").attr("dir") == 'rtl',
                        escapeKey: 'cancel',
                        closeIcon: true,
                        type: 'orange',
                        buttons: {
                            ok: {
                                text: Lang.get('global.ok'),
                                btnClass: 'btn-blue',
                                action: function() {

                                }
                            },
                        },
                    });

                },


                errorPlacement: function(error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },


                highlight: function(element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },


                unhighlight: function(element) { // revert the change done by hightlight

                    $(element)

                    .closest('.form-group').removeClass('has-error'); // set error class to the control group

                },


                success: function(label) {

                    label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                },


                submitHandler: function(form) {

                    var data = new FormData(form1[0]);
                    if ($('.file').length) {
                        jQuery.each(jQuery('.file')[0].files, function(i, file) {
                            data.append('file-' + i, file);
                        });
                    }

                    data.append('par1', form1.attr("par1"));
                    data.append('lang', $("html").attr("lang"));
                    data.append('MediaFiles', JSON.stringify(MediaFiles));

                    $.ajax({
                        type: "POST",
                        url: APP_URL + "/Shop/ShopEdit",
                        data: data,
                        //async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "JSON",
                        beforeSend: function() {
                            // FormMod.showLoading(true);
                            $("#btnSubmit").attr("disabled", true);

                        },

                        complete: function() {
                            //FormMod.hideLoading(true);
                            $("#btnSubmit").attr("disabled", false);
                        },

                        error: function(data, xhr, ajaxOptions, thrownError) {




                            //  console.log(data);
                            var response = data.responseJSON;

                            if (response != undefined && response !== null && response.hasOwnProperty("errors")) {
                                var errors = response.errors;
                                var eee = '';
                                $.each(errors, function(key, value) {
                                    //$('#' + key).parent().addClass('error');
                                    eee += ' * ' + value + '</br>';
                                });
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: eee,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function() {

                                            }
                                        },
                                    },
                                });

                            } else {
                                $.alert({
                                    title: Lang.get('global.error'),
                                    content: Lang.get('global.something_went_error_rep_sent'),
                                    rtl: $("html").attr("dir") == 'rtl',
                                    type: 'red',
                                });
                            }
                        },


                        success: function(response) {

                            if (response !== null && response.hasOwnProperty("Errors")) {
                                var Resp = response.Errors;
                                //FormMod.close();
                                // FormMod.buttons.formSubmit.hide();
                                //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                // //FormMod.setColumnClass('col-md-6');
                                // FormMod.setTitle(Resp.title);
                                // FormMod.setContent(Resp.content);


                            } else if (response !== null && response.hasOwnProperty("Success")) {

                                var Resp = response.Success;
                                //FormMod.close();

                                $.alert({
                                    title: Resp.title,
                                    content: Resp.content,
                                    rtl: $("html").attr("dir") == 'rtl',
                                    escapeKey: 'ok',
                                    closeIcon: true,
                                    type: 'green',
                                    buttons: {
                                        ok: {
                                            text: Lang.get('global.ok'),
                                            btnClass: 'btn-blue',
                                            action: function() {
                                                location.reload();
                                                if (Resp.redirect == true) {
                                                    location.reload();
                                                }
                                            }
                                        },
                                    },
                                });


                                //FormMod.buttons.formSubmit.hide();
                                //FormMod.buttons.cancel.setText(Lang.get('global.ok'));
                                //FormMod.setColumnClass('col-md-6');
                                //FormMod.setTitle(Resp.title);
                                //FormMod.setContent(Resp.content);


                            }

                        }

                    });

                }


            });
        }


    }















    return {


        //main function to initiate the module

        init: function() {


            ShopEditValidation();
            Zone();
            initialize();

        }


    };


}();


jQuery(document).ready(function() {

    ShopEditMethods.init();

});