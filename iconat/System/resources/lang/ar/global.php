<?php

return [

    'currentLang' => 'ar',
    'ok' => 'حسناً',
    'cancel' => 'الغاء الأمر',
    'close' => 'اغلاق',
    'submit' => 'تقديم',
    'save' => 'حفظ',
    'error' => 'خطأ',
    'alert' => 'تنبيه',
    'sorry' => 'عذراً',
    'yes' => 'نعم',
    'no' => 'لا',
    'select' => 'اختر',
    'please_recheck_fields' => 'من فضلك قم بتصحيح الأخطاء الموضحة أسفل الحقول باللون الأحمر',
    'loading' => 'انتظر قليلاً من فضلك...',
    'something_went_error_rep_sent' => 'حدث خطأ ما أثناء محاولة معالجة الطلب، تم ارسال تقرير بالخطأ لفريق البرمجة وسيتم مراجعته في القريب العاجل',
    'NEXT' => 'التالي',
    'PREV' => 'السابق',
    'register' => 'التسجيل الان',
    'Please_select_your_payment_method' => 'اختر طريقة الدفع من فضلك',
    'please_select_a_delivery_method' => 'اختر طريقة التوصيل من فضلك',
    'login' => 'دخول',
];
