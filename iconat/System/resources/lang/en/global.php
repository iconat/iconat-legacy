<?php

return [

    'currentLang' => 'en',
    'ok' => 'Ok',
    'cancel' => 'Cancel',
    'close' => 'Close',
    'submit' => 'Submit',
    'save' => 'Save',
    'error' => 'Error',
    'alert' => 'Alert',
    'sorry' => 'Sorry',
    'yes' => 'Yes',
    'no' => 'No',
    'select' => 'Select',
    'please_recheck_fields' => 'Please correct the errors shown below the fields in red',
    'loading' => 'Loading...',
    'something_went_error_rep_sent' => 'An error occurred while trying to process the request. An error report was sent to the programming team and will be reviewed in the near future...',
    'NEXT' => 'NEXT',
    'PREV' => 'PREV',
    'register' => 'Register',
    'Please_select_your_payment_method' => 'Please select your payment method',
    'please_select_a_delivery_method' => 'Please select a delivery method',
    'login' => 'Login',

];
