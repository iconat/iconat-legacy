@extends('layouts.website', ['title' => trans('general.register'),'main_classes' => 'home-wrap'])
@section('content')








<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('')}}">{{trans("general.home")}}</a></li>
            <li>{{trans('general.create_new_account')}}</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">
    <div class="container-indent">
        <div class="container">
            <h1 class="tt-title-subpages noborder">{{trans('general.create_new_account')}}</h1>
            <div class="tt-login-form">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-10">
                        <div class="tt-item">
                            <h2 class="tt-title">{{trans('general.create_new_account')}}</h2>
                            <div class="form-default">




                                <form id="UserForm" Ref="" novalidate="novalidate">
                                    <div class="form-group">
                                        <label for="loginInputName">{{trans("general.name")}}</label>
                                        <input type="text" name="name" class="form-control" id="loginInputName" placeholder="Enter Name">
                                    </div>

                                    
                                    <div class="form-group">
                                        <label for="loginUserName">{{trans("general.username")}}</label>
                                        <input type="text" name="username" class="form-control" id="loginLastName" placeholder="Enter User Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="loginInputEmail">{{trans("general.email")}}</label>
                                        <input type="text" name="email" class="form-control" id="loginInputEmail" placeholder="Enter E-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="loginInputPassword">{{trans("general.password")}}</label>
                                        <input type="password" name="password" class="form-control" id="loginInputPassword" placeholder="Enter Password">
                                    </div>

                                    <div class="form-group">
                                        <label for="shopCompanyName" class="control-label">COMPANY NAME</label>
                                        <input type="text" class="form-control" name="shopCompanyName" id="shopCompanyName">
                                    </div>
                                    <div class="form-group">
                            <label for="shopCompanyName" class="col-md-3 control-label">{{trans("general.city")}}</label>
                            <div class="col-md-9">
                            <select class="form-control" name="city">

                                <option value=""></option>
                           
                            @foreach(App\Models\Cites::orderBy('id', 'desc')->get() as  $city)
                                <option value="{{$city->id}}" @if(!empty($UserInfo->InfoUser->city) && ($UserInfo->InfoUser->city == $city->id)) selected @endif >{{ $city->Get_Trans(App::getLocale(),'name') }}</option>
                            @endforeach

                             

                            </select>
                        </div>
                        </div>
                                    <div class="form-group">
                                        <label for="shopAddress" class="control-label">{{trans("general.address")}}</label>
                                        <input type="text" class="form-control" name="shopAddress" id="shopAddress">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopTown" class="control-label">TOWN / CITY *</label>
                                        <input type="text" class="form-control" name="shopTown" id="shopTown">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopState" class="control-label">STATE / COUNTY *</label>
                                        <input type="text" class="form-control" name="shopState" id="shopState">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopPostCode" class="control-label">POSTCODE / ZIP *</label>
                                        <input type="text" class="form-control" name="shopPostCode" id="shopPostCode">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopPhone" class="control-label">{{trans("general.phone")}}</label>
                                        <input type="text" class="form-control" name="shopPhone" id="shopPhone">
                                    </div>



                                    <div class="form-group">
                                        <label  class="control-label" style="color: red;">ملاحظة  : بعد انشاء حساب  جديد لن تتمكن من تسجيل  الدخول حتى يقوم الادمن بتفعيل حسابك</label>
                                        
                                    </div>


                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-group">
                                                <button class="btn btn-border" type="submit">{{trans("general.register")}}</button>
                                            </div>
                                        </div>
                                        <div class="col-auto align-self-center">
                                            <div class="form-group">
                                                <ul class="additional-links">
                                                    <li>or <a href="{{url('')}}">{{trans("general.return_to_home")}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </form>






                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('script')
<script src="{{ asset('assets') }}/global/scripts/app.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/login.js" type="text/javascript"></script>

<script src="{{url('')}}/assets/site/js/custom.js"></script>
@endsection


















