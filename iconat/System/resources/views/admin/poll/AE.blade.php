<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("poll.poll_question")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="question" value="{{$CurrentObj->question ?? NULL}}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("poll.start_date")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="start_date" value="{{$CurrentObj->start_date ?? NULL}}" class="form-control date-picker">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("poll.closing_date")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="closing_date" value="{{$CurrentObj->closing_date ?? NULL}}" class="form-control date-picker">
                    </div>
                </div>

                <hr></hr>

                <h4 class="text-center font-blue-steel font-lg sbold">{{trans("poll.poll_options")}}</h4>

                <div class="row" style="padding: 0 15px;">
                    <div class="col-md-4">
                        <div class=" text-center" style="margin-bottom: 0px;">
                            <label>{{trans("poll.option")}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="  text-center" style="margin-bottom: 0px;">
                            <label>{{trans("poll.additional_votes")}}</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="" style="margin-bottom: 5px;">
                        </div>
                    </div>
                </div>

                <div class="mt-repeater" style="margin: 10px 0;">
                    <div data-repeater-list="Options">
                        <?php
                        $Item_View = view('admin.poll.form_repeater');
                        $Item_View->with('Items_Of_Order', $Items_Of_Order);
                        !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                        echo $Item_View;
                        ?>
                        @php

                        foreach ($Items_Of_Order as $Item) {
                            $Item_View = view('admin.poll.form_repeater');
                            $Item_View->with('Items_Of_Order', $Items_Of_Order);
                            $Item_View->with('Item', $Item);
                            echo $Item_View;
                        }
                        @endphp
                    </div>
                    <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-2 col-md-offset-10">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("poll.option_")])}}
                    </button>
                </div>



            </div>
        </form>
    </div>
</div>