<div data-repeater-item class="mt-repeater-item mt-overflow items" style="border-bottom:none;padding-bottom:0;margin-bottom: 0;<?php if (isset($CurrentObj) && count($Items_Of_Order) > 0) { ?>display: none;<?php } ?>">
    <div>
        <div class="col-md-4">
               <input name="option" class="form-control" type="text" value="@isset($Item){{$Item->option}}@endisset" placeholder="{{trans("poll.option")}}" />
        </div>
        <div class="col-md-4">
               <input name="additional" class="form-control" type="text" value="@isset($Item){{$Item->additional}}@endisset" placeholder="{{trans("poll.additional_votes")}}" />
        </div>
        <div class="col-md-4">
           <div class="form-group">
                <?php if(isset($Item)){ ?>
                <input type="text" name="id" value="<?php echo $Item->id; ?>" hidden="" />
                <?php } ?>
                <a href="javascript:;" par1="<?php if(isset($Item)){ echo $Item->id;} ?>" par2="{{trans("poll.option")}}" Dtitle="{{trans("general.alert")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_this_item")}}" confirmButton="{{trans("general.yes")}}" cancelButton="{{trans("general.no")}}" data-repeater-delete class="btn btn-danger btn-block mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline" style="margin-top:0;">
                    <i class="fa fa-close"></i> {{trans("general.delete")}}
                </a>
            </div>
        </div>
    </div>
</div>