<div data-repeater-item class="mt-repeater-item mt-overflow items w_items" style="border-bottom:none;padding-bottom:0;margin-bottom: 0;@if(((empty($Item->id)) && (count($Items_Of_worksheets) > 0))) display: none; @endif">
    <div>

        <div class="col-md-3">
        <div class="form-group">
            <label class="col-md-2 control-label">{{trans("classes.attachment_number")}} </label>
            <div class="col-md-10">
                <select class="form-control" name="att_id">
                    <option value=""></option>
                    @for($x = 1;$x <= 100;$x++)
                    <option value="{{$x}}" @if(!empty($Item->att_id) && ($Item->att_id == $x)) selected @endif>{{$x}}</option>
                    @endfor
                </select>
            </div>
        </div>
        </div>
        <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-2 control-label">{{trans("general.title_of_name",['name' => trans("classes.attached_file")])}} </label>
            <div class="col-md-10">
                <input type="text" name="title" value="{{$Item->title ?? NULL}}" class="form-control">
            </div>
        </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label class="col-md-2 control-label">{{trans("general.file")}} pdf</label>
                <div class="col-md-10">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> </span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_file')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="worksheet_file" value="{{$Item->title ?? NULL}}" accept="application/pdf">
                                </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                        </div>
                    </div>

                    @php
                        if(!empty($Item)){
                            $media = $Item->lastMedia('Worksheet');
                        }
                    @endphp
                    @isset($media)
                        <span class="help-block">{{trans("general.to_view_the_current_file")}} <a class="btn btn-circle btn-xs grey-mint" target="_blank" href="{{url("/WorkSheetPDF/".$Item->id)}}">{{trans("general.click_here")}}</a></span>
                    @endisset
                </div>
            </div>
        </div>


    </div>
    <div>
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                <?php if(isset($Item)){ ?>
                <input type="text" name="id" value="<?php echo $Item->id; ?>" hidden="" />
                <?php } ?>
                <a href="javascript:;" par1="{{$Item->id ?? NULL}}" par2="{{trans("classes.worksheet")}}" par3="worksheet" Dtitle="{{trans("general.alert")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_this_item")}}" confirmButton="{{trans("general.yes")}}" cancelButton="{{trans("general.no")}}" data-repeater-delete class="btn btn-danger btn-block mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline" style="margin-top:0;">
                    <i class="fa fa-close"></i> {{trans("general.delete")}}
                </a>
            </div>
        </div>

    </div>

</div>
<hr></hr>