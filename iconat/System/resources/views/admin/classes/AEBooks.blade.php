<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" par2="{{$par2}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name")}}</label>
                    <div class="col-md-9">
                        <input type="text" name="name" value="{{$CurrentObj->name ?? NULL}}" class="form-control">
                    </div>
                </div>



                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('classes.the_book')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 250px; height: 350px;">
                                <img src="{{$image ?? url('').'/assets/images/250_350.jpg'}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 250px; max-height: 350px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="img"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.for_best_display_to_image_height_must_be_equal_with_width")}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("classes.a_brief_description_of_the_book")}}</label>
                    <div class="col-md-9">
                        <textarea name="brief_description" rows="3" class="form-control">{{$CurrentObj->brief_description ?? NULL}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('classes.the_book')])}}</label>
                    <div class="col-md-9">
                        <textarea name="text" class="form-control ckeditor">{{$text ?? NULL}}</textarea>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>