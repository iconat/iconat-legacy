@extends('admin.app', ['title' => trans('general.settings')])
@section('css')
@endsection
@section('content')

<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>{{ trans('general.settings') }}</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content margin-top-15">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">{{ trans('general.settings') }}</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">{{trans('general.general_settings')}}</a>
                                </li>
                              <!--   <li class="">
                                    <a href="#tab_1_2" data-toggle="tab">{{trans('general.home_page_settings')}}</a>
                                </li> -->
                                <li class="">
                                    <a href="#tab_1_3" data-toggle="tab">{{trans('general.about_us')}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form id="General_Settings" par1="General_Settings" role="form">
                                        @foreach ($AllLangs as $alt)
                                        <div class="form-group">
                                            <label class="control-label">{{trans('general.website_title')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <input dir="{{$alt['dir']}}" type="text" name="website_title_{{$alt['locale']}}" value="{{setting('website_title_'.$alt['locale'] , "")}}" placeholder="{{trans('general.website_title')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}" class="form-control"/>
                                        </div>
                                        @endforeach
                                        @foreach ($AllLangs as $alt)
                                        <div class="form-group">
                                            <label class="control-label">{{trans('general.website_logo')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput" style="display: block;">
                                                <div class="fileinput-new thumbnail" style="width: {{$header_web_logo_width}}px; height: {{$header_web_logo_height}}px;">
                                                    @php
                                                    $Obj = \App\Models\SettingsMediable::where('key','website_logo_'.$alt['locale'])->first();
                                                    $Img = NULL;
                                                    $S = $Obj->lastMedia('website_logo_'.$alt['locale']."_thumb");
                                                    if($Obj && $S){
                                                    $Img = $S->getUrl();
                                                }
                                                @endphp
                                                <img src="{{$Img ?? url(setting('default_image_logo' , ""))}}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$header_web_logo_width}}px; max-height: {{$header_web_logo_height}}px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                                    <input type="file" name="website_logo_{{$alt['locale']}}" class="file"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                                                </div>
                                            </div>
                                            <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $header_web_logo_width,'height' => $header_web_logo_height])}}</span>
                                        </div>
                                        @endforeach
                                        <hr>
                                           @foreach ($AllLangs as $alt)


                                        <div class="form-group">
                                            <label class="control-label">{{trans('iconat.about_iconat')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <textarea dir="{{$alt['dir']}}" name="about_iconat_{{$alt['locale']}}" placeholder="{{trans('iconat.about_iconat')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}" class="form-control " rows="6" />{{setting('about_iconat_'.$alt['locale'] , "")}}</textarea>
                                        </div>

                                        @if (!$loop->last)
                                        <hr>
                                        @endif

                                        @endforeach

                                          @foreach ($AllLangs as $alt)
                                        <div class="form-group">
                                            <label class="control-label">{{trans('general.address')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <input dir="{{$alt['dir']}}" type="text" name="address_{{$alt['locale']}}" value="{{setting('address_'.$alt['locale'] , "")}}" placeholder="{{trans('general.address')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}" class="form-control"/>
                                        </div>
                                        @endforeach

                                                <div class="form-group">
                                                    <label class="control-label">{{trans('general.phone')}}</label>
                                                    <input type="text" name="general_phone" class="form-control" value="{{setting('general_phone' , "")}}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('general.email')}}</label>
                                                    <input type="text" name="general_email" class="form-control" value="{{setting('general_email' , "")}}">
                                                </div>

                                                <hr>
                                                @php $Socail_Array = ['facebook','twitter','google-plus','instagram','snapchat']; @endphp
                                                @foreach($Socail_Array as $Social)
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('general.'.$Social)}}</label>
                                                    <input dir="ltr" type="text" name="{{$Social}}" class="form-control" value="{{setting($Social , "")}}">
                                                </div>
                                                @endforeach
                                                <hr>




                                                @php $Map_Array = ['latitude','longitude']; @endphp
                                                @foreach($Map_Array as $Map)
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('general.'.$Map)}}</label>
                                                    <input dir="ltr" type="text" name="{{$Map}}" class="form-control" value="{{setting($Map , "")}}">
                                                </div>
                                                @endforeach
                                                <hr>



                                                @php $a = ['duration']; @endphp
                                                @foreach($a as $b)
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('general.'.$b)}}</label>
                                                    <input dir="ltr" type="text" name="{{$b}}" class="form-control" value="{{setting($b , "")}}">
                                                </div>
                                                @endforeach
                                                <hr>




                                                <div class="margiv-top-10">
                                                    <button type="submit" class="btn green"> {{trans('general.save_changes')}} </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="tab_1_2">
                                            <form id="HomePage_Settings" par1="HomePage_Settings" role="form">
                                                @foreach ($AllLangs as $alt)
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('products.image_of_the_main_categories')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="display: block;">
                                                        <div class="fileinput-new thumbnail" style="width: 150px; height: 90px;">
                                                            @php
                                                            $Obj = \App\Models\SettingsMediable::where('key','image_1_of_the_main_categories_'.$alt['locale'])->first();
                                                            $Img = NULL;
                                                            if($Obj){
                                                            $S = $Obj->lastMedia('image_1_of_the_main_categories_'.$alt['locale']."_thumb");
                                                        }
                                                        if($Obj && $S){
                                                        $Img = $S->getUrl();
                                                    }
                                                    @endphp
                                                    <img src="{{$Img ?? "https://via.placeholder.com/150x90"}}" alt="" />
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 90px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                                        <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                                        <input type="file" name="image_1_of_the_main_categories_{{$alt['locale']}}" class="file"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                                                    </div>
                                                </div>
                                                <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => '150','height' => '90'])}}</span>
                                            </div>
                                            @endforeach

                                            @foreach ($AllLangs as $alt)
                                            <div class="form-group">
                                                <label class="control-label">{{trans('products.image_2_of_the_main_categories')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput" style="display: block;">
                                                    <div class="fileinput-new thumbnail" style="width: 280px; height: 100px;">
                                                        @php
                                                        $Obj = \App\Models\SettingsMediable::where('key','image_2_of_the_main_categories_'.$alt['locale'])->first();
                                                        $Img = NULL;
                                                        if($Obj){
                                                        $S = $Obj->lastMedia('image_2_of_the_main_categories_'.$alt['locale']."_thumb");
                                                    }
                                                    if($Obj && $S){
                                                    $Img = $S->getUrl();
                                                }
                                                @endphp
                                                <img src="{{$Img ?? "https://via.placeholder.com/280x100"}}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 280px; max-height: 100px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                                    <input type="file" name="image_2_of_the_main_categories_{{$alt['locale']}}" class="file"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                                                </div>
                                            </div>
                                            <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => '280','height' => '100'])}}</span>
                                        </div>
                                        @endforeach


                                        <div class="margiv-top-10">
                                            <button type="submit" class="btn green"> {{trans('general.save_changes')}} </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <form id="AboutUs" par1="AboutUs_Settings" role="form">


                                         @foreach ($AllLangs as $alt)

                                        <div class="form-group">
                                            <label class="control-label">{{trans('general.title_of_about_us')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <input dir="{{$alt['dir']}}" type="text" name="about_us_title_{{$alt['locale']}}" value="{{setting('about_us_title_'.$alt['locale'] , "")}}" placeholder="{{trans('general.title_of_about_us')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}" class="form-control"/>
                                        </div>



                                        @if (!$loop->last)
                                        <hr>
                                        @endif

                                        @endforeach

                                        @foreach ($AllLangs as $alt)


                                        <div class="form-group">
                                            <label class="control-label">{{trans('general.text_of_about_us')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}</label>
                                            <textarea dir="{{$alt['dir']}}" name="about_us_text_{{$alt['locale']}}" placeholder="{{trans('general.title_of_about_us')}} - {{trans('general.'.$alt['locale'],[],$alt['locale'])}}" class="form-control ckeditor"/>{{setting('about_us_text_'.$alt['locale'] , "")}}</textarea>
                                        </div>

                                        @if (!$loop->last)
                                        <hr>
                                        @endif

                                        @endforeach


                                        <div class="form-group">
                                            <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('general.about_us')])}}</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 100px;">

                                                      @php
                                                      $Obj = \App\Models\SettingsMediable::where('key','about_img')->first();
                                                      $Img = NULL;
                                                      if($Obj){
                                                      $S = $Obj->lastMedia('about_img');
                                                  }
                                                  if($Obj && $S){
                                                  $Img = $S->getUrl();
                                              }
                                              @endphp
                                              <img src="{{$Img ?? "https://via.placeholder.com/280x100"}}" alt="" /> </div>
                                              <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 100px;"> </div>
                                              <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                                    <input type="file" name="about_img"> </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                                                </div>
                                            </div>
                                            <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => 2484,'height' => 1600])}}</span>
                                        </div>
                                    </div>



                                    <div class="margiv-top-10">
                                        <button type="submit" class="btn green"> {{trans('general.save_changes')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
</div>
</div>
<!-- END PAGE BASE CONTENT -->



@endsection

@section('script')
<script src="{{url('assets')}}/global/plugins/ckeditor4/ckeditor.js" type="text/javascript"></script>
<script src="{{url('assets')}}/js/Settings.js" type="text/javascript"></script>

@endsection



