<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.created_at")}}</label>
                    <div class="col-md-9">
                        <p class="form-control-static">{{$CurrentObj->created_at}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('business_fields.investor')])}}</label>
                    <div class="col-md-9">
                        <a  class="img-responsive popup-link" href="{{$CurrentObj->GetImageURL("main_img")}}"><img src="{{$CurrentObj->GetImageURL("main_img_thumb")}}" /></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.name")}}</label>
                    <div class="col-md-9">
                        <p class="form-control-static">{{$CurrentObj->get_Full_name()}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.phone")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->phone ?? NULL}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.email")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->email ?? NULL}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.address")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->address ?? NULL}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.birthday")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->birthday ?? NULL}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.country")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->Country->{'name_'.App::getLocale()} ?? NULL}}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.country_live")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->CountryLive->{'name_'.App::getLocale()} ?? NULL}}</p>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.city")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->city ?? NULL}}</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.state_c")}}</label>
                    <div class="col-md-5">
                        <p class="form-control-static">{{$CurrentObj->state ?? NULL}}</p>
                    </div>
                </div>

                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{trans("investors.legal_documents")}} </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        @forelse ($Identities as $Identitie)
                            @if ($loop->first)
                                <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                    <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="20%"> {{trans("investors.id_type")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("general.id_no")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("general.image")}} </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                            @endif
                            <tr>
                                <td> {{$Identitie->id_type ?? NULL}} </td>
                                <td> {{$Identitie->id_number ?? NULL}} </td>
                                <td> @if(!empty($Identitie->GetImageURL("main_img_thumb"))) <a class="popup-link" href="{{$Identitie->GetImageURL("main_img")}}"><img class="img-responsive" src="{{$Identitie->GetImageURL("main_img_thumb")}}" /></a> @endif </td>
                            </tr>
                            @if ($loop->last)
                                    </tbody>
                                </table>
                            @endif
                        @empty
                            <div class="note note-warning" style="margin-top: 15px;">{{trans("general.no_data_related_to_:name",["name" => trans("investors.legal_documents")])}}</div>
                        @endforelse
                    </div>
                </div>

                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{trans("investors.payment_data")}} </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        @forelse ($Payments as $Payment)
                            @if ($loop->first)
                                <div class="parent-container">
                                    <div class="row">
                            @endif
                            <div class="col-md-4">

                                <a  href="{{$Payment->GetImageURL("main_img")}}"><img class="img-responsive" src="{{$Payment->GetImageURL("main_img_thumb")}}" /></a>
                            </div>
                            @if ($loop->last)
                                    </div>
                                </div>
                            @endif
                        @empty
                            <div class="note note-warning" style="margin-top: 15px;">{{trans("general.no_data_related_to_:name",["name" => trans("investors.payment_data")])}}</div>
                        @endforelse
                    </div>
                </div>



                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{trans("contracts.investment_contracts")}} </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        @forelse ($Contracts as $Contract)
                            @if ($loop->first)
                                <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                    <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="20%"> {{trans("business_fields.field")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("general.image_of_name",['name' => trans('contracts.the_contract')])}} </th>
                                        <th class="text-center"  width="20%"> {{trans("contracts.contract_pdf")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("shares.share_amount")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("contracts.date")}} </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @endif
                                    <tr>
                                        <td> {{$Contract->Field->name_ar ?? NULL}} </td>
                                        <td> @if(!empty($Contract->GetImageURL("main_img"))) <a class="popup-link" href="{{$Contract->GetImageURL("main_img")}}"><img class="img-responsive" src="{{$Contract->GetImageURL("main_img_thumb")}}" /></a> @endif </td>
                                        <td> @isset($media) <a class='btn btn-xs btn-circle grey-gallery' target='_blank' href='{{url('Contracts/BPDF/' . $Contract->id)}}'>{{trans("general.review")}}</a> @endisset </td>
                                        <td> {{$Contract->amount ?? NULL}} </td>
                                        <td> {{$Contract->date_of_contract ?? NULL}} </td>
                                    </tr>
                                    @if ($loop->last)
                                    </tbody>
                                </table>
                            @endif
                        @empty
                            <div class="note note-warning" style="margin-top: 15px;">{{trans("general.no_data_related_to_:name",["name" => trans("contracts.investment_contracts")])}}</div>
                        @endforelse
                    </div>
                </div>

                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{{trans("general.financial_shares")}} </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        @forelse ($Shares as $Share)
                            @if ($loop->first)
                                <table class="table table-bordered table-striped table-condensed flip-content text-center">
                                    <thead class="flip-content">
                                    <tr>
                                        <th class="text-center" width="20%"> {{trans("general.state")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("business_fields.field")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("shares.share_amount")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("shares.share_number")}} </th>
                                        <th class="text-center"  width="20%"> {{trans("shares.share_date")}} </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @endif
                                    <tr>
                                        <td> @if($Share->state == NULL) {{trans('shares.share_ineffective')}} @else {{trans('shares.effective_share')}} @endif </td>
                                        <td> {{$Share->Field->name_ar ?? NULL}} </td>
                                        <td> {{$Share->share_amount ?? NULL}} </td>
                                        <td> {{$Share->share_number ?? NULL}} </td>
                                        <td> {{$Share->share_date ?? NULL}} </td>
                                    </tr>
                                    @if ($loop->last)
                                    </tbody>
                                </table>
                            @endif
                        @empty
                            <div class="note note-warning" style="margin-top: 15px;">{{trans("general.no_data_related_to_:name",["name" => trans("general.financial_shares")])}}</div>
                        @endforelse
                    </div>
                </div>


            </div>
        </form>
    </div>
</div>