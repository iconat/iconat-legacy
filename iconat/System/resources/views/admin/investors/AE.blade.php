<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('business_fields.investor')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: {{$thumb_width/2}}px; height: {{$thumb_height/2}}px;">
                                <img src="{{$image ?? url(setting('default_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$thumb_width/2}}px; max-height: {{$thumb_height/2}}px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="img" class="file"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.for_best_display_to_image_height_must_be_equal_with_width")}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.name")}}</label>
                    <div class="col-md-2">
                        <input type="text" name="firstname" value="{{$CurrentObj->firstname ?? NULL}}" placeholder="{{trans('general.firstname')}}" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="secondname" value="{{$CurrentObj->secondname ?? NULL}}" placeholder="{{trans('general.secondname')}}" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="thirdname" value="{{$CurrentObj->thirdname ?? NULL}}" placeholder="{{trans('general.thirdname')}}" class="form-control"/>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="fourthname" value="{{$CurrentObj->fourthname ?? NULL}}" placeholder="{{trans('general.fourthname')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.phone")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="phone" value="{{$CurrentObj->phone ?? NULL}}" placeholder="{{trans('general.phone')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.email")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="email" value="{{$CurrentObj->email ?? NULL}}" placeholder="{{trans('general.email')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.address")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="address" value="{{$CurrentObj->address ?? NULL}}" placeholder="{{trans('general.address')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.birthday")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="birthday" value="{{$CurrentObj->birthday ?? NULL}}" placeholder="{{trans('general.birthday')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.country")}}</label>
                    <div class="col-md-5">
                        <select class="form-control select2me" name="country">
                            <option value="">{{trans('general.country')}}</option>
                            @foreach(App\Models\Countries::where('arabic_country','=', 1)->orderBy('order', 'desc')->get() as  $countray)
                                <option value="{{$countray->id}}" @if(!empty($CurrentObj->id_country) && ($CurrentObj->id_country == $countray->id)) selected @endif >{{ $countray->{'name_'.\App::getLocale()} }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.country_live")}}</label>
                    <div class="col-md-5">
                        <select class="form-control select2me" name="id_country_live">
                            <option value="">{{trans('general.country_live')}}</option>

                            @foreach(App\Models\Countries::where('arabic_country','=', 1)->orderBy('order', 'desc')->get() as  $countray)
                                <option value="{{$countray->id}}" @if(!empty($CurrentObj->id_country_live) && ($CurrentObj->id_country_live == $countray->id)) selected @endif >{{ $countray->{'name_'.\App::getLocale()} }}</option>

                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.city")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="city" value="{{$CurrentObj->city ?? NULL}}" placeholder="{{trans('general.city')}}" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.state")}}</label>
                    <div class="col-md-5">
                        <input type="text" name="state" value="{{$CurrentObj->state ?? NULL}}" placeholder="{{trans('general.state')}}" class="form-control"/>
                    </div>
                </div>

                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil"></i>{{trans("investors.payment_data")}} </div>
                        <div class="tools">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-repeater" style="margin: 10px 0;">
                            <div data-repeater-list="Payments">
                                <?php
                                $Item_View = view('admin.investors.Payments_repeater');
                                $Item_View->with('Items_Of_Payment', $Items_Of_Payment);
                                !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                                echo $Item_View;
                                ?>
                                @php

                                    foreach ($Items_Of_Payment as $Item) {
                                        $Item_View = view('admin.investors.Payments_repeater');
                                        $Item_View->with('Items_Of_Payment', $Items_Of_Payment);
                                        $Item_View->with('Item', $Item);
                                        echo $Item_View;
                                    }
                                @endphp
                            </div>
                            <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-3 col-md-offset-9">
                                <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("financial.singleproof_of_payment")])}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil"></i>{{trans("contracts.investment_contracts")}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-repeater" style="margin: 10px 0;">
                            <div data-repeater-list="contract_details">
                                @php
                                $Item_View = view('admin.investors.Contracts_repeater');
                                $Item_View->with('Items_Of_Contracts', $Items_Of_Contracts);
                                !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                                echo $Item_View;
                                foreach ($Items_Of_Contracts as $Item) {
                                    $Item_View = view('admin.investors.Contracts_repeater');
                                    $Item_View->with('Items_Of_Contracts', $Items_Of_Contracts);
                                    $Item_View->with('Item', $Item);
                                    echo $Item_View;
                                }
                                @endphp
                            </div>
                            <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-3 col-md-offset-9">
                                <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("contracts.contract")])}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil"></i>{{trans("investors.legal_documents")}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-repeater" style="margin: 10px 0;">
                            <div data-repeater-list="identity_details">
                                @php
                                    $Item_View = view('admin.investors.legal_documents_repeater');
                                    $Item_View->with('Items_Of_Identities', $Items_Of_Identities);
                                    !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                                    echo $Item_View;
                                    foreach ($Items_Of_Identities as $Item) {
                                        $Item_View = view('admin.investors.legal_documents_repeater');
                                        $Item_View->with('Items_Of_Identities', $Items_Of_Identities);
                                        $Item_View->with('Item', $Item);
                                        echo $Item_View;
                                    }
                                @endphp
                            </div>
                            <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-3 col-md-offset-9">
                                <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("investors.legal_document_once")])}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="portlet box green-meadow">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil"></i>{{trans("general.financial_shares")}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-repeater" style="margin: 10px 0;">
                            <div data-repeater-list="share_amount_details">
                                @php
                                    $Item_View = view('admin.investors.share_amount_repeater');
                                    $Item_View->with('Items_Of_Shares', $Items_Of_Shares);
                                    !empty($CurrentObj)?$Item_View->with('CurrentObj', $CurrentObj):NULL;
                                    echo $Item_View;
                                    foreach ($Items_Of_Shares as $Item) {
                                        $Item_View = view('admin.investors.share_amount_repeater');
                                        $Item_View->with('Items_Of_Shares', $Items_Of_Shares);
                                        $Item_View->with('Item', $Item);
                                        echo $Item_View;
                                    }
                                @endphp
                            </div>
                            <button type="button" data-repeater-create class="btn blue-steel mt-repeater-add col-md-3 col-md-offset-9">
                                <i class="fa fa-plus"></i> {{trans("general.add_name_element",["name" => trans("shares.share")])}}
                            </button>
                        </div>
                    </div>
                </div>



            </div>
        </form>
    </div>
</div>