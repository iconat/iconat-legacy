<div data-repeater-item class="mt-repeater-item mt-overflow items" style="border-bottom:none;padding-bottom:0;margin-bottom: 0;<?php if (isset($CurrentObj) && count($Items_Of_Payment) > 0) { ?>display: none;<?php } ?>">
    <div>
        <div class="col-md-8">
            <div class="form-group">
                <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('financial.proof_of_payment')])}}</label>
                <div class="col-md-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: {{$thumb_width/2}}px; height: {{$thumb_height/2}}px;">
                            @isset($Item)
                                @php
                                    $image = ($Item->lastMedia("main_img_thumb")) ? $Item->lastMedia("main_img_thumb")->getUrl() : NULL;
                                @endphp
                            @endisset
                            <img src="{{$image ?? url(setting('default_image' , ""))}}" alt="" /> </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$thumb_width/2}}px; max-height: {{$thumb_height/2}}px;"> </div>
                        <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="payment_image" class="file"> </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
           <div class="form-group">
                <?php if(isset($Item)){ ?>
                <input type="text" name="id" value="<?php echo $Item->id; ?>" hidden="" />
                <?php } ?>
                <a href="javascript:;" par1="{{$Item->id ?? NULL}}" par2="{{trans("financial.proof_of_payment")}}" par3="Payments" Dtitle="{{trans("general.alert")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_this_item")}}" confirmButton="{{trans("general.yes")}}" cancelButton="{{trans("general.no")}}" data-repeater-delete class="btn btn-danger btn-block mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline" style="margin-top:0;">
                    <i class="fa fa-close"></i> {{trans("general.delete")}}
                </a>
            </div>
        </div>
    </div>
</div>