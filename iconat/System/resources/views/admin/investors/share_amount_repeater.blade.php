<div data-repeater-item class="mt-repeater-item mt-overflow items share_amount_details" style="border-bottom:none;padding-bottom:0;margin-bottom: 0;<?php if (isset($CurrentObj) && count($Items_Of_Shares) > 0) { ?>display: none;<?php } ?>">
    <div>
        <div class="col-md-3">
            <select class="form-control" name="id_type" style="padding:0 12px;">
                <option value="">{{trans("business_fields.field")}}</option>
                @foreach(App\Models\Classes::orderBy('id', 'desc')->get() as  $Field)
                    <option value="{{$Field->id}}" @if(!empty($Item->field_id) && ($Item->field_id == $Field->id)) selected @endif >{{ $Field->name_ar }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <input name="share_amount" class="form-control" type="text" value="@isset($Item){{$Item->share_amount}}@endisset" placeholder="{{trans("shares.share_amount")}}" />
        </div>
        <div class="col-md-2">
            <input name="share_number" class="form-control" type="text" value="@isset($Item){{$Item->share_number}}@endisset" placeholder="{{trans("shares.share_number")}}" />
        </div>
        <div class="col-md-2">
            <input name="share_date" class="form-control date-picker" type="text" value="@isset($Item){{$Item->share_date}}@endisset" placeholder="{{trans("shares.share_date")}}" />
        </div>


        <div class="col-md-2">
           <div class="form-group">
                @isset($Item)
                <input type="text" name="id" value="{{$Item->id ?? NULL}}" hidden="" />
                @endisset
                <a href="javascript:;" par1="{{$Item->id ?? NULL}}" par2="{{trans("shares.share")}}" par3="share" Dtitle="{{trans("general.alert")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_this_item")}}" confirmButton="{{trans("general.yes")}}" cancelButton="{{trans("general.no")}}" data-repeater-delete class="btn btn-danger btn-block mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline" style="margin-top:0;">
                    <i class="fa fa-close"></i> {{trans("general.delete")}}
                </a>
            </div>
        </div>
    </div>
</div>