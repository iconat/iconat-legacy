<div style="margin-top:10px;">
    <div class="col-md-11">
        <input dir="ltr" readonly id="item" class="form-control" value="{{$Item}}"/>
    </div>
    <div class="col-md-1">
        <button id="copy" class="btn btn-alert">{{trans("classes.copy")}}</button>
    </div>
</div>