<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <!-- BEGIN FORM-->
        <form id="DForm" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.group")}}</label>
                    <div class="col-md-9">
                        <select name="group" class="select2me form-control">

                            @foreach(App\Models\Translator\Groups::get() as $value)
                                <option value="{{$value->name}}" @if($AllPrev && explode('.',$par1)[0] == $value->name) selected @endif >{{$value->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.token")}}</label>
                    <div class="col-md-9">
                        <input dir="ltr" type="text" name="item" value="@if($AllPrev){{explode('.',$par1)[1]}}@endif" class="form-control">
                    </div>
                </div>

                @foreach ($AllLangs as $alt)
                    @if($AllPrev)
                    @php
                    $vv = NULL;
                    foreach($AllPrev as $prev){
                    if($prev->locale == $alt['locale']){
                    $vv = $prev->text;
                    }
                    }

                    @endphp
                    @endif
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{ trans('general.'.$alt['locale']) }}</label>
                        <div class="col-md-9">
                            <input dir="{{ $alt['dir'] }}" type="text" name="txt_{{ $alt['locale'] }}" value="@isset($vv){{$vv}}@endisset" class="form-control">

                        </div>
                    </div>
                @endforeach
            </div>


        </form>

        <!-- END FORM-->

    </div>


    <!-- END VALIDATION STATES-->

</div>