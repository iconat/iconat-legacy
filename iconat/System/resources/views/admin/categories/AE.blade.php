<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" par2="{{$Parent->id ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox mt-checkbox-outline">
                                <input type="checkbox" name="active" @if((empty($CurrentObj)) ||($CurrentObj && ($CurrentObj->active == 1))) checked @endif> {{trans("general.active")}}
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("categories.parent_category")}}</label>
                    <div class="col-md-9">
                        <select class="select2me" name="parent_id">
                            <option class=""></option>
                            @foreach(\App\Models\Categories::where(function ($query) use ($CurrentObj) {
                                //To Select if item marked as not active after select this...
                                $query->where('active',1);
                                if(!empty($Parent->id)){
                                $query->where('id',"!=",$Parent->id);
                                }
                                if($CurrentObj){
                                $query->where('id',"!=",$CurrentObj->id);
                                }
                                if($CurrentObj && (!empty($CurrentObj->parent_id))){
                                    $query->orWhere('id',$CurrentObj->parent_id);
                                }
                                })->get() as $value)

                                <option value="{{$value->id}}" @if(($CurrentObj && ($CurrentObj->parent_id == $value->id)) || (!empty($Parent->id) && $Parent->id == $value->id) ) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("categories.categorie_importance")}}</label>
                    <div class="col-md-9">
                        <select class="form-control form-filter select2me" name="importance">
                            <option value=""></option>
                            @foreach($importance_array as $key=>$value)
                            <option value="{{$key}}" @if($CurrentObj && ($CurrentObj->importance == $key)) selected @endif>{{trans($value)}}</option>
                            @endforeach
                        </select>
                        <span class="help-block">{{trans("categories.the_importance_of_the_classification_depends_on_the_places_it_appears_on_the_different_pages_of_the_site,_including_the_home_page")}}</span>
                    </div>
                </div>

                @foreach ($AllLangs as $alt)
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('categories.the_categorie')])}} - {{trans('general.'.$alt['locale'])}}</label>
                    <div class="col-md-9">
                        <input type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                    </div>
                </div>
                @endforeach
            </div>
        </form>
    </div>
</div>