@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => trans("slider.slider_category")])])

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("slideR.slider_category")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("slider.slider_category")}}</h1>




   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => trans("slider.slider_category")))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("general.one_slide")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                     <thead>

                     <tr role="row" class="heading">
                        <th class="text-center" width="20%">{{trans("slider.picture")}} {{trans("general.or")}} {{trans("general.videoo")}}</th>
                        <th class="text-center" width="10%">{{trans("general.order")}}</th>

                        <th class="text-center" width="10%">{{trans("general.state")}}</th>

                         <th class="text-center" width="15%">{{trans("general.shopname")}}</th>

                        <th class="text-center" width="20%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">



                        </td>


                        <td class="text-center">
                           <!-- <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="{{trans('general.from')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                           <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="{{trans('general.to')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div> -->
                        </td>

                        <td class="text-center" style="padding: 8px;">

                         <select class="form-control form-filter " name="active">

                              <option value=""></option>

                              <option value="active">{{trans("general.active")}}</option>
                              <option value="inactive">{{trans("general.inactive")}}</option>
                           </select>

                        </td>

                        <td class="text-center" style="padding: 8px;">

                          <select class="form-control form-filter select2me" name="shop_link">

                                <option value=""></option>

                              @foreach(App\Models\Shops::where('active', 1)->orderBy('id', 'desc')->get() as  $shop)
                                <option value="{{$shop->id}}" @if(!empty($CurrentObj->shop_link) && ($CurrentObj->shop_link == $shop->id)) selected @endif >{{ $shop->Get_Trans(App::getLocale(),'name') }}</option>
                             @endforeach



                            </select>

                        </td>




                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
<script src="{{url('assets')}}/global/plugins/ckeditor4/ckeditor.js" type="text/javascript"></script>
<script src="{{url('assets')}}/js/Slider_Category.js" type="text/javascript"></script>

@endsection
