@extends('admin.app', ['title' => trans('account.personal_information').' | '.$Obj->get_Full_name()])
@section('css')
   <link href="{{url('')}}/assets/pages/css/profile-rtl.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{ trans('account.personal_information') }}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->
   <!-- BEGIN PAGE BASE CONTENT -->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PROFILE CONTENT -->
         <div class="profile-content">
            <div class="row">
               <div class="col-md-12">
                  <div class="portlet light bordered">
                     <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                           <i class="icon-globe theme-font hide"></i>
                           <span class="caption-subject font-blue-madison bold uppercase">{{ trans('account.personal_information') }}</span>
                        </div>
                        <ul class="nav nav-tabs">
                           <li class="active">
                              <a href="#tab_1_1" data-toggle="tab">{{trans('general.fullname')}}</a>
                           </li>
                           <li>
                              <a href="#tab_1_2" data-toggle="tab">{{trans('general.personal_picture')}}</a>
                           </li>
                           <li>
                              <a href="#tab_1_3" data-toggle="tab">{{trans('general.communication_data')}}</a>
                           </li>
                           <li>
                              <a href="#tab_1_4" data-toggle="tab">{{trans('general.residence_data')}}</a>
                           </li>
                        </ul>
                     </div>
                     <div class="portlet-body">
                        <div class="tab-content">
                           <!-- PERSONAL INFO TAB -->
                           <div class="tab-pane active" id="tab_1_1">
                              <form role="form">
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.firstname')}}</label>
                                    <input type="text" name="firstname" value="{{$invs_account->firstname ?? NULL}}" placeholder="{{trans('general.firstname')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.secondname')}}</label>
                                    <input type="text" name="secondname" value="{{$invs_account->secondname ?? NULL}}" placeholder="{{trans('general.secondname')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.thirdname')}}</label>
                                    <input type="text" name="thirdname" value="{{$invs_account->thirdname ?? NULL}}" placeholder="{{trans('general.thirdname')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.fourthname')}}</label>
                                    <input type="text" name="fourthname" value="{{$invs_account->fourthname ?? NULL}}" placeholder="{{trans('general.fourthname')}}" class="form-control"/>
                                 </div>

                                 <div class="margiv-top-10">
                                    <button type="button" class="btn green"> {{trans('general.save_changes')}} </button>
                                    <a href="{{url('Manage/MyAccount')}}" class="btn default"> {{trans('general.cancel')}} </a>
                                 </div>
                              </form>
                           </div>
                           <!-- END PERSONAL INFO TAB -->
                           <!-- CHANGE AVATAR TAB -->
                           <div class="tab-pane" id="tab_1_2">
                              <form role="form">
                                 <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-new thumbnail" style="width: {{$thumb_width}}px; height: {{$thumb_height}}px;">
                                          @php $image = isset($invs_account->profileimg_thumb)?url($invs_account->profileimg_thumb):NULL; @endphp
                                          <img src="{{$image ?? url('').'/assets/images/300_300.jpg'}}"
                                               alt=""/></div>
                                       <div class="fileinput-preview fileinput-exists thumbnail"
                                            style="max-width: {{$thumb_width}}px; max-height: {{$thumb_height}}px;"></div>
                                       <div>
                                       <span class="btn default btn-file">
                                           <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                           <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                           <input type="file" name="..."> </span>
                                          <a href="javascript:;" class="btn default fileinput-exists"
                                             data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                                       </div>
                                    </div>

                                 </div>
                                 <div class="margiv-top-10">
                                    <button type="button" class="btn green"> {{trans('general.save_changes')}} </button>
                                    <a href="{{url('Manage/MyAccount')}}" class="btn default"> {{trans('general.cancel')}} </a>
                                 </div>
                              </form>
                           </div>
                           <!-- END CHANGE AVATAR TAB -->
                           <!-- CHANGE PASSWORD TAB -->
                           <div class="tab-pane" id="tab_1_3">
                              <form>

                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.phone')}}</label>
                                    <input type="text" name="phone" value="{{$invs_account->phone ?? NULL}}" placeholder="{{trans('general.phone')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.email')}}</label>
                                    <input type="text" name="email" value="{{$invs_account->email ?? NULL}}" placeholder="{{trans('general.email')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.address')}}</label>
                                    <input type="text" name="address" value="{{$invs_account->address ?? NULL}}" placeholder="{{trans('general.address')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.birthday')}}</label>
                                    <input type="text" name="birthday" value="{{$invs_account->birthday ?? NULL}}" placeholder="{{trans('general.birthday')}}" class="form-control"/>
                                 </div>
                                 <div class="margiv-top-10">
                                    <button type="button" class="btn green"> {{trans('general.save_changes')}} </button>
                                    <a href="{{url('Manage/MyAccount')}}" class="btn default"> {{trans('general.cancel')}} </a>
                                 </div>
                              </form>
                           </div>
                           <!-- END CHANGE PASSWORD TAB -->
                           <!-- PRIVACY SETTINGS TAB -->
                           <div class="tab-pane" id="tab_1_4">
                              <form>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.country')}}</label>
                                    <select class="form-control select2me" name="country">
                                       <option value="">{{trans('general.country')}}</option>

                                       @foreach(App\Models\Countries::where('arabic_country','=', 1)->orderBy('order', 'desc')->get() as  $countray)
                                          <option value="{{$countray->id}}" @if(!empty($invs_account->id_country) && ($invs_account->id_country == $countray->id)) selected @endif >{{ $countray->{'name_'.\App::getLocale()} }}</option>
                                       @endforeach

                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.country_live')}}</label>
                                    <select class="form-control select2me" name="id_country_live">
                                       <option value="">{{trans('general.country_live')}}</option>

                                       @foreach(App\Models\Countries::where('arabic_country','=', 1)->orderBy('order', 'desc')->get() as  $countray)
                                          <option value="{{$countray->id}}" @if(!empty($invs_account->id_country_live) && ($invs_account->id_country_live == $countray->id)) selected @endif >{{ $countray->{'name_'.\App::getLocale()} }}</option>

                                       @endforeach

                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.city')}}</label>
                                    <input type="text" name="city" value="{{$invs_account->city ?? NULL}}" placeholder="{{trans('general.city')}}" class="form-control"/>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">{{trans('general.state')}}</label>
                                    <input type="text" name="state" value="{{$invs_account->state ?? NULL}}" placeholder="{{trans('general.state')}}" class="form-control"/>
                                 </div>
                                 <!--end profile-settings-->
                                 <div class="margiv-top-10">
                                    <button type="button" class="btn green"> {{trans('general.save_changes')}} </button>
                                    <a href="{{url('Manage/MyAccount')}}" class="btn default"> {{trans('general.cancel')}} </a>
                                 </div>
                              </form>
                           </div>
                           <!-- END PRIVACY SETTINGS TAB -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END PROFILE CONTENT -->
      </div>
   </div>
   <!-- END PAGE BASE CONTENT -->



@endsection



