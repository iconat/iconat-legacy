@extends('admin.app', ['title' => trans('account.personal_information').' | '.$Obj->get_Full_name()])
@section('css')
   <link href="{{url('')}}/assets/pages/css/profile{{($currentLanguage->dir == "rtl")?'-rtl':NULL}}.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{ trans('account.personal_information') }}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->
   <!-- BEGIN PAGE BASE CONTENT -->
   <div class="row margin-top-15">
      <div class="col-md-12">
         <!-- BEGIN PROFILE SIDEBAR -->
         <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet bordered">
               <!-- SIDEBAR USERPIC -->
               <div class="profile-userpic">
                  <img src="{{Auth::user()->get_personal_image()}}" class="img-responsive" alt=""></div>
               <!-- END SIDEBAR USERPIC -->
               <!-- SIDEBAR USER TITLE -->
               <div class="profile-usertitle" style="padding-bottom: 5px;">
                  <div class="profile-usertitle-job"> {{$Obj->get_desc_name()}}</div>
                  <div class="profile-usertitle-name"> {{$Obj->get_Short_name()}}</div>
               </div>
               <!-- END SIDEBAR USER TITLE -->
            </div>
            <!-- END PORTLET MAIN -->
         </div>
         <!-- END BEGIN PROFILE SIDEBAR -->
         <!-- BEGIN PROFILE CONTENT -->
        <!--  <div class="profile-content">
            <div class="row">
               <div class="col-md-12">
                  ******
               </div>
            </div>
         </div> -->
         <!-- END PROFILE CONTENT -->
      </div>
   </div>
   <!-- END PAGE BASE CONTENT -->



@endsection



