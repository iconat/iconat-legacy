@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => trans("general.contact")])])

@section('css')



@endsection

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("general.subscribe")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("general.subscribe")}}</h1>




   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

              

             

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                     <thead>

                     <tr role="row" class="heading">

                     
                      <th class="text-center" width="50%">{{trans('general.email')}}</th>
               
                      <th class="text-center" width="5%"></th>
                       
                     </tr>

                     <tr role="row" class="filter">
                   
                        
                        
                        <td class="text-center" style="padding: 8px;">
                         <input type="text" class="form-control form-filter input-sm" name="email">

                        </td>

                         
                        
                        
                    
                        <td class="text-center">

                          

                        </td>
                        



                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
 <!-- BEGIN THEME multiselect SCRIPTS -->
     



<script src="{{url('assets')}}/js/SubscribeAdmin.js" type="text/javascript"></script>

  

@endsection