@extends('admin.app', ['title' => trans('general.control_of_name_contents',['name' => trans("general.allimage ")])])
@section('css')
   <link rel="stylesheet" href="{{url('/assets/global/plugins/magnific-popup/magnific-popup.css')}}">
@endsection

@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("general.allimage")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->


   <div class="row">











<div class="col-md-12">

<h2>صور  المتاجر</h2>
 @foreach($shops as $shop)

 @if( $shop->viewImage('main_shop_img'))

   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$shop->viewImage('main_shop_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$shop->viewImage('main_shop_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif
         @if( $shop->viewImage('main_logo_shop_img'))

   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$shop->viewImage('main_logo_shop_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$shop->viewImage('main_logo_shop_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif

@if( $shop->AllImage(['shop_images']))


          @foreach($shop->AllImage(['shop_images'])->where('deleted_at', NULL ) as $Image)


<div class="col-md-3" style="padding: 10px;">
  <a href="  {{ $Image->getUrl() }}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url(  {{ $Image->getUrl() }});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>


                                       @endforeach

                                       @endif
          @endforeach



             <div class="col-md-12 text-center">

      {{ $shops->links() }}
   </div>



</div>












<div class="col-md-12">

<h2>صورالاعلانات</h2>
 @foreach(\App\Models\Ads::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $adds)
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$adds->viewImage()}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$adds->viewImage()}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>

          @endforeach





</div>



<div class="col-md-12">

<h2>صور المدن</h2>
 @foreach(\App\Models\Cites::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $city)
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$city->viewImage()}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$city->viewImage()}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>

          @endforeach
</div>


<div class="col-md-12">

<h2>صور الاستديو</h2>
 @foreach(\App\Models\Gallery::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $gallary)

 @if( $gallary->viewImage('main_gallery_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{ $gallary->viewImage('main_gallery_img') }}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$gallary->viewImage('main_gallery_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif


         @if( $gallary->viewImage('second_gallery_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{ $gallary->viewImage('second_gallery_img') }}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$gallary->viewImage('second_gallery_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif

           @if( $gallary->viewImage('second_gallery_img2'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{ $gallary->viewImage('second_gallery_img2') }}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$gallary->viewImage('second_gallery_img2')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif
          @endforeach
</div>





<div class="col-md-12">

<h2>صور السلايدر</h2>
 @foreach(\App\Models\Sliders::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $slid)

 @if( $slid->viewImage('main_slider_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid->viewImage('main_slider_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid->viewImage('main_slider_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif


         @if( $slid->viewImage('second_slider_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid->viewImage('second_slider_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid->viewImage('second_slider_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif
            @if( $slid->viewImage('second_slider_img2'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid->viewImage('second_slider_img2')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid->viewImage('second_slider_img2')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif

          @endforeach
</div>



<div class="col-md-12">

<h2>صور سلايدر الاقسام</h2>
 @foreach(\App\Models\Slider_Category::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $slid_cat)

 @if( $slid_cat->viewImage('main_slider_cat_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid_cat->viewImage('main_slider_cat_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid_cat->viewImage('main_slider_cat_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif


         @if( $slid_cat->viewImage('second_slider_cat_img2'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid_cat->viewImage('second_slider_cat_img2')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid_cat->viewImage('second_slider_cat_img2')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif
            @if( $slid_cat->viewImage('second_slider_cat_img'))
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$slid_cat->viewImage('second_slider_cat_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$slid_cat->viewImage('second_slider_cat_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>
         @endif

          @endforeach
</div>



<div class="col-md-12">

<h2>صور تصنيفات المتاجر</h2>
 @foreach(\App\Models\Shop_Categories::where('deleted_at','=', NULL)->orderBY("id",'desc')->get() as $shop)
   <div class="col-md-3" style="padding: 10px;">
  <a href="{{$shop->viewImage('main_shop_categories_img')}}"class="popup-link">


             <div style="
   height: 200px; width: 100%; border: 1px solid black;
   background-image: url({{$shop->viewImage('main_shop_categories_img')}});
   background-repeat: no-repeat;
   background-position: center center;
   background-size: cover;
   ">
</div>

         </a>

         </div>

          @endforeach

</div>




































































































   </div>

   <div class="text-center">

      {{-- {{ $shop->links() }} --}}
   </div>





@endsection



@section('script')

<script src="{{url('assets/global/plugins/magnific-popup/jquery.magnific-popup.js')}}"></script>
<script type="text/javascript">
    $('.popup-link').magnificPopup({
          type: 'image'
         // other options
         });

</script>

@endsection
