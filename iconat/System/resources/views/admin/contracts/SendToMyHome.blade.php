<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$par1  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <p>@php echo trans("contracts.a_copy_of_the_original_contract_will_be_sent_to_your_specified_home_at_the_following_address",["invs_name"=> '<span class="font-blue-steel">'.$investor_account->get_Full_name()."</span>","address" => '<span class="font-purple-seance">'.$investor_account->get_Full_Address().'</span>']); @endphp</p>
                <div class="form-group">
                    <label class="col-md-2 control-label">{{trans("general.notes")}}</label>
                    <div class="col-md-10">
                        <textarea class="form-control" name="amount" rows="5"  placeholder="{{trans("general.notes")}}"></textarea>
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>