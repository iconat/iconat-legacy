<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$Contract->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("business_fields.field")}}</label>
                    <div class="col-md-9">
                        <select class="form-control form-filter select2me" name="field_id">
                            <option value=""></option>
                            @foreach(App\Models\Classes::orderBy('id', 'desc')->get() as  $Field)
                                <option value="{{$Field->id}}" @if(!empty($CurrentObj->field_id) && ($CurrentObj->field_id == $Field->id)) selected @endif >{{ $Field->name_ar }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if(isset($CurrentObj) && !empty($CurrentObj->GetImageURL("main_img_thumb")))
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.image_of_name",['name' => trans('contracts.the_contract')])}}</label>
                    <div class="col-md-9">
                        <a class="popup-link" href="{{$CurrentObj->GetImageURL("main_img")}}"><img class="img-responsive" src="{{$CurrentObj->GetImageURL("main_img_thumb")}}" /></a>
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("business_fields.investor")}}</label>
                    <div class="col-md-9">
                        @if(empty($CurrentObj->invs_id))
                        <select class="form-control form-filter select2me" name="invs_id">
                            <option value=""></option>
                                @foreach(App\Models\Invs::whereNotNull('user_id')->orderBy('id', 'desc')->get() as  $Invs)
                                    <option value="{{$Invs->id}}" @if(!empty($CurrentObj->invs_id) && ($CurrentObj->invs_id == $Invs->id)) selected @endif >{{ $Invs->get_Full_name() }}</option>
                                @endforeach
                        </select>
                        @else
                        <p class="form-control-static font-purple-seance">{{$CurrentObj->Investor->get_Full_name()}}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("contracts.contract_pdf")}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group input-large">
                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename"> </span>
                                </div>
                                <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_file')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="pdf">
                                </span>
                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        @php
                        if(!empty($CurrentObj)){
                            $media = $CurrentObj->lastMedia('contract');
                        }
                        @endphp
                        @isset($media)
                        <span class="help-block">{{trans("general.to_view_the_current_file")}} <a class="btn btn-circle btn-xs grey-mint" target="_blank" href="{{url('Contracts/BPDF/' . $CurrentObj->id)}}">{{trans("general.click_here")}}</a></span>
                        @endisset
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("shares.share_amount")}}</label>
                    <div class="col-md-9">
                        <div class="input-group input-small margin-top-10">
                            <input class="form-control" name="amount" value="{{$CurrentObj->amount ?? NULL}}" placeholder="{{trans("shares.share_amount")}}" type="text">
                            <span class="input-group-addon">
                                <i class="fa fa-dollar"></i>
                            </span>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("contracts.date")}}</label>
                    <div class="col-md-3">
                        <input type="text" name="date_of_contract" value="{{$CurrentObj->date_of_contract ?? NULL}}" placeholder="{{trans("contracts.date")}}" class="form-control date-picker">

                    </div>
                </div>

            </div>
        </form>
    </div>
</div>