@extends('admin.app', ['title' => trans('general.manage_:name',['name' => trans("contracts.investment_contracts")])])
@section('css')
   <link rel="stylesheet" href="{{url('/assets/global/plugins/magnific-popup/magnific-popup.css')}}">
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("contracts.investment_contracts")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("contracts.investment_contracts")}}</h1>




   </div>

   <div class="row">

      <div class="col-md-12">
         <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-title">

               <div class="caption">

                  <i class="icon-eye font-dark"></i>

                  <span class="caption-subject font-dark sbold uppercase">{{trans("general.browse_and_search_in_name",array("name" => trans("contracts.investment_contracts")))}}</span>

               </div>

               <div class="actions">

                  <div class="btn-group">
                     @if($Add)
                     <button  par1="Add" class="btn btn-sm green AEObject">
                        <i class="fa fa-plus"></i> {{trans("general.add_name_element",array("name" => trans("contracts.contract")))}}</button>
                     @endif

                  </div>

               </div>

            </div>

            <div class="portlet-body">

               <div class="table-container">

                  <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">

                     <thead>

                     <tr role="row" class="heading">

                        <th class="text-center" width="10%">{{trans("contracts.contract_pdf")}}</th>
                        @if(!$Only_Invs)<th class="text-center" width="20%">{{trans("business_fields.investor")}}</th>@endif
                        <th class="text-center" width="15%">{{trans("business_fields.section")}}</th>
                        <th class="text-center" width="15%">{{trans("shares.share_amount")}}</th>
                        <th class="text-center" width="15%">{{trans("contracts.date")}}</th>
                        <th class="text-center" width="@if($Only_Invs) 30 @else 15 @endif%">{{trans("general.control")}}</th>

                     </tr>

                     <tr role="row" class="filter">

                        <td class="text-center" style="padding: 8px;">



                        </td>
                        @if(!$Only_Invs)
                           <td class="text-center" style="padding: 8px;">
                           <input type="text" class="form-control form-filter input-sm" name="investor_name">

                        </td>
                        @endif
                        <td>
                           <select class="form-control form-filter select2me" name="field_id">
                              <option value=""></option>
                              @foreach(App\Models\Classes::orderBy('id', 'desc')->get() as  $Field)
                                 <option value="{{$Field->id}}">{{ $Field->name_ar }}</option>
                              @endforeach
                           </select>
                        </td>
                        <td class="text-center" style="padding: 8px;">

                           <input type="text" class="form-control form-filter input-sm" name="share_number">

                        </td>


                        <td class="text-center">
                           <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="{{trans('general.from')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                           <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                              <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="{{trans('general.to')}}">
                              <span class="input-group-btn">
                                  <button class="btn btn-sm default"
                                          type="button">
                                      <i class="fa fa-calendar"></i>
                                  </button>
                              </span>
                           </div>
                        </td>




                        <td class="text-center">

                           <div class="margin-bottom-5">

                              <button class="btn btn-sm green btn-outline filter-submit margin-bottom">

                                 <i class="fa fa-search"></i> {{trans("general.search")}}</button>

                           </div>

                           <button class="btn btn-sm red btn-outline filter-cancel">

                              <i class="fa fa-times"></i> {{trans("general.cancel_filters")}}</button>

                        </td>

                     </tr>

                     </thead>

                     <tbody> </tbody>

                  </table>

               </div>

            </div>

         </div>
      </div>

   </div>



@endsection



@section('script')
   <script src="{{url('assets/global/plugins/magnific-popup/jquery.magnific-popup.js')}}"></script>

   <script src="{{url('assets')}}/js/Contracts.js" type="text/javascript"></script>

@endsection