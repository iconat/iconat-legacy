@extends('admin.app', ['title' => trans("news.news_of_company")])
@section('css')
   <link href="{{url('')}}/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

   <!-- BEGIN PAGE BAR -->
   <div class="page-bar">
      <ul class="page-breadcrumb">
         <li>
            <a href="{{url("My")}}">{{trans("general.home")}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
            <span>{{trans("news.news_of_company")}}</span>
         </li>
      </ul>
   </div>
   <!-- END PAGE BAR -->

   <div class="breadcrumbs">

      <h1>{{trans("news.news_of_company")}}</h1>




   </div>
   <!-- BEGIN PAGE BASE CONTENT -->
   <div class="blog-page blog-content-1">
      <div class="row">
         @foreach($ObjsToShow as $Obj)
         @php
            $image = !empty($Obj) ? ((($Obj->lastMedia("main_img_thumb")) ? $Obj->lastMedia("main_img_thumb")->getUrl() : NULL)) : NULL;
         @endphp
         <div class="col-md-3 col-sm-3">
            <div class="blog-post-sm bordered blog-container">
               <div class="blog-img-thumb">
                  <a href="{{url("Show/New/".$Obj->id)}}">
                     <img src="{{$image ?? url('').'/assets/images/300_300.jpg'}}" />
                  </a>
               </div>
               <div class="blog-post-content">
                  <h2 class="blog-title blog-post-title">
                     <a href="{{url("Show/New/".$Obj->id)}}">{{ $Obj->{"title_".\App::getLocale()} }}</a>
                  </h2>

                  <div class="blog-post-foot">
                     <div class="blog-post-meta">
                        <i class="icon-calendar font-blue"></i>
                        <a href="{{url("Show/New/".$Obj->id)}}">{{ date('Y/m/d',strtotime($Obj->created_at)) }}</a>
                     </div>

                  </div>
               </div>
            </div>
         </div>
         @endforeach
         @if(count($ObjsToShow) == 0)
            <div class="col-md-12">
               <div class="note note-info margin-top-15">
                  <p>{{trans("general.no_content_found_to_display")}}</p>
               </div>
            </div>
         @endif
      </div>
   </div>
   <!-- END PAGE BASE CONTENT -->




@endsection



