<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">
                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.name_of_:name",["name"=>trans('products.the_product')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <input placeholder="{{trans("general.name_of_:name",["name"=>trans('products.the_product')])}} - {{trans('general.'.$alt['locale'])}}" type="text" dir="{{$alt['dir']}}" name="{{$alt['locale']}}[name]" class="form-control" value="@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'name') ?? NULL}}@endif">
                        </div>
                    </div>
                @endforeach

                @foreach ($AllLangs as $alt)
                    <div class="form-group">
                        <label class="col-md-3 control-label">{{trans("general.text_of_:name",["name"=>trans('products.the_product')])}} - {{trans('general.'.$alt['locale'])}}</label>
                        <div class="col-md-9">
                            <textarea dir="{{$alt['dir']}}" name="{{$alt['locale']}}[text]" class="form-control" placeholder="{{trans("general.text_of_:name",["name"=>trans('products.the_product')])}} - {{trans('general.'.$alt['locale'])}}">@if($CurrentObj){{$CurrentObj->Get_Trans($alt['locale'],'text') ?? NULL}}@endif</textarea>
                        </div>
                    </div>
                @endforeach


                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.title_of_name",["name"=>trans('categories.the_categorie')])}}</label>
                    <div class="col-md-9">
                        <select class="select2me" name="categorie">
                            <option class=""></option>
                            @foreach(\App\Models\Categories::where(function ($query) use ($CurrentObj) {
                                //To Select if item marked as not active after select this...
                                $query->where('active',1);
                                if($CurrentObj && (!empty($CurrentObj->categorie))){
                                    $query->orWhere('id',$CurrentObj->categorie);
                                }
                                })->get() as $value)

                                <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->categorie == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("products.product_code")}}</label>
                    <div class="col-md-3">
                        <input type="text" disabled value="{{$CurrentObj->product_code ?? trans("general.automatically_generated")}}" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("products.price")}}</label>
                    <div class="col-md-2">
                        <input type="text" name="customer_price" value="{{$CurrentObj->customer_price ?? NULL}}" class="form-control">
                        <span class="help-block">{{trans("products.price")}} {{trans('products.for_customer')}}</span>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="real_price" value="{{$CurrentObj->real_price ?? NULL}}" class="form-control">
                        <span class="help-block">{{trans("products.real_price")}}</span>
                    </div>
                    <div class="col-md-2">
                        <input type="text" name="dealer_price" value="{{$CurrentObj->dealer_price ?? NULL}}" class="form-control">
                        <span class="help-block">{{trans("products.price")}} {{trans('products.for_dealer')}}</span>
                    </div>
                    <div class="col-md-3">
                        <select class="select2me" name="price_currency">
                            <option class=""></option>
                            @foreach(\App\Models\Currencies::where(function ($query) use ($CurrentObj) {
                                //To Select if item marked as not active after select this...
                                $query->where('active',1);
                                if($CurrentObj && (!empty($CurrentObj->price_currency))){
                                    $query->orWhere('id',$CurrentObj->price_currency);
                                }
                                })->get() as $value)

                                <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->price_currency == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                            @endforeach

                        </select>
                        <span class="help-block">{{trans('currencies.the_currency')}}</span>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("units.the_unit")}}</label>

                    <div class="col-md-3">
                        <select class="select2me" name="unit">
                            <option class=""></option>
                            @foreach(\App\Models\Units::where(function ($query) use ($CurrentObj) {
                                //To Select if item marked as not active after select this...
                                $query->where('active',1);
                                if($CurrentObj && (!empty($CurrentObj->unit))){
                                    $query->orWhere('id',$CurrentObj->unit);
                                }
                                })->get() as $value)

                                <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->unit == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">{{trans("units.the_unit")}}</label>--}}
                    {{--<div class="col-md-3">--}}
                        {{--<select class="select2me" name="currency">--}}
                            {{--<option class=""></option>--}}
                            {{--@foreach(\App\Models\Units::where('active',1)->get() as $value)--}}
                                {{--<option value="{{$value->id}}" @if($CurrentObj && $CurrentObj->currency == $value->id) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--<span class="help-block">{{trans('currencies.the_currency')}}</span>--}}
                    {{--</div>--}}
                {{--</div>--}}


                <div class="form-group">
                    <label class="control-label col-md-3">{{trans("general.image_of_name",["name"=>trans('products.the_product')])}}</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: {{$main_thumb_width_xs}}px; height: {{$main_thumb_height_xs}}px;">
                                <img src="{{$main_img_thumb ?? url(setting('default_product_image' , ""))}}" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: {{$main_thumb_width_xs}}px; max-height: {{$main_thumb_height_xs}}px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> {{trans('general.select_image')}} </span>
                                    <span class="fileinput-exists"> {{trans('general.change')}} </span>
                                    <input type="file" name="main_product"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('general.remove')}} </a>
                            </div>
                        </div>
                        <span class="help-block">{{trans("general.the_image_will_be_crop_with_:width_pixels_width_and_:height_pixels_height",['width' => $main_thumb_width_lg,'height' => $main_thumb_height_lg])}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <div id="Dzone" class="dropzone" style="overflow-y: hidden;"></div>
                </div>




            </div>
        </form>
    </div>
</div>