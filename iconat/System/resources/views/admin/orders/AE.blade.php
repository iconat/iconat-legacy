<div class="portlet light portlet-fit portlet-form bordered">
    <div class="portlet-body">
        <form id="DForm" par1="{{$CurrentObj->id  ?? NULL}}" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("general.state")}}</label>
                    <div class="col-md-9">
                        <select class="form-control select2me" name="state">
                            <option class=""></option>
                            @foreach(\App\Models\OrdersStatus::where(function ($query) use ($CurrentObj) {
                                //To Select if item marked as not active after select this...
                                $query->where('active',1);
                                if($CurrentObj && (!empty($CurrentObj->state))){
                                    $query->orWhere('id',$CurrentObj->state);
                                }
                                })->get() as $value)

                                <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->state == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">{{trans("orders.purchaser")}}</label>
                    <div class="col-md-9">
                        <p class="form-control-static">{{!empty($CurrentObj)?$CurrentObj->CreatorName():NULL}}</p>
                    </div>
                </div>


                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i>{{trans("orders.order_details")}} </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active">
                                        <a href="#shipping_information" data-toggle="tab"> {{trans("deliverymethods.shipping_information")}} </a>
                                    </li>
                                    <li>
                                        <a href="#payments_information" data-toggle="tab"> {{trans("paymethods.payment_informations")}} </a>
                                    </li>
                                    <li>
                                        <a href="#products" data-toggle="tab"> {{trans("products.products")}} </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="shipping_information">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.shipping_method")}}</label>
                                            <div class="col-md-9">
                                                <select disabled class="form-control select2me" name="state">
                                                    <option class=""></option>
                                                    @foreach(\App\Models\DeliveryMethods::where(function ($query) use ($CurrentObj) {
                                                        //To Select if item marked as not active after select this...
                                                        $query->where('active',1);
                                                        if($CurrentObj && (!empty($CurrentObj->delivery_method))){
                                                            $query->orWhere('id',$CurrentObj->delivery_method);
                                                        }
                                                        })->get() as $value)

                                                        <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->delivery_method == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.contact_name")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][contact_name]" value="{{$CurrentObj->Shipment_Information("contact_name") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.country/region")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][country]" value="{{$CurrentObj->Shipment_Information("country") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.street_address")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][street_address]" value="{{$CurrentObj->Shipment_Information("street_address") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.apartment_suite_unit_etc")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][second_address]" value="{{$CurrentObj->Shipment_Information("second_address") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.city")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][city]" value="{{$CurrentObj->Shipment_Information("city") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.state_province_region")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][region]" value="{{$CurrentObj->Shipment_Information("region") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.zip/postal_code")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][postal_code]" value="{{$CurrentObj->Shipment_Information("postal_code") ?? NULL}}" class="form-control">
                                            </div>
                                        </div><div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("deliverymethods.zip/postal_code")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][postal_code]" value="{{$CurrentObj->Shipment_Information("postal_code") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("general.mobile")}}</label>
                                            <div class="col-md-9">
                                                <input disabled type="text" name="[Shipping_Information][mobile]" value="{{$CurrentObj->Shipment_Information("mobile") ?? NULL}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments_information">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{trans("products.payment_method")}}</label>
                                            <div class="col-md-9">
                                                <select class="form-control select2me" name="[Payment_Information][method_id]" disabled>
                                                    <option class=""></option>
                                                    @foreach(\App\Models\PayMethods::where(function ($query) use ($CurrentObj) {
                                                        //To Select if item marked as not active after select this...
                                                        $query->where('active',1);
                                                        if($CurrentObj && (!empty($CurrentObj->Payments_Information("method_id")))){
                                                            $query->orWhere('id',$CurrentObj->Payments_Information("method_id"));
                                                        }
                                                        })->get() as $value)

                                                        <option value="{{$value->id}}" @if($CurrentObj && ($CurrentObj->Payments_Information("method_id") == $value->id)) selected @endif >{{$value->Get_Trans(App::getLocale(),'name')}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="products">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th> # </th>
                                                        <th> {{trans("products.the_product")}} </th>
                                                        <th> {{trans("products.quantity")}} </th>
                                                        <th> {{trans("orders.unit_price")}} </th>
                                                        <th> {{trans("products.total_price")}} </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($CurrentObj->OrdersProducts as $Product_Rel)
                                                    <tr>
                                                        <td> {{$loop->iteration}} </td>
                                                        <td> {{$Product_Rel->Product->Get_Trans(\App::getLocale(),"name")}} </td>
                                                        <td> {{$Product_Rel->quantity}} </td>
                                                        <td> {{$Product_Rel->unit_price}} <i class="fa fa-{{$Product_Rel->Currency->icon}}"></i> </td>
                                                        <td> {{$Product_Rel->unit_price * $Product_Rel->quantity}} <i class="fa fa-{{$Product_Rel->Currency->icon}}"></i> </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>






            </div>
        </form>
    </div>
</div>