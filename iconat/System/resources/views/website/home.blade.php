@extends('layouts.website', ['title' => trans('general.home'),'main_classes' => 'home-wrap'])
@section('content')


<div id="tt-pageContent">
<div class="mobileHide">
<div class="container-indent0">
   <div class="container">
      <div class="row flex-sm-row-reverse tt-layout-promo-box">
         <div class="col-sm-12 col-md-5">
            <div class="row">
               <div class="col-sm-12">
                  <div class="tt-layout-vertical-listing scroll-box" style="margin-top: 20px;">
                     @foreach($Main_8_Ads as $ad)
                     <div class="tt-item">
                        <div class="tt-layout-vertical">
                           <div class="tt-img">
                              <a href="{{url('ShopsDetails')}}/{{$ad->shop_link}}">
                              <span class="tt-img-default"><img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $ad->GetThumb()}}" alt=""></span>
                              <span class="tt-img-roll-over"><img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $ad->GetThumb()}}" alt=""></span>
                              </a>
                           </div>
                           <div class="tt-description">
                              <ul class="tt-add-info">
                              <li><a href="{{url('ShopsDetails')}}/{{$ad->shop_link}}">{{$ad->Get_Trans(App::getLocale(),'text')}}</a></li>
                              </ul>
                              <h6 class="tt-title"><a href="{{url('ShopsDetails')}}/{{$ad->shop_link}}">{{$ad->Get_Trans(App::getLocale(),'name')}}</a></h6>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
         @if(count($Slides) > 0)
         <div class="col-sm-12 col-md-7">
            <div class="tt-post-img">

























  <!--Main Slider-->
                     <section id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
                        <div id="main-banner2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                           <ul>
                              @php
                         $x=0;
                        @endphp
                        @foreach($Slides as $Slide)
                        @php

                        $MainImage = $Slide->MainImage();
                        $MainVideo = $Slide->GetvideoURL('video');
                        $UrlVideo = $Slide->link_video;
                        $SecondImage = $Slide->SecondImage();
                        $SecondImage2 = $Slide->SecondImage2();
                        $SliderText = $Slide->GetSliderText(App::getLocale());
                        $link= $Slide->shop_link;
                         $x++;
                        @endphp
                        @if($MainImage)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500" class="rev_gradient">
                           <!-- MAIN IMAGE -->
                           <img src="{{ $MainImage }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"  data-no-retina>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                           </div>

                             <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>

                        @endif
                        @if($MainVideo)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade">
                           <div class="rev_overlay"></div>
                           <div class="rs-background-video-layer"
                              data-forcerewind="on"
                              data-volume="mute"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-videomp4="{{$MainVideo}}"
                              data-videopreload="auto"
                              data-videoloop="loopandnoslidestop"
                              data-forceCover="1"
                              data-aspectratio="16:9"
                              data-autoplay="true"
                              data-autoplayonlyfirsttime="false">
                           </div>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                              data-responsive_offset="on"
                              data-width="['auto','auto','auto','200']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-visibility="['on', 'on', 'on', 'on']"
                              data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                           @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif


                           </div>

                            <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>
                        @endif
                        @if($UrlVideo)
                        <!-- SLIDE 2  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500">
                           <!-- MAIN IMAGE -->
                          <!-- BEGIN YOUTUBE LAYER -->
                           <div class="tp-caption tp-resizeme tp-videolayer"
                              data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                              {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                              data-type="video"
                              data-ytid="{{$UrlVideo}}"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-autoplay="on"
                              data-videocontrols="controls"
                              data-nextslideatend="true"
                              data-forcerewind="on"
                              data-videoloop="loopandnoslidestop"
                              data-videoattributes="version=3&enablejsapi=1&html5=1&hd=1&wmode=opaque&showinfo=0&rel=0&
                              origin=https://www.maurelligroup.com;"
                              data-x="center"
                              data-y="center"
                              data-hoffset="0"
                              data-voffset="0"
                              data-basealign="slide">
                              <div>
                                 <div class="tp-caption tp-resizeme"
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                                    data-responsive_offset="on"
                                    data-width="['auto','auto','auto','200']" data-type="text"
                                    data-textalign="['center','center','center','center']"
                                    data-visibility="['on', 'on', 'on', 'on']"
                                    data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                                    data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                    data-start="1200" data-splitin="none" data-splitout="none">
                                    @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                                 </div>
                                 <!-- END YOUTUBE LAYER -->
                                <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn xx" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                         </div> </div>
                        </li>
                        @endif
                        @endforeach
                           </ul>
                           </div>
                     </section>
                     <!--Main Slider ends -->




















               </div>
               </div>
               @endif
            </div>
         </div>
      </div>
      <div class="container-indent">
         <div class="container container-fluid-custom-mobile-padding">
            <!-- <div class="tt-block-title">
               <h2 class="tt-title">City</h2>
               <div class="tt-description">TEXT TEXT TEXT</div>
               </div> -->
            <div class="row tt-img-box-listing">
               @foreach($Main_8_Cites as $City)
               <div class="col">
                  <a href="{{url('/Categoreis')}}/{{$City->id}}" class="tt-img-box">
                  <img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $City->MainImage()}}" alt="" style="border-radius: 10px;">
                  </a>
                  <div class="aa text-center">
                     <h4 class="tt-title">{{$City->Get_Trans(App::getLocale(),'name')}}</h4>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
      </div>
   </div>
   <div class="desHide">
      <div class="container-indent">
         <div class="container container-fluid-custom-mobile-padding">
            <!-- <div class="tt-block-title">
               <h2 class="tt-title">City</h2>
               <div class="tt-description">TEXT TEXT TEXT</div>
               </div> -->
            <div class="row tt-img-box-listing">
               @foreach($Main_8_Cites as $City)
               <div class="col col-4">
                  <a href="{{url('/Categoreis')}}/{{$City->id}}" class="tt-img-box">
                  <img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $City->MainImage()}}" alt="">
                  </a>
                  <div class="aa text-center">
                     <h4 class="tt-title">{{$City->Get_Trans(App::getLocale(),'name')}}</h4>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
      </div>
      <div class="container-indent0">
         <div class="container">
            <div class="row flex-sm-row-reverse tt-layout-promo-box">
               @if(count($Slides) > 0)
               <div class="col-sm-12 col-md-7">
                  <div class="tt-post-img">
                        <!--Main Slider-->
                     <section id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
                        <div id="main-banner2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                           <ul>
                              @php
                         $x=0;
                        @endphp
                        @foreach($Slides as $Slide)
                        @php

                        $MainImage = $Slide->MainImage();
                        $MainVideo = $Slide->GetvideoURL('video');
                        $UrlVideo = $Slide->link_video;
                        $SecondImage = $Slide->SecondImage();
                        $SecondImage2 = $Slide->SecondImage2();
                        $SliderText = $Slide->GetSliderText(App::getLocale());
                        $link= $Slide->shop_link;
                         $x++;
                        @endphp
                        @if($MainImage)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500" class="rev_gradient">
                           <!-- MAIN IMAGE -->
                           <img src="{{ $MainImage }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"  data-no-retina>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                           </div>

                             <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>

                        @endif
                        @if($MainVideo)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade">
                           <div class="rev_overlay"></div>
                           <div class="rs-background-video-layer"
                              data-forcerewind="on"
                              data-volume="mute"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-videomp4="{{$MainVideo}}"
                              data-videopreload="auto"
                              data-videoloop="loopandnoslidestop"
                              data-forceCover="1"
                              data-aspectratio="16:9"
                              data-autoplay="true"
                              data-autoplayonlyfirsttime="false">
                           </div>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                              data-responsive_offset="on"
                              data-width="['auto','auto','auto','200']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-visibility="['on', 'on', 'on', 'on']"
                              data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                           @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif


                           </div>

                            <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>
                        @endif
                        @if($UrlVideo)
                        <!-- SLIDE 2  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500">
                           <!-- MAIN IMAGE -->
                          <!-- BEGIN YOUTUBE LAYER -->
                           <div class="tp-caption tp-resizeme tp-videolayer"
                              data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                              {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                              data-type="video"
                              data-ytid="{{$UrlVideo}}"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-autoplay="on"
                              data-videocontrols="controls"
                              data-nextslideatend="true"
                              data-forcerewind="on"
                              data-videoloop="loopandnoslidestop"
                              data-videoattributes="version=3&enablejsapi=1&html5=1&hd=1&wmode=opaque&showinfo=0&rel=0&
                              origin=https://www.maurelligroup.com;"
                              data-x="center"
                              data-y="center"
                              data-hoffset="0"
                              data-voffset="0"
                              data-basealign="slide">
                              <div>
                                 <div class="tp-caption tp-resizeme"
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                                    data-responsive_offset="on"
                                    data-width="['auto','auto','auto','200']" data-type="text"
                                    data-textalign="['center','center','center','center']"
                                    data-visibility="['on', 'on', 'on', 'on']"
                                    data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                                    data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                    data-start="1200" data-splitin="none" data-splitout="none">
                                    @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                                 </div>
                                 <!-- END YOUTUBE LAYER -->
                                <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn xx" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                         </div> </div>
                        </li>
                        @endif
                        @endforeach
                           </ul>
                           </div>
                     </section>
                     <!--Main Slider ends -->
                  </div>
               </div>
               @endif
               <div class="col-sm-12 col-md-5">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="tt-layout-vertical-listing" style="margin-top: 20px;">
                           @foreach($Main_8_Ads as $ad)
                           <div class="tt-item">
                              <div class="tt-layout-vertical">
                                 <div class="tt-img">
                                    <a href="#">
                                    <span class="tt-img-default"><img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $ad->GetThumb()}}" alt=""></span>
                                    <span class="tt-img-roll-over"><img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{ $ad->GetThumb()}}" alt=""></span>
                                    </a>
                                 </div>
                                 <div class="tt-description">
                                    <ul class="tt-add-info">
                                       <li><a href="#">{{$ad->Get_Trans(App::getLocale(),'text')}}</a></li>
                                    </ul>
                                    <h6 class="tt-title"><a href="#">{{$ad->Get_Trans(App::getLocale(),'name')}}</a></h6>
                                    {{--
                                    <div class="tt-price">
                                       $78
                                    </div>
                                    --}}
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container-indent">
      <div class="container-fluid container-fluid-custom-mobile-padding">
         <div class="tt-block-title">
            <h1 class="tt-title">{{ trans('gallery.title') }}</h1>
            <div class="tt-description">{{ trans('gallery.text') }}</div>
         </div>
         <div class="tt-carousel-products row arrow-location-tab arrow-location-tab01 tt-alignment-img tt-layout-product-item slick-animated-show-js">
            @foreach($Main_8_Gallery as $img)
            @php
            $MainImage = $img->MainImage();
            $MainVideo = $img->GetvideoURL('video');
            $UrlVideo = $img->link_video;
             $SecondImage = $img->SecondImage();
            $SecondImage2 = $img->SecondImage2();
            @endphp
            @if($MainImage)
            <div class="col-2 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center">
                  <div class="box">
                     <div class="imgBox">
                        <a href="{{ $img->shop_link}}"><img src="{{ $img->MainImage()}}"></a>
                     </div>
                     <div class="content">
                        <a href="{{ $img->shop_link}}">
                           <h2>{{$img->Get_Trans(App::getLocale(),'name')}}</h2>
                        </a>
                        <a href="{{ $img->shop_link}}">
                           <p>{{$img->Get_Trans(App::getLocale(),'text')}}</p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            @endif
            @if($MainVideo)
            <div class="col-2 col-md-4 col-lg-3">
               <a href="#" data-toggle="modal" data-type="video" data-target="#modalVideoProduct" data-value="{{$MainVideo}}">
                  <div class="tt-product thumbprod-center">
                     <div class="box">
                        <div class="imgBox">
               <a href="{{ $img->shop_link}}"><img src="{{ $SecondImage ?? "https://www.williamscommerce.com/app/uploads/2018/09/digital-marketing-640x640.jpg"}}"></a>
               </div>
               <div class="content">
               <a href="#" data-toggle="modal" data-type="video" data-target="#modalVideoProduct" data-value="{{$MainVideo}}"><h2>{{$img->Get_Trans(App::getLocale(),'name')}}</h2></a>
               <a href="#" data-toggle="modal" data-type="video" data-target="#modalVideoProduct" data-value="{{$MainVideo}}"><p>{{$img->Get_Trans(App::getLocale(),'text')}}</p></a>
               </div>
               </div>
               </div>
               </a>
            </div>
            @endif
            @if($UrlVideo)
            <div class="col-2 col-md-4 col-lg-3">
               <a href="#" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="https://www.youtube.com/embed/{{$UrlVideo}}">
                  <div class="tt-product thumbprod-center">
                     <div class="box">
                        <div class="imgBox">
               <a href="{{ $img->shop_link}}"><img src="{{ $SecondImage2 ?? "https://www.williamscommerce.com/app/uploads/2018/09/digital-marketing-640x640.jpg"}}"></a>
               </div>
               <div class="content">
               <a href="#" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="https://www.youtube.com/embed/{{$UrlVideo}}"><h2>{{$img->Get_Trans(App::getLocale(),'name')}}</h2></a>
               <a href="#" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="https://www.youtube.com/embed/{{$UrlVideo}}"><p>{{$img->Get_Trans(App::getLocale(),'text')}}</p></a>
               </div>
               </div>
               </div>
               </a>
            </div>
            @endif
            @endforeach
         </div>
      </div>
   </div>
</div>
@endsection
