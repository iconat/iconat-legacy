@extends('layouts.website', ['title' => trans("news.news")." - ".$post->Get_Trans(\App::getLocale(),'name')])

@section('content')

    <!--Breadcrumb Section Start-->
    <section class="breadcrumb-bg bg-2">
        <div class="theme-container container ">
            <div class="site-breadcumb white-clr">
                <h2 class="section-title"> <strong class="clr-txt"> {{$post->Get_Trans(\App::getLocale(),'name')}} </strong>   </h2>
                <ol class="breadcrumb breadcrumb-menubar">
                    <li> <a href="{{url("/")}}"> {{trans("general.home")}} </a> <a href="{{url("/News")}}">{{trans("news.news")}}</a> {{$post->Get_Trans(\App::getLocale(),'name')}}  </li>
                </ol>
            </div>
        </div>
    </section>
    <!--Breadcrumb Section End-->


    <!-- Blog Starts-->
    <section class="sec-space-bottom">
        <div class="container pt-50">
            <div class="row">
                <div class="col-md-12 pt-15">
                    <div class="blog-single">
                        <div class="content">
                            <div class="blog-media">
                                <h4 class="sub-title-1"> {{$post->created_at}} </h4>
                                <h2 class="fsz-30"> <strong>{{$post->Get_Trans(\App::getLocale(),"name")}}</strong> </h2>
                                @if(!empty($post->MainImage()))
                                    <div class="text-center">
                                        <img alt="" src="{{$post->MainImage()}}" />
                                    </div>
                                @endif
                            </div>
                            <div class="caption pt-15">
                                @php echo $post->Get_Trans(\App::getLocale(),"text"); @endphp
                            </div>
                        </div>
                        @if(FALSE)
                        <div class="author crl-bg">
                            <img alt="" src="{{url('')}}/assets/site/img/extra/author-1.png" />
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam Lorem ipsum dolor sit amet consectetuer </p>
                        </div>

                        <div class="pb-50">
                            <h2 class="title-2 pb-15"> <span class="light-font"> Visitor </span> <strong> Comments </strong> <span class="gray-color fsz-12">(2)</span>  </h2>
                            <div class="view-comment">
                                <div class="img-comment">
                                    <img alt="" src="{{url('')}}/assets/site/img/extra/comment-1.png" />
                                </div>
                                <div class="caption-comment">
                                    <h3 class="fsz-16 no-margin">LUISIANA GARCIAS <span class="fsz-12 gray-color">5 Min ago</span> </h3>
                                    <p> Category : <span class="clr-txt">Fruits</span> </p>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam...</p>
                                </div>
                                <div class="view-comment child">
                                    <div class="img-comment">
                                        <img alt="" src="{{url('')}}/assets/site/img/extra/comment-1.png" />
                                    </div>
                                    <div class="caption-comment">
                                        <h3 class="fsz-16 no-margin">JAMES TYLOR  <span class="fsz-12 gray-color">5 Min ago</span> </h3>
                                        <p> Category : <span class="clr-txt">Fruits</span> </p>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam...</p>
                                    </div>
                                    <div class="view-comment child">
                                        <div class="img-comment">
                                            <img alt="" src="{{url('')}}/assets/site/img/extra/comment-1.png" />
                                        </div>
                                        <div class="caption-comment">
                                            <h3 class="fsz-16 no-margin">LUISIANA GARCIAS <span class="fsz-12 gray-color">5 Min ago</span> </h3>
                                            <p> Category : <span class="clr-txt">Fruits</span> </p>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="divider-full-1"></div>

                        <div class="pt-50">
                            <h2 class="title-2 pb-15"> <span class="light-font"> Leave a  </span> <strong> Comments </strong>  </h2>
                            <div class="comment-form pt-15">
                                <form class="comment-form row">
                                    <div class="form-group col-sm-4">
                                        <input class="form-control" placeholder="Name" required="" type="text">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <input class="form-control" placeholder="Email" required="" type="email">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <input class="form-control" placeholder="Website" type="text">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <textarea class="form-control" placeholder="Message" cols="12" rows="4"></textarea>
                                    </div>
                                    <div class="form-group col-sm-12 text-center pt-15">
                                        <button class="theme-btn" type="submit"> <strong> SEND MESSSAGE </strong> </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- / Blog Ends -->


@endsection



@section('script')


@endsection