
@extends('layouts.website', ['title' => trans('categories.categories'),'main_classes' => 'home-wrap'])

@section('content')


<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
            <li><a href="{{url('/Categoreis/'.$city->id)}}">{{trans('categories.categories')}}</a></li>

             @if($CatFromURL && !empty($CatFromURL->parent))
            @foreach($CatFromURL->getParentsAttribute() as $p)
            <li><a href="{{url('/Categoreis/'.$city->id.'?cat='.$p->id)}}">
                  {{ $p->Get_Trans(\App::getLocale(),"name") }}
               </a></li>
            @endforeach

         @endif
         @if($CatFromURL)
         <li>{{ $CatFromURL->Get_Trans(\App::getLocale(),"name") }}</li>
         @endif
        </ul>
    </div>
</div>


    <div id="tt-pageContent">

        @if(count($CatsToShow) > 0)

        <div class="container-indent ">
            <div class="container-fluid-custom">
                 <div class="tt-block-title">
                    <h2 class="tt-title">{{trans('categories.categories_city')}} {{$city->Get_Trans(App::getLocale(),'name')}}</h2>
                    <!-- <div class="tt-description">TEXT TEXT TEXT</div> -->
                </div>
                <div class="row">


        @foreach($CatsToShow as $Categories)



                  <div class="col-6 col-sm-4 col-md-2 col-12-575width">
                        <a href="{{url('/Categoreis/'.$city->id.'?cat='.$Categories->id)}}" class="tt-promo-box tt-one-child">
                            <img src="{{ $Categories->GetThumb()}}" data-src="{{ $Categories->GetThumb()}}" alt="">
                            <div class="tt-description">
                                <div class="tt-description-wrapper">
                                    <div class="tt-background"></div>
                                    <div class="tt-title-small">{{$Categories->Get_Trans(App::getLocale(),'name')}}</div>
                                </div>
                            </div>
                        </a>
                    </div>

            @endforeach






                </div>
            </div>
        </div>

        @endif







@if($CatFromURL)
<div id="tt-pageContent">
   <div class="container-indent ">
      <div class="container-fluid-custom">
         @if($CatFromURL && !empty($CatFromURL->parent))
        <div class="tt-block-title">
            <h2 class="tt-title">{{$CatFromURL->parent->Get_Trans(App::getLocale(),'name')}}</h2>
         </div>
        @endif


        <style>
        .tt-promo-box{
           margin-top: 9px !important;
        }
        </style>
   <div class="row">
@php
                     $Slides = \App\Models\Slider_Category::where('active',1)->where('category_id','=', $CatFromURL->id)->orderBY("order",'asc')->get();
                     @endphp
       @if(count($Slides) > 0)
            <div class="col-6 col-sm-12 col-md-6 col-12-575width">
               <div class="tt-post-img">








  <!--Main Slider-->
                     <section id="revo_main_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
                        <div id="main-banner2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                           <ul>
                              @php
                         $x=0;
                        @endphp
                        @foreach($Slides as $Slide)
                        @php

                        $MainImage = $Slide->MainImage();
                        $MainVideo = $Slide->GetvideoURL('video_cat');
                        $UrlVideo = $Slide->link_video;
                        $SecondImage = $Slide->SecondImage();
                        $SecondImage2 = $Slide->SecondImage2();
                        $SliderText = $Slide->GetSliderText(App::getLocale());
                        $link= $Slide->shop_link;
                         $x++;
                        @endphp
                        @if($MainImage)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500" class="rev_gradient">
                           <!-- MAIN IMAGE -->
                           <img src="{{ $MainImage }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"  data-no-retina>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                           </div>

                             <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>

                        @endif
                        @if($MainVideo)
                        <!-- SLIDE  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade">
                           <div class="rev_overlay"></div>
                           <div class="rs-background-video-layer"
                              data-forcerewind="on"
                              data-volume="mute"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-videomp4="{{$MainVideo}}"
                              data-videopreload="auto"
                              data-videoloop="loopandnoslidestop"
                              data-forceCover="1"
                              data-aspectratio="16:9"
                              data-autoplay="true"
                              data-autoplayonlyfirsttime="false">
                           </div>
                           <!-- LAYER NR. 1 -->
                           <div class="tp-caption tp-resizeme"
                              data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                              data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                              data-responsive_offset="on"
                              data-width="['auto','auto','auto','200']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-visibility="['on', 'on', 'on', 'on']"
                              data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                           @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif


                           </div>

                            <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                        </li>
                        @endif
                        @if($UrlVideo)
                        <!-- SLIDE 2  -->
                        <li data-index="rs-{{ $x }}" data-transition="fade" data-slotamount="default" data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000" data-fsmasterspeed="1500">
                           <!-- MAIN IMAGE -->
                          <!-- BEGIN YOUTUBE LAYER -->
                           <div class="tp-caption tp-resizeme tp-videolayer"
                              data-frames='[{"delay": 500, "speed": 300, "from": "opacity: 0", "to": "opacity: 1"},
                              {"delay": "wait", "speed": 300, "to": "opacity: 0"}]'
                              data-type="video"
                              data-ytid="{{$UrlVideo}}"
                              data-videowidth="100%"
                              data-videoheight="100%"
                              data-autoplay="on"
                              data-videocontrols="controls"
                              data-nextslideatend="true"
                              data-forcerewind="on"
                              data-videoloop="loopandnoslidestop"
                              data-videoattributes="version=3&enablejsapi=1&html5=1&hd=1&wmode=opaque&showinfo=0&rel=0&
                              origin=https://www.maurelligroup.com;"
                              data-x="center"
                              data-y="center"
                              data-hoffset="0"
                              data-voffset="0"
                              data-basealign="slide">
                              <div>
                                 <div class="tp-caption tp-resizeme"
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']"
                                    data-responsive_offset="on"
                                    data-width="['auto','auto','auto','200']" data-type="text"
                                    data-textalign="['center','center','center','center']"
                                    data-visibility="['on', 'on', 'on', 'on']"
                                    data-whitespace="['nowrap', 'nowrap', 'nowrap', 'nowrap']"
                                    data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                    data-start="1200" data-splitin="none" data-splitout="none">
                                    @if ($SliderText)
                               <h1 class="text-capitalize font-medium darkcolor" style="padding: 20px;background: #fff;border-radius: 10px;">{{ $SliderText }} </h1>
                           @endif
                                 </div>
                                 <!-- END YOUTUBE LAYER -->
                                <div class="tp-caption "
                              data-x="['left','left','left','left']" data-hoffset="['5','5','5','5']"
                              data-y="['top','top','top','top']" data-voffset="['-50','-50','-50','-50']"
                              data-whitespace="nowrap" data-responsive_offset="on"
                              data-width="['none','none','none','none']" data-type="text"
                              data-textalign="['center','center','center','center']"
                              data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                              data-start="1200" data-splitin="none" data-splitout="none">
                              @if ($link)
                               <div><a href="{{url('ShopsDetails')}}/{{ $link }}" target="_blank" class="btn xx" data-text="{{ __('general.shop_visit') }}">{{ __('general.shop_visit') }}</a></div>
                           @endif
                           </div>
                         </div> </div>
                        </li>
                        @endif
                        @endforeach
                           </ul>
                           </div>
                     </section>
                     <!--Main Slider ends -->











               </div>
            </div>

            @endif

            @if(count($Slides) > 0)
            <div class="col-6 col-sm-12 col-md-6 col-12-575width">
               <div class="row tt-img-box-listing">
                  @foreach(\App\Models\Shops::where('deleted_at','=', NULL)->where('shop_categories','=', $CatFromURL->id)->where('city','=', $city->id)->where('active',1)->orderBY("order",'asc')->get() as $shop)
                  <div class="col-md-3 shop-logo">

                     @if ($shop->sale > 0)
                     <div class="wrap">
                             <span class="ribbon6">{{ $shop->sale }}{{ trans('shops.sale') }}</span>
                     </div>
                     @endif


                     <a href="{{url('/ShopsDetails')}}/{{$shop->id}}" class="tt-img-box">
                     <img src="images/loader.svg" data-src="{{$shop->GetThumb()}}" alt="">
                     </a>


                     <div class="aa text-center">
                        <h4 class="tt-title">{{$shop->Get_Trans(App::getLocale(),'name')}}</h4>
                     </div>


                  </div>
                  @endforeach
               </div>
            </div>
               @else
               <div class="col-12 col-sm-12 col-md-12 col-12-575width">
                  <div class="row tt-img-box-listing">
                  @foreach(\App\Models\Shops::where('deleted_at','=', NULL)->where('shop_categories','=', $CatFromURL->id)->where('city','=', $city->id)->where('active',1)->orderBY("order",'asc')->get() as $shop)
                  <div class="col-md-2 shop-logo">

                     @if ($shop->sale > 0)
                     <div class="wrap">
                             <span class="ribbon6">{{ $shop->sale }}{{ trans('shops.sale') }}</span>
                     </div>
                     @endif
                     <a href="{{url('/ShopsDetails')}}/{{$shop->id}}" class="tt-img-box">
                     <img src="images/loader.svg" data-src="{{$shop->GetThumb()}}" alt="">
                     </a>
                     <div class="aa text-center">
                        <h4 class="tt-title">{{$shop->Get_Trans(App::getLocale(),'name')}}</h4>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
               @endif

         </div>
              </div>
            </div>
         </div>

@endif




    </div>


























@endsection



@section('script')


@endsection
