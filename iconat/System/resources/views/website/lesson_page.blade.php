@extends('layouts.website', ['title' => trans('general.home')])

@section('content')

    <div class="main-content">
        <!-- Section: inner-header -->
        <section class="inner-header divider layer-overlay overlay-dark-7" data-bg-img="{{url('')}}/assets/site/images/bg/inner-header.jpg">
            <div class="container pt-120 pb-60">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="text-theme-color-yellow font-36"> {{$Paragraph->title}}</h2>
                            <ol class="breadcrumb text-right mt-10 white">
                                <li><a href="{{url("/Home")}}">{{trans("general.home")}}</a></li>
                                <li><a href="#">{{$Paragraph->BookOf->ClassOf->name}}</a></li>
                                <li><a href="#">{{$Paragraph->BookOf->name}}</a></li>
                                <li class="active">{{$Paragraph->title}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Lesson Details -->
        <section>
            <div class="container pt-70 pb-70">
                <div class="row">
                    <div class="col-md-9">
                        <div class="blog-posts single-post">
                            <article class="post clearfix mb-0">
                                <div class="entry-header">
                                    <div class="post-thumb thumb"> <img src="{{$Paragraph->GetImageURL("paragraph_as_jpg")}}" alt="" class="img-responsive img-fullwidth"> </div>
                                </div>
                            </article>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar sidebar-left mt-sm-30">
                            <div class="widget">
                                <h5 class="widget-title line-bottom">{{trans("classes.audio_file")}}</h5>
                                @php $paragraph_as_audio = $Paragraph->GetImageURL("paragraph_as_audio"); @endphp
                                @if(!empty($paragraph_as_audio) && $fp = curl_init($paragraph_as_audio))
                                <audio controls>
                                    <source src="{{$Paragraph->GetImageURL("paragraph_as_audio")}}" type="audio/mpeg">
                                    {{trans("general.your_browser_does_not_support_the_audio_element")}}
                                </audio>
                                @else
                                    <div class="alert alert-warning">{{trans("classes.no_sound_file_available_for_this_lesson")}}</div>
                                @endif
                            </div>
                            <div class="widget">
                                <h5 class="widget-title line-bottom">{{trans("classes.worksheets")}}</h5>
                                <div class="categories">
                                        @forelse($Paragraph->Worksheets()->orderBy("att_id")->get() as $Worksheet)
                                        @if ($loop->first) <ul class="list list-border angle-double-right"> @endif
                                        <li><a target="_blank" href="{{url("/WorkSheetPDF/".$Worksheet->id)}}"><span>({{$Worksheet->att_id}})</span> {{$Worksheet->title}}</a></li>
                                        @if ($loop->last) </ul> @endif
                                        @empty
                                        <div class="alert alert-warning">{{trans("classes.this_lesson_does_not_contain_any_worksheets")}}</div>
                                    @endforelse

                                </div>
                            </div>
                            <div class="widget">
                                <h5 class="widget-title line-bottom">{{trans("classes.sequence_of_lessons")}}</h5>
                                <div class="latest-posts">
                                    @foreach($pre_lessons as $pre_lesson)
                                    <article class="post media-post clearfix pb-0 mb-10">
                                        <a class="post-thumb" href="{{url("/Lesson/".$pre_lesson->id)}}"><img style="max-width: 100px;" src="{{$pre_lesson->GetImageURL("paragraph_as_jpg_thumb")}}" alt=""></a>
                                        <div class="post-right">
                                            <h5 class="post-title mt-0"><a href="{{url("/Lesson/".$pre_lesson->id)}}">{{$pre_lesson->title}}</a></h5>
                                        </div>
                                    </article>
                                    @endforeach
                                        <blockquote class="theme-color-sky pt-20 ">
                                    <article class="post media-post clearfix pb-0 mb-10 ">
                                        <a class="post-thumb" href="JavaScript:Void(0);"><img style="max-width: 100px;" src="{{$Paragraph->GetImageURL("paragraph_as_jpg_thumb")}}" alt=""></a>
                                        <div class="post-right">
                                            <h5 class="post-title mt-0"><a href="JavaScript:Void(0);">{{$Paragraph->title}}</a></h5>
                                        </div>
                                    </article>
                                        </blockquote>
                                    @foreach($next_lessons as $next_lesson)
                                    <article class="post media-post clearfix pb-0 mb-10">
                                        <a class="post-thumb" href="{{url("/Lesson/".$next_lesson->id)}}"><img style="max-width: 100px;" src="{{$next_lesson->GetImageURL("paragraph_as_jpg_thumb")}}" alt=""></a>
                                        <div class="post-right">
                                            <h5 class="post-title mt-0"><a href="{{url("/Lesson/".$next_lesson->id)}}">{{$next_lesson->title}}</a></h5>
                                        </div>
                                    </article>
                                    @endforeach

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection



@section('script')


@endsection