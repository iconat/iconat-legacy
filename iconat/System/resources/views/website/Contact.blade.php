@extends('layouts.website', ['title' => trans('iconat.contact'),'main_classes' => 'home-wrap'])

@section('content')

<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
            <li>{{trans('iconat.contact')}}</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">

 @if (setting('Lat') & setting('Lat'))
     <div class="container-indent">
        <div class="container">
            <div class="contact-map">
                <div id="map"></div>
            </div>
        </div>
    </div>
 @endif



    <div class="container-indent">
        <div class="container container-fluid-custom-mobile-padding">
            <div class="tt-contact02-col-list">
                <div class="row">
                    <div class="col-sm-12 col-md-4 ml-sm-auto mr-sm-auto">
                        <div class="tt-contact-info">
                            <i class="tt-icon icon-f-93"></i>
                            <h6 class="tt-title">{{trans('iconat.contact')}}!</h6>
                            <address>
                               {{setting('general_phone' , "")}}

                            </address>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="tt-contact-info">
                            <i class="tt-icon icon-f-24"></i>
                            <h6 class="tt-title">{{trans('general.visit_our_location')}}</h6>
                            <address>
                               {{setting('address_'.App::getLocale() , "")}}
                            </address>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="tt-contact-info">
                            <i class="tt-icon icon-f-92"></i>
                            <h6 class="tt-title">{{trans('general.email')}}</h6>
                            <address>
                                {{setting('general_email' , "")}}
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-indent">
        <div class="container container-fluid-custom-mobile-padding">
            <form id="contactform" class="contact-form form-default">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="inputName" placeholder="{{trans('general.name')}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" id="inputEmail" placeholder="{{trans('general.email')}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" class="form-control" id="inputSubject" placeholder="{{trans('general.subject')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <textarea  name="message" class="form-control" rows="7" placeholder="{{trans('general.message')}}"  id="textareaMessage"></textarea>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn">{{trans('general.send_message')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>







@endsection



@section('script')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw9YOQ6A0RrWGodIJ6WTZCat1rSwUpfb8&libraries=geometry&sensor=false"></script>


    <script>
    // When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);

function init() {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 12,

        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(31.898043, 35.204269), // Palestine

        scrollwheel:  false,


        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: []
    };

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    var image = '{{url('')}}/assets/site/images/custom/beachflag.png';

     var marker = new google.maps.Marker({
            position: new google.maps.LatLng({{ setting('Lat',"") }}, {{setting('Lin' , "")}}),
            map: map,
            icon : image,
            title: 'Iconat!'
        });
}



    </script>

<script src="{{ asset('assets') }}/global/scripts/app.js" type="text/javascript"></script>


<script src="{{url('')}}/assets/site/js/contact_us.js"></script>

@endsection
