@extends('layouts.website', ['title' => trans('general.home')])

@section('content')

    <div class="main-content">
        <!-- Section: inner-header -->
        <section class="inner-header divider layer-overlay overlay-dark-7" data-bg-img="{{url('')}}/assets/site/images/bg/inner-header.jpg">
            <div class="container pt-120 pb-60">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="text-theme-color-yellow font-36">{{$Book->ClassOf->name}} - {{$Book->name}}</h2>
                            <ol class="breadcrumb text-right mt-10 white">
                                <li><a href="{{url("/Home")}}">{{trans("general.home")}}</a></li>
                                <li><a href="#">{{$Book->ClassOf->name}}</a></li>
                                <li class="active">{{$Book->name}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Course Details -->
        <section>
            <div class="container mt-30 mb-30 pt-40 pb-30">
                <div class="row">
                    <div class="col-md-12 blog-pull-right">
                        <div class="single-service">

                            <div class="row ">
                                <div class="col-md-12">
                                    <h3 class="line-bottom text-theme-color-red text-uppercase mt-0 mb-20">
                                        <span class="text-theme-color-blue">{{$Book->ClassOf->name}} - </span> {{$Book->name}}
                                    </h3>
                                    <section class="layer-overlay overlay-white-7" data-bg-img="images/bg/bg5.jpg">
                                        <div class="container pt-70 pb-40">
                                            <div class="section-content">
                                                <div class="row">
                                                    @foreach($Book->Paragraphs as $Paragraph)
                                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                                        <div class="course-details-box bg-silver-light border-1px clearfix mb-30">
                                                            <div class="thumb">
                                                                <img class="img-fullwidth" alt="" src="{{$Paragraph->GetImageURL("paragraph_as_jpg_thumb")}}">
                                                            </div>
                                                            <div class="course-details clearfix p-20 pt-15">
                                                                <h4 class="price-tag">{{$Paragraph->order}}</h4>
                                                                <h4 class="title font-20 mt-0 mb-0"><a class="text-theme-color-red" href="{{url("/Lesson/".$Paragraph->id)}}">{{$Paragraph->title}}</a></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

@endsection



@section('script')


@endsection