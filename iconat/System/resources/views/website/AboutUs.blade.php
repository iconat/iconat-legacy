
@extends('layouts.website', ['title' => trans('iconat.about_us'),'main_classes' => 'home-wrap'])


@section('content')


   
<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
            <li>{{trans('iconat.about_us')}}</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">


    <div class="nomargin container-indent">

        <div class="row">

        <div class="col-md-6">

            @php
            $Obj = \App\Models\SettingsMediable::where('key','about_img')->first();
            $Img = NULL;
            if($Obj){
            $S = $Obj->lastMedia('about_img');
        }
        if($Obj && $S){
        $Img = $S->getUrl();
    }
@endphp

        

                 <div class="tt-about-box" style="background-image: url({{$Img}})">
                <img class="img-mobile" src="{{$Img }}" alt="">
                        <div class="col-md-8">
                            
                            <p>
                                {!! setting('about_us_text_'.\App::getLocale() , "") !!}
                            </p>
                            
                        </div>
            </div>
            
        </div>
        <div class="col-md-6">
             <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">

                    <div style="height: auto; margin: 50px;"></div>
                    <h1 class="tt-title-subpages noborder">{{ setting('about_us_title_'.\App::getLocale() , "")}}</h1>
                    <div class="tt-box-thumb-listing">
                        <div class="row">
                @foreach(App\Models\Whyus::where('deleted_at','=', NULL)->orderBY("order",'asc')->get() as  $about)

                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="tt-box-thumb">
                                    <a href="#" class="tt-img"><img src="images/loader.svg" data-src="images/custom/services-img-01.jpg" alt=""></a>
                                    <h6 class="tt-title"><a href="#">{{$about->Get_Trans(\App::getLocale(),"name") ?? NULL}}</a></h6>
                                    <p>{!! $about->Get_Trans(\App::getLocale(),"text") ?? NULL !!} </p>
                                </div>
                            </div>

                     @endforeach

                           
                            
                          
                         
                            
                           


                        </div>
                    </div>


                    
                </div>
            </div>

        </div>

        </div>


    </div>






</div>


@endsection



@section('script')


@endsection