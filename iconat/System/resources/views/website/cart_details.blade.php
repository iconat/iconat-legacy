@php
    $cartCollection = \Cart::session(config("Cart_ID"));
    $CartArray = $cartCollection->getContent();
@endphp
<!-- Cart Starts-->
<section class="shop-wrap cart_changes">
        <!-- Shopping Cart Starts -->
        <div class="cart-table">
            <form class="cart-form">
                <table class="product-table">
                    <thead class="">
                    <tr>
                        <th>{{trans("products.product_detail")}}</th>
                        <th></th>
                        <th>{{trans("products.product_price")}}</th>
                        <th>{{trans("products.quantity")}}</th>
                        <th>{{trans("products.total_price")}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($CartArray->count() > 0)
                    @foreach($CartArray as $P)
                    @php
                        $Item = \App\Models\Products::find($P->id);
                    @endphp
                    @if($Item)
                    <tr>
                        <td class="image">
                            <div class="white-bg">
                                <img src="{{$Item->GetThumb()}}" alt="">
                            </div>
                        </td>
                        <td class="description">
                            @if(false)
                            <div class="rating">
                                <span class="star active"></span>
                                <span class="star active"></span>
                                <span class="star active"></span>
                                <span class="star active"></span>
                                <span class="star active"></span>
                            </div>
                            <h6 class="fsz-12 gray-color"> Overall Rating : 5/5 </h6>
                            <div class="divider-2"></div>
                            @endif
                            <h3 class="product-title no-margin">  <strong>{{$Item->Get_Trans(App::getLocale(),'name')}} </strong>  </h3>
                            <h6>{{$Item->Categorie->Get_Trans(App::getLocale(),'name')}}</h6>
                        </td>
                        <td>
                            <div class="price fontbold-2">
                                <strong class="fsz-20">{{$P->price}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i> </strong>
                                @if(!empty($Item->real_price)) <del class="light-font"> <b>{{$Item->Get_Price_Depend_Currency($Item->real_price)}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i></b> </del> @endif
                            </div>
                        </td>
                        <td>
                            <div class="prod-btns fontbold-2">
                                <div class="quantity">
                                    <button class="btn minus qty_changes" r="m" type="button" p="{{$Item->id}}"><i class="fa fa-minus-circle"></i></button>
                                    <input title="Qty" readonly value="{{$P->quantity ?? 0}}" class="form-control qty" type="text">
                                    <button class="btn plus qty_changes" r="p" type="button" p="{{$Item->id}}"><i class="fa fa-plus-circle"></i></button>
                                </div>
                                <div class="sort-dropdown">
                                    {{$Item->Unit->Get_Trans(App::getLocale(),'name')}}
                                </div>
                            </div>
                        </td>
                        <td>
                            <strong class="clr-txt fsz-20 fontbold-2">{{$P->getPriceSum()}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i></strong>
                            <a href="javascript:void(0)" p="{{$Item->id}}" Dtitle="{{trans("general.are_you_sure")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_name",["name" => $Item->Get_Trans(App::getLocale(),'name')])}}" class="remove fa fa-close clr-txt DeleteCartItem"></a>
                        </td>
                    </tr>
                    @endif
                    @endforeach
                    @else
                        <tr><td colspan="5"><div class="alert alert-warning">{{trans("products.your_cart_is_empty")}}</div></td></tr>
                    @endif
                    </tbody>
                </table>

                <div class="continue-shopping">
                    <div class="left">
                        <h6>{{trans("products.if_you_have_a_coupon,_enter_its_code_here_please")}} </h6>
                        <div class="coupan-form">
                            <input class="form-control" >
                            <button class="btn" type="submit"> {{trans("products.apply_code")}} </button>
                        </div>
                    </div>
                    <div class="right"> <strong class="fsz-20 fontbold-2">{{trans("products.total")}} : <span class="clr-txt"> {{$cartCollection->getTotal()}} <i class='fa fa-{{config("Default_currency_Icon")}}'></i> </span> </strong> </div>
                </div>
            </form>
        </div>
        <!-- / Shopping Cart Ends -->
</section>
<!-- / Cart Ends -->