@extends('layouts.website', ['title' => trans('shops.the_shop'),'main_classes' => 'home-wrap'])
@section('content')
<div class="tt-breadcrumb">
   <div class="container">
      <ul>
         <li><a href="{{url('')}}">{{trans('general.home')}}</a></li>
         <li>{{$CatId->Get_Trans(App::getLocale(),'name')}}</li>
      </ul>
   </div>
</div>
<div id="tt-pageContent">
   <div class="container-indent ">
      <div class="container-fluid-custom">
         <div class="tt-block-title">
            <h2 class="tt-title">{{$CatId->Get_Trans(App::getLocale(),'name')}}</h2>
         </div>
         <div class="row">
            <div class="col-6 col-sm-12 col-md-6 col-12-575width">
               <div class="tt-post-img">
                  <div class="tt-slider-blog arrow-location-center tt-arrow-type-02 slick-animated-show-js" style=" max-height:560px !important;">
                     @php
                     $Slides = \App\Models\Slider_Category::where('active',1)->where('category_id','=', $CatId->id)->orderBY("order",'asc')->get();
                     @endphp
                     @if(count($Slides) > 0)
                     <div class="">
                        <div class="tt-post-img" style="max-height:560px !important;">
                           <div class="tt-slider-blog arrow-location-center tt-arrow-type-02 slick-animated-show-js">
                              @foreach($Slides as $Slide)
                              @php
                              $MainImage = $Slide->MainImage();
                              $MainVideo = $Slide->GetvideoURL('video_cat');
                              $UrlVideo = $Slide->link_video;
                              $SliderText = $Slide->GetSliderText(App::getLocale());
                              @endphp
                              @if($MainImage)
                              <div  class="aaa2">
                                 <a href="#" class="tt-promo-box">
                                    <img src="{{url('')}}/assets/site/images/loader.svg" data-src="{{$MainImage}}" alt="">
                                    @if(!empty($SliderText))
                                    <div class="tt-description">
                                       <div class="tt-description-wrapper">
                                          <div class="tt-background"></div>
                                          @if(!empty($SliderText))
                                          <div class="tt-title-large">{{$SliderText}}</div>
                                          @endif
                                       </div>
                                    </div>
                                    @endif
                                 </a>
                              </div>
                              @endif
                              @if($MainVideo)
                              <div  class="aaa2">
                                 <a href="#" class="tt-promo-box">
                                    <div class="tt-video-block">
                                 <a href="#" class="link-video"></a>
                                 <video class="movie" src="{{$MainVideo}}" poster=""></video>
                                 </div>
                                 </a>
                              </div>
                              @endif
                              @if($UrlVideo)
                              <div class="aaa2">
                           <iframe width="100%" height="560px" src="{{$UrlVideo}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                              @endif
                              @endforeach



                           </div>
                        </div>
                     </div>
                     @endif
                  </div>
               </div>
            </div>
            <div class="col-6 col-sm-12 col-md-6 col-12-575width">
               <div class="row tt-img-box-listing">
                  @php
                   $a = [];
                  @endphp
                  @foreach(\App\Models\Shops::where('deleted_at','=', NULL)->where('shop_categories','=', $CatId->id)->orderBY("id",'desc')->get() as $shop)
                  $a = $shop;

                  <div class="col-md-3">
                     <a href="{{url('/ShopsDetails')}}/{{$shop->id}}" class="tt-img-box">
                     <img src="images/loader.svg" data-src="{{$shop->GetThumb()}}" alt="">
                     </a>
                     <div class="aa text-center">
                        <h4 class="tt-title">{{$shop->Get_Trans(App::getLocale(),'name')}}</h4>
                     </div>
                  </div>

                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
@endsection
