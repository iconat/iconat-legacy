@extends('layouts.website', ['title' => trans('products.checkout')])

@section('content')



    <!--Breadcrumb Section Start-->
    <section class="breadcrumb-bg">
        <div class="theme-container container ">
            <div class="site-breadcumb white-clr">
                <h2 class="section-title"> <strong class="clr-txt">{{trans("products.checkout")}} </strong>  </h2>
                <ol class="breadcrumb breadcrumb-menubar">
                    <li> <a href="{{url("/")}}"> {{trans("general.home")}} </a> {{trans("products.checkout")}}  </li>
                </ol>
            </div>
        </div>
    </section>
    <!--Breadcrumb Section End-->

    <!-- Checkout Starts-->
    <section class="checkout-wrap sec-space">
        <div class="container">
            <div class="panel-group chk-panel" id="accordion">
                <div class="panel">
                    <div class="chk-heading">
                        <a class="fsz-30" data-toggle="collapse" data-parent="#accordion" href="#account_collapse">
                            <span class="light-font">01. </span> <strong>{{trans("products.account")}}</strong>
                        </a>
                    </div>
                    <div id="account_collapse" class="panel-collapse collapse in">
                        <div class="chk-body pt-15 block-inline">
                            @if(empty(\Auth::user()))
                            <div class="col-md-6">
                                <form class="chk-form" >
                                    <h2 class="title-1">{{trans("products.check_as_a_guest_or_register")}}</h2>
                                    <p>{{trans("products.register_with_us_for_future_convenience")}}:</p>
                                    <div class="form-group block-inline ">
                                        <label class="radio-inline title-1"> <input type="radio" id="check_as_guest" value=""> <span> {{trans("products.continue_your_purchase_as_a_guest")}} </span>  </label>
                                        <label class="radio-inline title-1"> <input type="radio" id="register_a_new_membership" value=""> <span> {{trans("general.register_a_new_membership")}} </span>  </label>
                                    </div>
                                    <h2 class="title-1"> {{trans("general.register_and_save_time!")}} </h2>
                                    <ul>
                                        <li> <span class="fa fa-square"></span> {{trans("products.fast_and_easy_check_out")}} </li>
                                        <li> <span class="fa fa-square"></span> {{trans("products.easy_access_to_your_order_history_and_status")}} </li>
                                    </ul>
                                    <div class="form-group block-inline text-right">
                                        <button id="ContinueCheckOut" class="theme-btn-sm-2 btn submit-btn" type="button"> <b> {{trans("general.continue")}} </b> </button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-6">
                                <form class="LoginForm">
                                    <h2 class="title-1"> {{trans("general.already_registed")}} </h2>
                                    <p class=""> {{trans("general.please_log_in_below")}} : </p>
                                    <div class="form-group block-inline">
                                        <label> {{trans("general.email_address")}} <span class="red-clr"> * </span> </label>
                                        <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="" name="username" class="form-control">
                                    </div>
                                    <div class="form-group block-inline">
                                        <label> {{trans("general.password")}} <span class="red-clr"> * </span> </label>
                                        <input type="password" title="" data-placement="bottom" data-toggle="tooltip" value="" name="password" class="form-control">
                                    </div>
                                    <label class="red-clr">* {{trans("general.required_filelds")}}</label>
                                    <div class="form-group block-inline text-right">
                                        <b class="black-color fpw"> {{trans("general.did_you_forget_your_password")}} </b>
                                        <button class="theme-btn-sm-3 btn submit-btn" type="submit"> <b> {{trans("general.login")}} </b> </button>
                                    </div>
                                </form>
                            </div>
                            @else
                            <div class="col-md-12">
                                {{trans("products.you_have_signed_in,_your_purchase_is_being_linked_with_your_membership",['name' => \Auth::user()->name])}}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="panel">
                    <div class="chk-heading">
                        <a class="fsz-30" data-toggle="collapse" data-parent="#accordion" href="#shipping_information_collapse">
                            <span class="light-font">02.</span> <strong>{{trans("deliverymethods.shipping_information")}} </strong>
                        </a>
                    </div>
                    <div id="shipping_information_collapse" class="panel-collapse collapse">
                        <div class="chk-body pt-15 block-inline">
                            <form id="ShippingInformationForm">
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.contact_name")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['contact_name'] ?? NULL}}" name="contact_name" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.country/region")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['country'] ?? NULL}}" name="country" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.street_address")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['street_address'] ?? NULL}}" name="street_address" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.apartment_suite_unit_etc")}} </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['second_address'] ?? NULL}}" name="second_address" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.city")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['city'] ?? NULL}}" name="city" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.state_province_region")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['region'] ?? NULL}}" name="region" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("deliverymethods.zip/postal_code")}} </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['postal_code'] ?? NULL}}" name="postal_code" class="form-control">
                                </div>
                                <div class="form-group block-inline">
                                    <label> {{trans("general.mobile")}} <span class="red-clr"> * </span> </label>
                                    <input type="text" title="" data-placement="bottom" data-toggle="tooltip" value="{{$ShippingInformation['mobile'] ?? NULL}}" name="mobile" class="form-control">
                                </div>

                                <label class="red-clr">* {{trans("general.required_filelds")}}</label>
                                <div class="form-group block-inline text-right">
                                    <button class="theme-btn-sm-2 btn submit-btn" type="submit"> <b> {{trans("general.continue")}} </b> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="chk-heading">
                        <a class="fsz-30" data-toggle="collapse" data-parent="#accordion" href="#deliverymethods_collapse">
                            <span class="light-font">03.</span> <strong> {{trans("deliverymethods.shipping_method")}} </strong>
                        </a>
                    </div>
                    <div id="deliverymethods_collapse" class="panel-collapse collapse">
                        <div class="chk-body pt-15 block-inline">
                            <form id="DeliveryMethod" class="chk-form">
                                <h2 class="title-1">{{trans("deliverymethods.please_choose_shipping_method_please")}}:</h2>
                                <div class="form-group block-inline ">
                                    @foreach(\App\Models\DeliveryMethods::where('active', 1)->get() as $DeliveryMethod)
                                        <label class="radio-inline title-1"> <input name="delivery_method" type="radio" value="{{$DeliveryMethod->id}}"  @if(!empty($Cart["Delivery_Method"]) && ($Cart["Delivery_Method"] == $DeliveryMethod->id)) checked @endif > <span> {{$DeliveryMethod->Get_Trans(\App::getLocale(),"name")}} </span>  </label>
                                    @endforeach
                                </div>
                                <div class="form-group block-inline text-right">
                                    <button class="theme-btn-sm-2 btn submit-btn" type="submit"> <b> {{trans("general.continue")}} </b> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="chk-heading">
                        <a class="fsz-30" data-toggle="collapse" data-parent="#accordion" href="#cart_details_collapse">
                            <span class="light-font">04. </span> <strong>{{trans("products.order_review")}} </strong>
                        </a>
                    </div>
                    <div id="cart_details_collapse" class="panel-collapse collapse">
                        <div id="cart_details" class="chk-body pt-15 block-inline">
                            @include('website.cart_details')
                        </div>
                    </div>

                </div>
                <div class="panel">
                    <div class="chk-heading">
                        <a class="fsz-30" data-toggle="collapse" data-parent="#accordion" href="#PaymentMethodcollapse">
                            <span class="light-font">05. </span> <strong>{{trans("products.payment_method")}} </strong>
                        </a>
                    </div>
                    <div id="PaymentMethodcollapse" class="panel-collapse collapse">
                        <div class="chk-body pt-15 block-inline">
                            <div class="col-md-12">
                                <form id="PaymentMethod" class="chk-form">
                                    <h2 class="title-1" id="PSYPM">{{trans("products.please_select_your_payment_method")}}:</h2>
                                    <div class="form-group block-inline ">
                                        @foreach(\App\Models\PayMethods::where('active', 1)->orderBY("order")->get() as $PayMethod)
                                            <label class="radio-inline title-1"> <input name="paymethod" type="radio" value="{{$PayMethod->id}}" @if(!empty($Cart["Payemnt_Method"]) && ($Cart["Payemnt_Method"] == $PayMethod->id)) checked @endif > <span> {{$PayMethod->Get_Trans(\App::getLocale(),"name")}} </span>  <div class="F"></div></label>

                                        @endforeach
                                    </div>

                                    <div class="form-group block-inline text-right">
                                        <button class="theme-btn-sm-2 btn submit-btn" type="submit"> <b> {{trans("general.continue")}} </b> </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shp-btn col-sm-12 text-center">
                    <button class="theme-btn-3 btn" id="Checkout_Now" type="button"> <b> {{trans("products.checkout_now")}} </b> </button>
                </div>
            </div>
        </div>
    </section>
    <!-- / Checkout Ends -->


@endsection



@section('script')


@endsection