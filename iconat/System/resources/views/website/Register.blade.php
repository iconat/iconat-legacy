@extends('layouts.website', ['title' => setting('Category_title' , "")])

@section('content')



<div class="tt-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.html">Home</a></li>
            <li>Create An Account</li>
        </ul>
    </div>
</div>
<div id="tt-pageContent">
    <div class="container-indent">
        <div class="container">
            <h1 class="tt-title-subpages noborder">CREATE AN ACCOUNT</h1>
            <div class="tt-login-form">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-10">
                        <div class="tt-item">
                            <h2 class="tt-title">PERSONAL INFORMATION</h2>
                            <div class="form-default">
















                                <form id="UserForm" Ref="" novalidate="novalidate">
                                    <div class="form-group">
                                        <label for="loginInputName">FIRST NAME *</label>
                                        <div class="tt-required">* Required Fields</div>
                                        <input type="text" name="name" class="form-control" id="loginInputName" placeholder="Enter First Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="loginLastName">LAST NAME *</label>
                                        <input type="text" name="username" class="form-control" id="loginLastName" placeholder="Enter Last Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="loginInputEmail">E-MAIL *</label>
                                        <input type="text" name="email" class="form-control" id="loginInputEmail" placeholder="Enter E-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="loginInputPassword">PASSWORD *</label>
                                        <input type="password" name="password" class="form-control" id="loginInputPassword" placeholder="Enter Password">
                                    </div>

                                    <!-- <div class="form-group">
                                        <label for="shopCompanyName" class="control-label">COMPANY NAME</label>
                                        <input type="text" class="form-control" id="shopCompanyName">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopCompanyName" class="control-label">COUNTRY *</label>
                                        <select class="form-control" name="city">
                                            <option>Country</option>
                                            <option>Country 02</option>
                                            <option>Country 03</option>
                                            <option>Country 04</option>
                                            <option>Country 05</option>
                                            <option>Country 06</option>
                                            <option>Country 07</option>
                                            <option>Country 08</option>
                                            <option>Country 09</option>
                                            <option>Country 10</option>
                                            <option>Country 11</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="shopAddress" class="control-label">ADDRESS *</label>
                                        <input type="text" class="form-control" id="shopAddress">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopTown" class="control-label">TOWN / CITY *</label>
                                        <input type="text" class="form-control" id="shopTown">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopState" class="control-label">STATE / COUNTY *</label>
                                        <input type="text" class="form-control" id="shopState">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopPostCode" class="control-label">POSTCODE / ZIP *</label>
                                        <input type="text" class="form-control" id="shopPostCode">
                                    </div>
                                    <div class="form-group">
                                        <label for="shopPhone" class="control-label">PHONE</label>
                                        <input type="text" class="form-control" id="shopPhone">
                                    </div> -->
                                  <!--   <div class="checkbox-group">
                                        <input type="checkbox" id="checkBox11" name="checkbox">
                                        <label for="checkBox11">
                                            <span class="check"></span>
                                            <span class="box"></span>
                                            Set as deafult address?
                                        </label>
                                    </div> -->



                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="form-group">
                                                <button class="btn btn-border" type="submit">CREATE</button>
                                            </div>
                                        </div>
                                        <div class="col-auto align-self-center">
                                            <div class="form-group">
                                                <ul class="additional-links">
                                                    <li>or <a href="#">Return to Home</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </form>






                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection



@section('script')
<script src="{{url('')}}/assets/site/js/custom.js"></script>

@endsection