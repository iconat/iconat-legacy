@php
    $cartCollection = \Cart::session(config("Cart_ID"));
    $CartArray = $cartCollection->getContent();
@endphp

    <a href="#"> <img alt="" src="{{url('')}}/assets/site/img/icons/cart-icon-rtl.png"/> </a>
    <span class="cnt crl-bg">{{count($CartArray)}}</span> <span class="price">{{$cartCollection->getTotal()}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i> </span>

    <ul class="pop-up-box cart-popup">
        @if($CartArray->count())
        @foreach($CartArray as $P)
        @php
            $Item = \App\Models\Products::find($P->id);
        @endphp
        @if($Item)
        <li class="cart-list">
            <div class="cart-img"><img src="{{$Item->GetThumb()}}" alt=""></div>
            <div class="cart-title">
                <div class="fsz-16">
                    <a href="#"> <span class="light-font"> {{$Item->Get_Trans(App::getLocale(),'name')}}  </span></a>
                    <h6 class="sub-title-1">{{$Item->Categorie->Get_Trans(App::getLocale(),'name')}}</h6>
                </div>
                <div class="price">
                    <strong class="clr-txt">{{$P->price}}<i class="fa fa-{{config("Default_currency_Icon")}}"></i> </strong>
                    <span class="light-font"> * {{$P->quantity}} {{$Item->Unit->Get_Trans(App::getLocale(),'name')}}</span>
                </div>
            </div>
            <div class="close-icon DeleteCartItem" p="{{$Item->id}}" Dtitle="{{trans("general.are_you_sure")}}" Dcontent="{{trans("general.are_you_sure_you_want_to_delete_name",["name" => $Item->Get_Trans(App::getLocale(),'name')])}}"><i class="fa fa-close clr-txt"></i></div>
        </li>
        @endif
        @endforeach
        @else
            <li class="cart-list">
                <div class="alert alert-warning" style="display: block;">
                    {{trans("products.no_items_have_been_added_to_your_cart_yet")}}
                </div>
            </li>
        @endif

        @if(!empty($CartArray))
        <li class="cart-list sub-total">
            <div class="pull-left">
                <strong style="color: #000;">{{trans("products.total_cost")}}</strong>
            </div>
            <div class="pull-right">
                <strong class="clr-txt">{{$cartCollection->getTotal()}} <i class="fa fa-{{config("Default_currency_Icon")}}"></i></strong>
            </div>
        </li>

        <li class="cart-list buttons">
            @if(false)
            <div class="pull-left">
                <button href="{{url("/Product/Cart")}}" class="theme-btn-sm-2">{{trans("products.go_to_cart")}}</button>
            </div>
            @endif
            <div class="pull-right">
                <a href="{{url("/Cart/Checkout")}}" class="theme-btn-sm-3"> {{trans("products.go_for_payment")}} </a>
            </div>
        </li>
        @endif
    </ul>

