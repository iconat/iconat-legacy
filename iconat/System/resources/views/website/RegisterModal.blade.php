<form id="UserForm" Ref="{{$Ref}}">
        <div class="col-md-8 col-md-offset-2">
            <div class="form-group ">
                <label class="color-black">{{trans("general.fullname")}}</label>
                <input name="name" class="form-control r-input" placeholder="{{trans("general.fullname")}}" type="text">
            </div>
            <div class="form-group">
                <label class="color-black">{{trans("general.email_address")}}</label>
                <input name="email" class="form-control r-input" placeholder="{{trans("general.email_address")}}" type="text">
            </div>
            <div class="form-group">
                <label class="color-black">{{trans("general.password")}}</label>
                <input name="password" class="form-control r-input" placeholder="{{trans("general.password")}}" type="password">
            </div>
        </div>
</form>