@extends('invs.app')

@section('content')

<style type="text/css">
  html { overflow-y: hidden; }

</style>
				<section class="flexbox-container" style="align-items: start;">

					<div class="col-md-4 col-xs-1 login-back-line">
						<div class="row">
		    				<p class="line_login_register_forgot">&nbsp;</p>
		    			</div>
		    		</div>

					<div class="col-md-4 col-xs-10   box-shadow-2 p-0"><!--  offset-md-4 offset-xs-1 --><!-- <div class="row"> -->
						<div class="card border-grey m-0"><!-- border-lighten-3 -->
							<div class="card-header1 no-border">
								<div class="card-title text-xs-center">
									<div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png" style="wid3th: 190px;" alt="branding logo"></div>
								</div>
								<h6 class="card-subtitle line-on-side text-xs-center pt-2" style="border-bottom: 0px;"><span>تفقد حالة طلب تم تقديمه</span></h6>
							</div>
							<div class="card-body collapse in">
								<div class="card-block">
									<div  class="form">
										
								<form id="check_form"> 
									 
									<h4  id="result"  style="border-bottom: 0px; background-color: #da4453; color: #ffffff; text-align: center;"><span></span></h4>

											<div class="form-group">
												<!--label for="email">Email</label-->
												<input id="email" class="form-control round" style="text-align: center;" placeholder="معرف الدخول" name="email">
											</div>



											<div class="form-group">
												<!--label for="Password">Password</label-->
												<input id="password" class="form-control round" style="text-align: center;" placeholder="رمز مرورك السري" name="password" type="Password">
											</div>
											<!-- <input type="checkbox" id="remember_me" name="_remember_me" checked />
											<label for="remember_me">Keep me logged in</label> -->
											<div class="form-actions">

												<button type="submit" class="btn btn-warning">
													تفقد الحالة
												</button>

											</div>


										</form> 
									</div>
								</div>

								<div class="card-footer">
									      <div class="row">
                        <div class="col-md-4">
                            <a href="{{route('invs_reg')}}" class="btn btn-info btn-block">اطلب بياناتك</a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{route('login')}}" class="btn btn-green btn-block">تسجيل الدخول</a>
                        </div>
                        <div class="col-md-4">
                           <a href="{{route('forget')}}" class="btn btn-danger btn-block">نسيت بياناتك؟</a>
                        </div>
                    </div>


								</div>

							</div>
						</div>

						<!-- </div> -->
					</div>
					
					

					<div class="col-md-4 col-xs-1 login-back-line">
						<div class="row">
		    				<p class="line_login_register_forgot">&nbsp;</p>
		    			</div>
		    		</div>
		    		
		    		
		   

				</section>
				
					
			
			
					 @endsection

					  @section('script')


                <script src="{{url('admin')}}/js/check.js" type="text/javascript"></script>

                @endsection