
@extends('invs.app')

@section('content')
<style type="text/css">
  html { overflow-y: hidden; }

</style>

              <section class="flexbox-container" style="align-items: start;">

                <div class="col-md-4 col-xs-1 login-back-line">
                    <div class="row">
                        <p class="line_login_register_forgot">&nbsp;</p>
                    </div>
                </div>
                
                <div class="col-md-4 col-xs-10 box-shadow-2 p-0">
                    <div class="card border-grey m-0">
                        <div class="card-header1 no-border">
                            <div class="card-title text-xs-center">
                                <div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png" alt="Homyt"></div>
                            </div>
                            <h6 class="card-subtitle line-on-side text-xs-center  pt-2"><span>نسيت رمز المرور السري</span></h6>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <div  class="form">
                      <form > 

                                    <div class="form-group">
                                     <input id="email" class="form-control round" style="text-align: center;" placeholder="ما هو معرف دخولك الخاص؟ " name="Email" type="email">
                                 </div>

                                 <div class="form-actions">

                                     <button type="submit" class="btn btn-danger">
                                       استعادة
                                  </button>
                              </div>

                          </form> 

                      </div>
                  </div>

                  <div class="card-footer">

                                  <div class="row">
                        <div class="col-md-4">
                            <a href="{{route('invs_reg')}}" class="btn btn-info btn-block">اطلب بياناتك</a>
                        </div>
                        <div class="col-md-4">
                          <a href="{{route('check')}}" class="btn btn-warning btn-block">تفقد حالة طلبك</a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{route('login')}}" class="btn btn-green btn-block">تسجيل الدخول</a>
                        </div>
                        
                    </div>



                
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xs-1 login-back-line">
        <div class="row">
            <p class="line_login_register_forgot">&nbsp;</p>
        </div>
    </div>

</section>

	 @endsection