@extends('invs.app')

@section('content')

<style type="text/css">
  html { overflow-y: hidden; }

</style>
				<section class="flexbox-container" style="align-items: start;">

					<div class="col-md-4 col-xs-1 login-back-line">
						<div class="row">
		    				<p class="line_login_register_forgot">&nbsp;</p>
		    			</div>
		    		</div>

					<div class="col-md-4 col-xs-10   box-shadow-2 p-0"><!--  offset-md-4 offset-xs-1 --><!-- <div class="row"> -->
						<div class="card border-grey m-0"><!-- border-lighten-3 -->
							<!-- <div class="card-header1 no-border">
								<div class="card-title text-xs-center">
									<div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png" style="width: 190px;" alt="branding logo"></div>
								</div>
								<h6 class="card-subtitle line-on-side text-xs-center font-large-1 pt-2" style="border-bottom: 0px;"><span>رجاءاً أدخل بيانات نظامك</span></h6>
							</div> -->
							<div class="card-header1 no-border">
                            <div class="card-title text-xs-center">
                                <div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png" alt="Homyt"></div>
                            </div>
                            <h6 class="card-subtitle line-on-side text-xs-center  pt-2"><span>رجاءاً أدخل بيانات نظامك</span></h6>

                        </div>

							<div class="card-body collapse in">
								<div class="card-block">
									<div  class="form">
										
		<form id="form_login"> 

			<h4 id="msg" style="border-bottom: 0px; background-color: #f6bb42; color: #ffffff; text-align: center;"><span></span></h4>

	         <div class="form-group">
					<!--label for="email">Email</label-->
			  <input id="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} round" placeholder=" معرف الدخول " name="email" value="{{ old('email') }}"  autofocus style="text-align: center;">

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
			</div>



				<div class="form-group">
			   <input id="password" type="password" style="text-align: center;" placeholder="رمز المرور السري" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} round" name="password" >

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
				</div>


				<!-- <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> تذكر جهازي (لا ينصح به)
                </label> -->


				<div class="form-actions">

					<button type="submit" class="btn btn-green">
						تسجيل الدخول 
					</button>

				</div>
		</form> 
									</div>
								</div>


								<div class="card-footer">
									        <div class="row">
                        <div class="col-md-4">
                            <a href="{{route('invs_reg')}}" class="btn btn-info btn-block">اطلب بياناتك</a>
                        </div>
                        <div class="col-md-4">
                          <a href="{{route('check')}}" class="btn btn-warning btn-block">تفقد حالة طلبك</a>
                        </div>
                        <div class="col-md-4">
                           <a href="{{route('forget')}}" class="btn btn-danger btn-block">نسيت بياناتك؟</a>
                        </div>
                        
                    </div>



					
								</div>

							</div>
						</div>

						<!-- </div> -->
					</div>

					<div class="col-md-4 col-xs-1 login-back-line">
						<div class="row">
		    				<p class="line_login_register_forgot">&nbsp;</p>
		    			</div>
		    		</div>

				</section>
				 @endsection

				   @section('script')


                <script src="{{url('admin')}}/js/login.js" type="text/javascript"></script>

                @endsection