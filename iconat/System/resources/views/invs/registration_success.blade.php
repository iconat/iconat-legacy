@extends('invs.app')

@section('content')
<style type="text/css">
  html { overflow-y: hidden; }

</style>
                <section class="flexbox-container" style="align-items: start; margin: 52px 0 !important;">

                    <div class="col-md-4 col-xs-1 login-back-line">
                        <div class="row">
                            <p class="line_login_register_forgot">&nbsp;</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-10   box-shadow-2 p-0"><!--  offset-md-4 offset-xs-1 --><!-- <div class="row"> -->
                        <div class="card border-grey m-0"><!-- border-lighten-3 -->
                          <div class="card-header1 no-border">
                            <div class="card-title text-xs-center">
                                <div class=""><img src="{{url('admin')}}/app-assets/images/logo/robust-logo-light-small.png" alt="Homyt"></div>
                            </div>
                            <h6 class="card-subtitle line-on-side text-xs-center  pt-2"><span>لقد تم تقديم طلبك بنجاح</span></h6>

                        </div>
                         <br>
                        <div>
                            
                             <h4 id="fileHelp" style="text-align: center; font-color: #ffffff;"> شكراً جزيلاً, لقد تم إستلام طلبك الخاص. <br><br> <br> طلبك تحت المراجعة حالياً. فريق هوميت سيقوم بمراجعة طلبك وسنقوم بالرد على طلبك خلال مدة أقصاها شهر عمل من تاريخ تسجيلك, ستصلك رسالة شخصية على هاتفك المحمول وبريد إلكتروني  عند الإنتهاء من معالجة طلبك. <br><br> كما ويمكنك دوماً تفقد حالة طلبك يدوياً.. عن طريق إختيار الزر أدناه (تفقد حالة طلبك) </h4>

                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block" style="text-align: center;">
                              
                            </div>
                      </div>

                     


                             
                        </div>
                        
                 <div class="card-footer" style="text-align: center;">
                       <div class="row">
                         <div class="col-md-4">
                            <a href="{{route('login')}}" class="btn btn-green btn-block">تسجيل الدخول</a>
                        </div>
                        
                        <div class="col-md-4">
                            <a href="{{route('check')}}" class="btn btn-warning btn-block">تفقد حالة طلبك</a>
                        </div>
                       
                        <div class="col-md-4">
                            <a href="{{route('forget')}}" class="btn btn-danger btn-block">نسيت بياناتك؟</a>
                        </div>
                    </div>
                         
                </div>

                        <!-- </div> -->
                    </div>

                    <div class="col-md-4 col-xs-1 login-back-line">
                        <div class="row">
                            <p class="line_login_register_forgot">&nbsp;</p>
                        </div>
                    </div>

                </section>
                 @endsection