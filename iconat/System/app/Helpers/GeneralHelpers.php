<?php


if (!function_exists('GetTextDashed')) {

    /**
     * say hello
     *
     * @param string $name
     * @return string
     */
    function GetTextDashed($sentence)
    {
        $sentence = str_replace('.','',$sentence); //Remove dots from text

        $new_array = explode(' ', $sentence);
        foreach ($new_array as $key=>$value){
            if(strlen($value) < 2){
                unset($new_array[$key]);
            }
        }
        return implode('_', $new_array);
    }
}






