<?php

namespace App;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class User extends Authenticatable
{

    use EntrustUserTrait; // add this trait to your user model
    use Notifiable;
    use Mediable;
    use SoftDeletes;
    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','ref','active'
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function InfoUser()
    {
    return $this->hasOne('App\Models\InfoUser','user_id');
   }



    public function get_Full_name()
    {
        $name = $this->name;

        return $name;
    }

    public function get_Short_name()
    {
        $name = $this->name;

        return $name;
    }

    public function get_desc_name()
    {
        $name = trans("general.manager");

        return $name;
    }

    public function get_personal_image()
    {


        if ($this->lastMedia("main_img_thumb")) {
            $img = $this->lastMedia("main_img_thumb")->getUrl();
        }else{
            $img = url(setting('default_image', ""));
        }


        return $img;
    }




    public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }
    public function GetImageUrlXS()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath() : NULL;
    }
    public function GetImageUrlSM()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath() : NULL;
    }

    public function GetImageUrlMD()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath() : NULL;
    }

    public function GetImageUrlLG()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath() : NULL;
    }










    public function GetImageUrlXS2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath() : NULL;
    }
    public function GetImageUrlSM2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath() : NULL;
    }

    public function GetImageUrlMD2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath() : NULL;
    }

    public function GetImageUrlLG2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath() : NULL;
    }












}
