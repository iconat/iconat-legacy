<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class LoginValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
     $ae=array(

         
            'email' => 'required|email',
            'password' => 'required', 
         
          

        );

       

        return $ae;
    
    }


            public function messages()
    {
        return [

         
       
         'email.required' => ' الرجاء ادخال الايميل بشكل صحيح',
         'email.email' => 'يجب ان يكون بريد الكتروني',
         'password.required' => 'الرجاء ادخال الرقم السري',
         'password.min' => ' الرقم السري على الاقل 8 حروف',
         
     ];
 }








}
