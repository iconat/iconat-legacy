<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;
use File;

class Gallery extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;


    protected $main_thumb_width_lg = 640;
    protected $main_thumb_height_lg = 640;

    protected $main_thumb_width_md = 640;
    protected $main_thumb_height_md = 640;

    protected $main_thumb_width_sm = 300;
    protected $main_thumb_height_sm = 300;

    protected $main_thumb_width_xs = 300;
    protected $main_thumb_height_xs = 300;


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
             $this->Browse = Auth::user()->ability(array('admin'), array('B_Gallery'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Gallery'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Gallery'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Gallery'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('main_thumb_width_lg', $this->main_thumb_width_lg);
            View::share('main_thumb_height_lg', $this->main_thumb_height_lg);
            View::share('main_thumb_width_md', $this->main_thumb_width_md);
            View::share('main_thumb_height_md', $this->main_thumb_height_md);
            View::share('main_thumb_width_sm', $this->main_thumb_width_sm);
            View::share('main_thumb_height_sm', $this->main_thumb_height_sm);
            View::share('main_thumb_width_xs', $this->main_thumb_width_xs);
            View::share('main_thumb_height_xs', $this->main_thumb_height_xs);

            return $next($request);
        });
    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.gallery.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("general.gallery_single")));
        } else {
            $CurrentObj = \App\Models\Gallery::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {
            $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_gallery_img_thumb_xs")) ? $CurrentObj->lastMedia("main_gallery_img_thumb_xs")->getUrl() : NULL)) : NULL;
            $main_gallery_img = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_gallery_img")) ? $CurrentObj->lastMedia("main_gallery_img")->getUrl() : NULL)) : NULL;
            $second_gallery_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("second_gallery_img_thumb")) ? $CurrentObj->lastMedia("second_gallery_img_thumb")->getUrl() : NULL)) : NULL;
            $second_gallery_img_thumb2 = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("second_gallery_img_thumb2")) ? $CurrentObj->lastMedia("second_gallery_img_thumb2")->getUrl() : NULL)) : NULL;
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.gallery.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->with('main_img_thumb', $main_img_thumb)->with('main_gallery_img', $main_gallery_img)->with('second_gallery_img_thumb', $second_gallery_img_thumb)->with('second_gallery_img_thumb2', $second_gallery_img_thumb2)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
         $Obj = $q = \App\Models\Gallery::where('id', '=', $id)->first();

        $results = array();

        if (($this->Edit && !empty(\App\Models\Gallery::where('id', $id)->count())) || ($this->Add && empty($id))) {



            if(!empty($id) && $request->hasFile('main_gallery')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

            }
            if(!empty($id) && $request->hasFile('second_gallery2')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img2');
                  $imagen2 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img_thumb2');

                  $files = array($imagen1, $imagen2);

                  File::delete($files);

            }

            if(!empty($id) && $request->hasFile('second_gallery')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img_thumb');


                  $files = array($imagen1, $imagen2);

                  File::delete($files);

            }




            // $idd = array(
            //     'ar.name' => 'required',
            //     'en.name' => 'required',
            //     'main_city' => 'required',

            // );

            // if ($this->Edit && !empty(\App\Models\Cites::where('id', $id)->count())) {
            //     unset($idd['main_city']);
            // }
            // Validator::make($ReqData, $idd)->validate();






            try {
                if ($request->hasFile('main_gallery')) {
                    $file = $request->file('main_gallery');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('gallery/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();
                    ImageOptimizer::optimize($orginal_media->getAbsolutePath());

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->main_thumb_width_lg, $this->main_thumb_height_lg)
                        ->save($folder_ . '/' . $orginal_media->filename . '_lg.' . $orginal_media->extension);

                    $thumb_lg = new \Plank\Mediable\Media;
                    $thumb_lg->disk = $orginal_media->disk;
                    $thumb_lg->directory = $orginal_media->directory;
                    $thumb_lg->filename = $orginal_media->filename . '_lg';
                    $thumb_lg->extension = $orginal_media->extension;
                    $thumb_lg->mime_type = $orginal_media->mime_type;
                    $thumb_lg->aggregate_type = $orginal_media->aggregate_type;
                    $thumb_lg->size = filesize($folder_ . '/' . $orginal_media->filename . '_lg.' . $orginal_media->extension);
                    $thumb_lg->save();
                    ImageOptimizer::optimize($thumb_lg->getAbsolutePath());


                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->main_thumb_width_md, $this->main_thumb_height_md)
                        ->save($folder_ . '/' . $orginal_media->filename . '_md.' . $orginal_media->extension);

                    $thumb_md = new \Plank\Mediable\Media;
                    $thumb_md->disk = $orginal_media->disk;
                    $thumb_md->directory = $orginal_media->directory;
                    $thumb_md->filename = $orginal_media->filename . '_md';
                    $thumb_md->extension = $orginal_media->extension;
                    $thumb_md->mime_type = $orginal_media->mime_type;
                    $thumb_md->aggregate_type = $orginal_media->aggregate_type;
                    $thumb_md->size = filesize($folder_ . '/' . $orginal_media->filename . '_md.' . $orginal_media->extension);
                    $thumb_md->save();
                    ImageOptimizer::optimize($thumb_md->getAbsolutePath());

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->main_thumb_width_sm, $this->main_thumb_height_sm)
                        ->save($folder_ . '/' . $orginal_media->filename . '_sm.' . $orginal_media->extension);

                    $thumb_sm = new \Plank\Mediable\Media;
                    $thumb_sm->disk = $orginal_media->disk;
                    $thumb_sm->directory = $orginal_media->directory;
                    $thumb_sm->filename = $orginal_media->filename . '_sm';
                    $thumb_sm->extension = $orginal_media->extension;
                    $thumb_sm->mime_type = $orginal_media->mime_type;
                    $thumb_sm->aggregate_type = $orginal_media->aggregate_type;
                    $thumb_sm->size = filesize($folder_ . '/' . $orginal_media->filename . '_sm.' . $orginal_media->extension);
                    $thumb_sm->save();
                    ImageOptimizer::optimize($thumb_sm->getAbsolutePath());

                    $image = \Image::make($orginal_media->getAbsolutePath())
                        ->resize($this->main_thumb_width_xs, $this->main_thumb_height_xs)
                        ->save($folder_ . '/' . $orginal_media->filename . '_xs.' . $orginal_media->extension);

                    $thumb_xs = new \Plank\Mediable\Media;
                    $thumb_xs->disk = $orginal_media->disk;
                    $thumb_xs->directory = $orginal_media->directory;
                    $thumb_xs->filename = $orginal_media->filename . '_xs';
                    $thumb_xs->extension = $orginal_media->extension;
                    $thumb_xs->mime_type = $orginal_media->mime_type;
                    $thumb_xs->aggregate_type = $orginal_media->aggregate_type;
                    $thumb_xs->size = filesize($folder_ . '/' . $orginal_media->filename . '_xs.' . $orginal_media->extension);
                    $thumb_xs->save();
                    ImageOptimizer::optimize($thumb_xs->getAbsolutePath());


                }


            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'image' => [$e->getMessage()],
                ]);
                throw $error;
            }

              if(isset($ReqData['type_upload']) && ($ReqData['type_upload'] == "upload_video")){



             try {

                if ($request->hasFile('video')) {
                    $file = $request->file('video');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['video/mp4', 'video/x-flv','video/3gpp', 'video/quicktime' , 'video/x-msvideo', 'video/x-ms-wmv']);
                    $uploader->setAllowedExtensions(['mp4', 'x-flv', '3gpp', 'quicktime', 'x-msvideo', 'x-ms-wmv']);
                    $uploader->toDirectory('gallery_video/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));

                    $orginal_media_video = $uploader->upload();


                }
                  } catch (\Exception $e) {

                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }


        }








         try {
                if ($request->hasFile('second_gallery')) {
                $file = $request->file('second_gallery');
                $uploader = \MediaUploader::fromSource($file);
                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                $uploader->toDirectory('gallery/' . date("Y") . '/' . date("m") . '/' . date("d"));
                $uploader->useFilename(str_random(5));
                $orginal_media_sec = $uploader->upload();
                ImageOptimizer::optimize($orginal_media_sec->getAbsolutePath());
                $folder_ = storage_path("app/public") . '/' . $orginal_media_sec->directory;
                if (!file_exists($folder_)) {
                    mkdir($folder_, 666, true);
                }
                $image = \Image::make($orginal_media_sec->getAbsolutePath())->resize($this->main_thumb_width_lg, $this->main_thumb_height_lg)->save($folder_ . '/' . $orginal_media_sec->filename . '_thumb.' . $orginal_media_sec->extension);
                // $image = \Image::make($orginal_media->getAbsolutePath())
                // ->resize($this->main_thumb_width_lg, $this->main_thumb_height_lg)
                // ->save($folder_ . '/' . $orginal_media->filename . '_lg.' . $orginal_media->extension);
                $thumb_sec = new \Plank\Mediable\Media;
                $thumb_sec->disk = $orginal_media_sec->disk;
                $thumb_sec->directory = $orginal_media_sec->directory;
                $thumb_sec->filename = $orginal_media_sec->filename . '_thumb';
                $thumb_sec->extension = $orginal_media_sec->extension;
                $thumb_sec->mime_type = $orginal_media_sec->mime_type;
                $thumb_sec->aggregate_type = $orginal_media_sec->aggregate_type;
                $thumb_sec->size = filesize($folder_ . '/' . $orginal_media_sec->filename . '_thumb.' . $orginal_media_sec->extension);
                $thumb_sec->save();
                ImageOptimizer::optimize($thumb_sec->getAbsolutePath());
            }
            }
            catch(\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages(['image' => [$e->getMessage() ], ]);
                throw $error;
            }
             try {
                if ($request->hasFile('second_gallery2')) {
                $file = $request->file('second_gallery2');
                $uploader = \MediaUploader::fromSource($file);
                $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                $uploader->toDirectory('gallery/' . date("Y") . '/' . date("m") . '/' . date("d"));
                $uploader->useFilename(str_random(5));
                $orginal_media_sec2 = $uploader->upload();
                ImageOptimizer::optimize($orginal_media_sec2->getAbsolutePath());
                $folder_ = storage_path("app/public") . '/' . $orginal_media_sec2->directory;
                if (!file_exists($folder_)) {
                    mkdir($folder_, 666, true);
                }
                $image = \Image::make($orginal_media_sec2->getAbsolutePath())->resize($this->main_thumb_width_lg, $this->main_thumb_height_lg)->save($folder_ . '/' . $orginal_media_sec2->filename . '_thumb.' . $orginal_media_sec2->extension);
                // $image = \Image::make($orginal_media->getAbsolutePath())
                // ->resize($this->main_thumb_width_lg, $this->main_thumb_height_lg)
                // ->save($folder_ . '/' . $orginal_media->filename . '_lg.' . $orginal_media->extension);
                $thumb_sec2 = new \Plank\Mediable\Media;
                $thumb_sec2->disk = $orginal_media_sec2->disk;
                $thumb_sec2->directory = $orginal_media_sec2->directory;
                $thumb_sec2->filename = $orginal_media_sec2->filename . '_thumb';
                $thumb_sec2->extension = $orginal_media_sec2->extension;
                $thumb_sec2->mime_type = $orginal_media_sec2->mime_type;
                $thumb_sec2->aggregate_type = $orginal_media_sec2->aggregate_type;
                $thumb_sec2->size = filesize($folder_ . '/' . $orginal_media_sec2->filename . '_thumb.' . $orginal_media_sec2->extension);
                $thumb_sec2->save();
                ImageOptimizer::optimize($thumb_sec2->getAbsolutePath());
            }
            }
            catch(\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages(['image' => [$e->getMessage() ], ]);
                throw $error;
            }






           // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);
            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }



            $Obj = \App\Models\Gallery::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_gallery_img']);
            }
            if (isset($thumb_lg)) {
                $Obj->attachMedia($thumb_lg, ['main_gallery_img_thumb_lg']);
            }
            if (isset($thumb_md)) {
                $Obj->attachMedia($thumb_md, ['main_gallery_img_thumb_md']);
            }
            if (isset($thumb_sm)) {
                $Obj->attachMedia($thumb_sm, ['main_gallery_img_thumb_sm']);
            }
            if (isset($thumb_xs)) {
                $Obj->attachMedia($thumb_xs, ['main_gallery_img_thumb_xs']);
            }

             if (isset($orginal_media_sec)) {
                $Obj->attachMedia($orginal_media_sec, ['second_gallery_img']);
            }
            if (isset($thumb_sec)) {
                $Obj->attachMedia($thumb_sec, ['second_gallery_img_thumb']);
            }
            if (isset($orginal_media_sec2)) {
                $Obj->attachMedia($orginal_media_sec2, ['second_gallery_img2']);
            }
            if (isset($thumb_sec2)) {
                $Obj->attachMedia($thumb_sec2, ['second_gallery_img_thumb2']);
            }

             if (isset($orginal_media_video)) {
                $Obj->attachMedia($orginal_media_video, ['video']);
            }

            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("slider.slider")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.added_a_new_slider');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("slider.slider")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.edited_the_slider');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if(isset($ReqData[$Lang->locale])){
                        foreach ($ReqData[$Lang->locale] as $k => $v){
                            $ObjTrans = \App\Models\Translations\GalleryTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id,'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Gallery::orderBy('order', 'asc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                // if (!empty($request["name"])) {
                //     $Objs = $Objs->where("name", 'LIKE', '%' . $request["name"] . '%');
                // }






                 if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }


            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {


                $buttons = '';
                $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                $buttons .= '<button par1="' . $value->id . '" par2="'.$value->name.'" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name'})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                $created_at = new Carbon($value->created_at);
                // $records["data"][] = array(
                //     $value->{'name'},
                //     $created_at->toDayDateTimeString(),

                //     $buttons,
                // );

                 $array_data = array();

                if ((empty($value->MainImage("main_gallery_img")) && (empty($value->GetvideoURL('video'))) && (empty($value->link_video)))) {

                    array_push($array_data, '<p>لا يوجد ملف مرفوع</p>');
                }

                 if($value->MainImage("main_gallery_img")){

                        array_push($array_data, '<a href="'.$value->MainImage("main_gallery_img").'"class="popup-link"><img class="" src="' . $value->MainImage("main_gallery_img") . '"  height="150px" width="200px" /></a>  ');

                    }


                  if($value->GetvideoURL('video')){

                        array_push($array_data, '<video width="200" controls>
                        <source src="'.$value->GetvideoURL('video').'" type="video/mp4" height="150px" width="200px">
                        Your browser does not support HTML5 video.
                         </video>');

                       }



                       if($value->link_video){

                         // array_push($array_data, '<a href="'.$value->link_video.'" target="_blank" class="btn btn-sm btn-outline blue" height="100px" width="200px">' . trans("slider.youtube_video") . '</a>');

                        array_push($array_data, '<a href="https://www.youtube.com/embed/'.$value->link_video.'" target="_blank"/><input type="image" name="' . trans("slider.youtube_video") . '" src="'. url(setting('youtube' , "")).'" border="border of the image" alt="text" height="150px" width="200px" target="_blank"></a>');

                       }


                    array_push($array_data, $value->order);





                     if ($value->active == 1) {
                        array_push($array_data, trans("general.active") );
                    }

                     if ($value->active == NULL) {
                        array_push($array_data, trans("general.inactive") );
                    }


                     array_push($array_data, $buttons);





                $records["data"][] = $array_data;

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Gallery::where('id', '=', $id)->first();

            $q = \App\Models\Gallery::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

               $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetURL('main_gallery_img_thumb_xs');
                  $imagen6 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img');
                  $imagen7 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img_thumb');
                  $imagen8 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img2');
                  $imagen9 = getcwd()."/storage/". $Obj->GetURL('second_gallery_img_thumb2');
                  $imagen10 = getcwd()."/storage/". $Obj->GetURL('video');


                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5,$imagen6,$imagen7,$imagen8,$imagen9,$imagen10);

                  File::delete($files);

            if (!empty($q)) {

                activity("slider.slider")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.deleted_slider');

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



