<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;


class Categories extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;


    protected $importance_array = [1 => "general.very_important", 2 => 'general.average_importance', 3 => 'general.not_so_important'];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Categories'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Categories'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Categories'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Categories'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            View::share('importance_array', $this->importance_array);

            return $next($request);
        });
    }


    public function index(Request $request)
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $category = NULL;
            $title = trans("categories.main_categories");
            if ($request->has('M')) {
                $Parent_id = $request->input('M');
                $category = \App\Models\Categories::find($Parent_id);
                if ($category) {
                    $title = trans("categories.subcategories_for:_name", ["name" => $category->Get_Trans(\App::getLocale(), "name")]);
                }
            }
            return view('admin.categories.view_all')
                ->with('category', $category)
                ->with('title', $title);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;
        $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;


        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("categories.categorie")));
        } else {
            $CurrentObj = \App\Models\Categories::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {

            $Parent = NULL;
            if (!empty($par2)) {
                $Parent = \App\Models\Categories::find($par2);
            } elseif ($CurrentObj) {
                $Parent = $CurrentObj->parent;
            }
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.categories.AE')
                    ->with('par1', $par1)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('Parent', $Parent)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $results = array();

        if (($this->Edit && !empty(\App\Models\Categories::where('id', $id)->count())) || ($this->Add && empty($id))) {

            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }

            if(empty($ReqData['parent_id'])){
                $ReqData['parent_id'] = NULL;
            }


            Validator::make($ReqData, [
//                'name' => [
//                    'required',
//                ],
            ])->validate();


            // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);

            $Obj = \App\Models\Categories::updateOrCreate(
                ['id' => $id],
                $ReqData
            );


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("categories.categories")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('categories.added_a_new_categorie');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("categories.categories")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('categories.edited_categorie');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\CategoriesTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Categories::orderBy('id', 'desc');

            if (!empty($request['par1'])) {
                $Objs = $Objs->where('parent_id', $request['par1']);
            }else{
                $Objs = $Objs->where('parent_id', NULL);

            }


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["name"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["name"] . '%');
                    });
                }

                if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }

                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = $request["date_from"];
                    $date_to = $request["date_to"];
                    $Objs = $Objs->whereBetween('created_at', [$date_from, $date_to]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {

                $AcState = trans("general.not_active");

                if ($value->active == 1) {
                    $AcState = trans("general.active");
                }

                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Browse) {
                    $buttons .= '<a href="' . url("/Manage/Categories?M=" . $value->id) . '" class="btn btn-sm btn-outline purple-sharp"><i class="fa fa-list"></i> ' . trans("categories.subcategories") . '</a>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . $value->name . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name'})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $created_at = new Carbon($value->created_at);
                $records["data"][] = array(
                    $value->Get_Trans(App::getLocale(), 'name'),
                    $AcState,
                    $created_at->toDayDateTimeString(),
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Categories::where('id', '=', $id)->first();
            $CanDeleted = $Obj->CanDeleted();
            if ($CanDeleted === true) {
                $q = \App\Models\Categories::where('id', '=', $id)
                    ->where('deleted_at', NULL)
                    ->update([
                        'deleted_at' => date("Y-m-d H:i:s"),
                        'deleted_by' => Auth::user()->id,
                    ]);

                if (!empty($q)) {

                    activity("categories.categories")
                        ->performedOn($Obj)
                        ->causedBy(Auth::user()->id)
                        ->log('categories.deleted_categorie');

                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                    );
                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans('general.the_operation_was_not_done_correctly'),
                    );
                }
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $CanDeleted,
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



