<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;
use Illuminate\Validation\Rule;
use File;

class Shops extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;


    protected $main_thumb_width_lg = 1000;
    protected $main_thumb_height_lg = 1000;

    protected $main_thumb_width_md = 700;
    protected $main_thumb_height_md = 700;

    protected $main_thumb_width_sm = 500;
    protected $main_thumb_height_sm = 500;

    protected $main_thumb_width_xs = 300;
    protected $main_thumb_height_xs = 300;

     protected $main_logo_thumb_width_lg = 500;
    protected $main_logo_thumb_height_lg = 500;

    protected $main_logo_thumb_width_md = 400;
    protected $main_logo_thumb_height_md = 400;

    protected $main_logo_thumb_width_sm = 300;
    protected $main_logo_thumb_height_sm = 300;

    protected $main_logo_thumb_width_xs = 200;
    protected $main_logo_thumb_height_xs = 200;




    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin','dealer'), array('B_Shops'));
            $this->Add = Auth::user()->ability(array('admin','dealer'), array('A_Shops'));
            $this->Edit = Auth::user()->ability(array('admin','dealer'), array('E_Shops'));
            $this->Delete = Auth::user()->ability(array('admin','dealer'), array('D_Shops'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('main_thumb_width_lg', $this->main_thumb_width_lg);
            View::share('main_thumb_height_lg', $this->main_thumb_height_lg);
            View::share('main_thumb_width_md', $this->main_thumb_width_md);
            View::share('main_thumb_height_md', $this->main_thumb_height_md);
            View::share('main_thumb_width_sm', $this->main_thumb_width_sm);
            View::share('main_thumb_height_sm', $this->main_thumb_height_sm);
            View::share('main_thumb_width_xs', $this->main_thumb_width_xs);
            View::share('main_thumb_height_xs', $this->main_thumb_height_xs);

            View::share('main_logo_thumb_width_lg', $this->main_logo_thumb_width_lg);
            View::share('main_logo_thumb_height_lg', $this->main_logo_thumb_height_lg);
            View::share('main_logo_thumb_width_md', $this->main_logo_thumb_width_md);
            View::share('main_logo_thumb_height_md', $this->main_logo_thumb_height_md);
            View::share('main_logo_thumb_width_sm', $this->main_logo_thumb_width_sm);
            View::share('main_logo_thumb_height_sm', $this->main_logo_thumb_height_sm);
            View::share('main_logo_thumb_width_xs', $this->main_logo_thumb_width_xs);
            View::share('main_logo_thumb_height_xs', $this->main_logo_thumb_height_xs);

            return $next($request);
        });
    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.shops.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("shops.shop")));
        } else {
            $CurrentObj = \App\Models\Shops::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {
            $main_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_shop_img")) ? $CurrentObj->lastMedia("main_shop_img")->getUrl() : NULL)) : NULL;

            $main_logo_img_thumb = !empty($CurrentObj) ? ((($CurrentObj->lastMedia("main_logo_shop_img")) ? $CurrentObj->lastMedia("main_logo_shop_img")->getUrl() : NULL)) : NULL;

            return json_encode(array(
                'title' => $title,
                'content' => view('admin.shops.AE')
                    ->with('par1', $par1)
                    ->with('CurrentObj', $CurrentObj)
                    ->with('main_img_thumb', $main_img_thumb)
                    ->with('main_logo_img_thumb', $main_logo_img_thumb)
                    ->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
         $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

         $user_id = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;
  $Obj = \App\Models\Shops::where('id', '=', $id)->first();
         $CurrentObj = \App\User::find($user_id);

       // echo($CurrentObj->id);

        $results = array();

     if (($this->Edit && !empty(\App\Models\Shops::where('id', $id)->count())) || ($this->Add && empty($id))) {


         if(!empty($id) && $request->hasFile('main_logo_shop')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_logo_shop_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG('main_logo_shop_img_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD('main_logo_shop_img_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM('main_logo_shop_img_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS('main_logo_shop_img_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

            }

         if(!empty($id) && $request->hasFile('main_shop')){

                 $imagen1 = getcwd()."/storage/". $Obj->GetURL('main_shop_img');
                  $imagen2 = getcwd()."/storage/". $Obj->GetImageUrlLG2('main_shop_img_lg');
                  $imagen3 = getcwd()."/storage/". $Obj->GetImageUrlMD2('main_shop_img_md');
                  $imagen4 = getcwd()."/storage/". $Obj->GetImageUrlSM2('main_shop_img_sm');
                  $imagen5 = getcwd()."/storage/". $Obj->GetImageUrlXS2('main_shop_img_xs');

                  $files = array($imagen1, $imagen2,$imagen3,$imagen4,$imagen5);

                  File::delete($files);

            }



            // Validator::make($ReqData, [
                //  'mobile' =>'required',
               // 'ar[name]' =>'required',
               // 'en[name]' =>'required',
               // 'ar[text]' =>'required',
               // 'en[text]' =>'required',
               // 'ar[address]' =>'required',
               // 'en[address]' =>'required',


                // 'user_id' => [
                //     'required',
                //     Rule::unique('shops')->where(function ($query) use ($request, $CurrentObj) {
                //         return $query->where('user_id', '!=', !empty($CurrentObj->id) ? $CurrentObj->id : NULL)->where('user_id', $request->user_id);
                //     }),
                // ],

            // ])->validate();


            $idd = array(
                'ar.name' => 'required',
                'en.name' => 'required',
                'mobile' => 'required',
                'city' => 'required',
                'shop_categories' => 'required',
                'main_logo_shop' => 'required',

            );

            if ($this->Edit && !empty(\App\Models\Shops::where('id', $id)->count())) {
                unset($idd['main_logo_shop']);
            }
            Validator::make($ReqData, $idd)->validate();

            try {
                if ($request->hasFile('main_shop')) {
                    $file = $request->file('main_shop');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader->toDirectory('Shop/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();
                    ImageOptimizer::optimize($orginal_media->getAbsolutePath());

                    $folder_ = storage_path("app/public") . '/' . $orginal_media->directory;
                    if (!file_exists($folder_)) {
                        mkdir($folder_, 666, true);
                    }

                    $make_thumbs = [
                        'lg' => [800, 800],
                        'md' => [600, 600],
                        'sm' => [300, 300],
                        'xs' => [100, 100],
                    ];

                    foreach ($make_thumbs as $key => $value) {
                        $image = \Image::make($orginal_media->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);

                        $thumb = new \Plank\Mediable\Media;
                        $thumb->disk = $orginal_media->disk;
                        $thumb->directory = $orginal_media->directory;
                        $thumb->filename = $orginal_media->filename . '_' . $key;
                        $thumb->extension = $orginal_media->extension;
                        $thumb->mime_type = $orginal_media->mime_type;
                        $thumb->aggregate_type = $orginal_media->aggregate_type;
                        $thumb->size = filesize($folder_ . '/' . $orginal_media->filename . '_' . $key . '.' . $orginal_media->extension);
                        $thumb->save();
                        ImageOptimizer::optimize($thumb->getAbsolutePath());

                        $orginal_media->attachMedia($thumb, [$key]);


                    }

                }





                if ($request->hasFile('main_logo_shop')) {
                    $file2 = $request->file('main_logo_shop');

                    $uploader2 = \MediaUploader::fromSource($file2);
                    $uploader2->setAllowedMimeTypes(['image/jpeg', 'image/gif', 'image/png']);
                    $uploader2->setAllowedExtensions(['jpg', 'jpeg', 'png', 'gif']);
                    $uploader2->toDirectory('Shop_logo/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader2->useFilename(str_random(5));
                    $orginal_media2 = $uploader2->upload();
                    ImageOptimizer::optimize($orginal_media2->getAbsolutePath());

                    $folder_2 = storage_path("app/public") . '/' . $orginal_media2->directory;
                    if (!file_exists($folder_2)) {
                        mkdir($folder_2, 666, true);
                    }

                    $make_thumbs2 = [
                        'lg' => [500, 500],
                        'md' => [400, 400],
                        'sm' => [300, 300],
                        'xs' => [200, 200],
                    ];

                    foreach ($make_thumbs2 as $key => $value) {
                        $image2 = \Image::make($orginal_media2->getAbsolutePath())
                            ->resize($value[0], $value[1])
                            ->save($folder_2 . '/' . $orginal_media2->filename . '_' . $key . '.' . $orginal_media2->extension);

                        $thumb2 = new \Plank\Mediable\Media;
                        $thumb2->disk = $orginal_media2->disk;
                        $thumb2->directory = $orginal_media2->directory;
                        $thumb2->filename = $orginal_media2->filename . '_' . $key;
                        $thumb2->extension = $orginal_media2->extension;
                        $thumb2->mime_type = $orginal_media2->mime_type;
                        $thumb2->aggregate_type = $orginal_media2->aggregate_type;
                        $thumb2->size = filesize($folder_2 . '/' . $orginal_media2->filename . '_' . $key . '.' . $orginal_media2->extension);
                        $thumb2->save();
                        ImageOptimizer::optimize($thumb2->getAbsolutePath());

                        $orginal_media2->attachMedia($thumb2, [$key]);


                    }

                }




            } catch (\Exception $e) {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'image' => [$e->getMessage()],
                ]);
                throw $error;
            }

              if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }


            // $ReqData['text_ar'] = str_replace('src="' . url(""), 'src="App_Path_Re', $ReqData['text_ar']);

            $Obj = \App\Models\Shops::updateOrCreate(
                ['id' => $id],
                $ReqData
            );

            if (isset($orginal_media)) {
                $Obj->attachMedia($orginal_media, ['main_shop_img']);
            }

               if (isset($orginal_media2)) {
                $Obj->attachMedia($orginal_media2, ['main_logo_shop_img']);
            }


            if (isset($ReqData['MediaFiles'])) {
                $MediaFiles_array = json_decode($ReqData['MediaFiles']);
                foreach ($MediaFiles_array as $HashedID) {
                    $m_id = \Crypt::decrypt($HashedID);
                    $F = $Obj->attachMedia([$m_id], 'shop_images');
                }
            }









            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->shop_code = $this->generate_code();
                $Obj->update();



                activity("shops.shops")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('shops.added_a_new_shop');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("shops.shops")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.edited_the_slider');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\ShopsTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);

    }
    public function generate_code()
    {
        $randomString = str_random(7);
        while (true){
            if(\App\Models\Shops::where('shop_code', $randomString)->count() > 0){
                return $this->generate_code();
            }
            else{
                return $randomString;
            }
        }

    }
    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Shops::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["name"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["name"] . '%');
                    });
                }

                 if (!empty($request["city"])) {
                    $Objs = $Objs->where("city", $request["city"]);
                }


                 if (!empty($request["shop_categories"])) {
                    $Objs = $Objs->where("shop_categories", $request["shop_categories"]);
                }

                  if (!empty($request["active"])) {
                    $ac = NULL;
                    if ($request["active"] == 'active') {
                        $ac = 1;
                    }
                    $Objs = $Objs->where("active", $ac);
                }


                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = isset($request["date_from"]) ? $request["date_from"] : NULL;
                    $date_to = isset($request["date_to"]) ? $request["date_to"] : NULL;
                    $newDateTo = date('Y-m-d', strtotime($date_to.' +1 day'));
                    if($date_from && $date_to){
                        $Objs = $Objs->whereBetween('created_at', [$date_from, $newDateTo]);
                    }
                    elseif ($date_from && (!$date_to)){
                        $Objs = $Objs->where('created_at',">=", $date_from);
                    }
                    elseif ((!$date_from) && $date_to){
                        $Objs = $Objs->where('created_at',"<=", $newDateTo);
                    }
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {


                $buttons = '';
                $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                $buttons .= '<button par1="' . $value->id . '" par2="' . $value->name . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'name'})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                $created_at = new Carbon($value->created_at);
                $array_data = array();

                  array_push($array_data, $value->Get_Trans(App::getLocale(), 'name'));
                  array_push($array_data, $value->Shop_City->Get_Trans(App::getLocale(), 'name'));
                  array_push($array_data, $value->Shop_Categories->Get_Trans(App::getLocale(), 'name'));

                    if($value->active == 1) {
                     array_push($array_data, trans("general.active"));
                      }

                    if ($value->active == NULL) {
                        array_push($array_data, trans("general.inactive") );
                    }

                  array_push($array_data, $created_at->toDayDateTimeString() );
                  array_push($array_data, $buttons);

                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Shops::where('id', '=', $id)->first();


            $imagen1 = getcwd() . "/storage/" . $Obj->GetURL('main_logo_shop_img');
            $imagen2 = getcwd() . "/storage/" . $Obj->GetImageUrlLG('main_logo_shop_img_lg');
            $imagen3 = getcwd() . "/storage/" . $Obj->GetImageUrlMD('main_logo_shop_img_md');
            $imagen4 = getcwd() . "/storage/" . $Obj->GetImageUrlSM('main_logo_shop_img_sm');
            $imagen5 = getcwd() . "/storage/" . $Obj->GetImageUrlXS('main_logo_shop_img_xs');
            $imagen6 = getcwd() . "/storage/" . $Obj->GetURL('main_shop_img');
            $imagen7 = getcwd() . "/storage/" . $Obj->GetImageUrlLG2('main_shop_img_lg');
            $imagen8 = getcwd() . "/storage/" . $Obj->GetImageUrlMD2('main_shop_img_md');
            $imagen9 = getcwd() . "/storage/" . $Obj->GetImageUrlSM2('main_shop_img_sm');
            $imagen10 = getcwd() . "/storage/" . $Obj->GetImageUrlXS2('main_shop_img_xs');

            $files = array($imagen1, $imagen2, $imagen3, $imagen4, $imagen5, $imagen6, $imagen7, $imagen8, $imagen9, $imagen10);

            File::delete($files);

            $q = \App\Models\Shops::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {

                activity("shops.shops")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('slider.deleted_slider');

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }













    /**
    * Method : DELETE
    *
    * @return delete images
    */
    public function deleteImage(Request $request) {

        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;

            $obj = \App\Models\Media::find($id);

            $fileName = $obj->filename;

            $images = \App\Models\Media::where('filename', 'like', $fileName.'%')->get();

            $timeStamp = date('Y-m-d H:i:s');
            $testOut = '';
            foreach($images as $image){
                \App\Models\Media::where('id', $image->id)->update(
                    ['deleted_at'=>$timeStamp]
                );
                $imagePath = public_path() .'/storage/'.$image->directory.'/'.$image->filename.'.'.$image->extension;
                $testOut = $imagePath;
                File::delete($imagePath);


            }
            return response()->json(['message' => 'ok']);

        }
           /*
            $q = \App\Models\Media::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                ]);

            if (!empty($q)) {

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.done'),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);








        Media::where('id', $id)->delete();
    	$image = \App\Models\media::findOrFail($id);
        if ($image && count($image) > 0) {

            $file = public_path() . '/uploads/'.$image->image;
            if(is_file($file)){
                @unlink($file);
            }
            $image->delete();
        }
        $images = Image::get();
		$view = view('partials.imagelist', compact('images'))->render();
        return response()->json(['html' => $view]);







        */






    }










public function search(Request $request)
{
if($request->ajax())
{
$output="";
$shops=DB::table('shops')->where('title','LIKE','%'.$request->search."%")->get();

return Response('g');

   }
}


}



