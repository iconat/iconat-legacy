<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;

class Contracts extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    protected $Only_Invs = FALSE; //هل هو فقط مستثمر

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Contracts'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Contracts'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Contracts'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Contracts'));

            $this->Only_Invs = (Auth::user()->hasRole('investor') && !(Auth::user()->ability(array('admin'), array('B_Contracts', 'A_Contracts', 'E_Contracts', 'D_Contracts'))));


            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            View::share('Only_Invs', $this->Only_Invs);

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Only_Invs || $this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.contracts.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("contracts.contract")));
        } else {
            $CurrentObj = \App\Models\Contract::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || (!empty($CurrentObj) && ($this->Edit || (isset($CurrentObj->Investor->UserOwner->id) && ($CurrentObj->Investor->UserOwner->id == Auth::user()->id))))) {


            return json_encode(array(
                'title' => $title,
                'content' => view('admin.contracts.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        //$ReqData = \Purifier::clean($request->all());
        $ReqData = $request->all();
        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = \App\Models\Contract::where('id', $id)->first();

        $results = array();
        if (($this->Add && empty($id)) || (!empty($CurrentObj) && ($this->Edit))) {
            $ss = isset($CurrentObj->id) ? $CurrentObj->id : NULL;

            $st = (!empty($ss)) ? 'required,id,' . $ss : 'required';

            $idd = array(
                'invs_id' => $st,
                'amount' => 'required',
                'date_of_contract' => 'required',
            );

            Validator::make($ReqData, $idd)->validate();

            if (isset($ReqData['state'])) {
                if ($ReqData['state'] == 'effective') {
                    $ReqData['state'] = 1;
                } else {
                    $ReqData['state'] = NULL;
                }
            }

            try {
                if ($request->hasFile('pdf')) {
                    $file = $request->file('pdf');

                    $uploader = \MediaUploader::fromSource($file);
                    $uploader->setAllowedMimeTypes(['application/pdf']);
                    $uploader->setAllowedExtensions(['pdf']);
                    $uploader->toDirectory('files/contract/' . date("Y") . '/' . date("m") . '/' . date("d"));
                    $uploader->useFilename(str_random(5));
                    $orginal_media = $uploader->upload();

                }
            } catch (\Exception $e) {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => $e->getMessage(),
                );
            }

            if (empty($results['Errors'])) {
                $Obj = \App\Models\Contract::updateOrCreate(
                    ['id' => $id],
                    $ReqData
                );

                if (isset($orginal_media)) {
                    $Obj->attachMedia($orginal_media, ['contract']);
                }
                if ($Obj->wasRecentlyCreated) {
                    $content = trans("general.successfully_added_new_:name", ['name' => trans("contracts.contract")]);
                    $Obj->created_by = Auth::user()->id;
                    $Obj->update();
                } else {
                    $content = trans("general.modified_successfully");
                    $Obj->updated_by = Auth::user()->id;
                    $Obj->update();
                }

                if (!empty($Obj->id)) {
                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => $content,
                    );
                }
            }


        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function BPDF(\App\Models\Contract $C)
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete || ($this->Only_Invs && $C->Investor->UserOwner->id == Auth::user()->id)) {

            $media = $C->lastMedia('contract');
            $url = $media->getUrl();
            $url = str_replace(url("/storage/"), storage_path("app/public"), $url);
            return response()->file($url);
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function getdata(Request $request)
    {

        if (Auth::user()->hasRole('investor') || $this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Contract::orderBy('id', 'desc');


            if ($this->Only_Invs) {
                $Objs = $Objs->whereHas('Investor', function ($q) {
                    $q->where("user_id", '=', Auth::user()->id);
                });
            }

            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["investor_name"])) {
                    $Objs = $Objs->whereHas('Investor', function ($q) use ($request) {
                        $q->where(\DB::raw('concat(`firstname`," ",`secondname`," ",`thirdname`," ",`fourthname`)'), 'LIKE', '%' . $request["investor_name"] . '%');
                    });
                }
                if (!empty($request["field_id"])) {
                    $Objs = $Objs->where("field_id", $request["field_id"]);
                }
                if (isset($request["share_amount_from"]) && is_numeric($request["share_amount_from"])) {
                    $Num_From = $request["share_amount_from"];
                    $Objs = $Objs->where('share_amount', ">=", $Num_From);
                }
                if (isset($request["share_amount_to"]) && is_numeric($request["share_amount_to"])) {
                    $Num_To = $request["share_amount_to"];
                    $Objs = $Objs->where('share_amount', "<=", $Num_To);
                }

                if (!empty($request["share_number"])) {
                    $Objs = $Objs->where("share_number", 'LIKE', '%' . $request["share_number"] . '%');
                }
                if (!empty($request["state"])) {
                    if ($request["state"] == 'effective') {
                        $Objs = $Objs->where("state", '=', 1);
                    } elseif ($request["state"] == 'ineffective') {
                        $Objs = $Objs->where("state", '=', NULL);
                    }
                }

                if (!empty($request["date_from"])) {
                    $Objs = $Objs->where('share_date', ">=", $request["date_from"]);
                }
                if (!empty($request["date_to"])) {
                    $Objs = $Objs->where('share_date', "<=", $request["date_to"]);
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            foreach ($Objs->get() as $value) {
                $media = $value->lastMedia('contract');

                $buttons = '';

                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $buttons .= '<button par1="' . $value->id . '" par2="' . trans("contracts.contract_of_investor_:name", ["name" => $value->Investor->get_Short_name()]) . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $value->{'title_' . App::getLocale()})) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                if ((isset($value->Investor->UserOwner->id) && $value->Investor->UserOwner->id == Auth::user()->id) && isset($media)) {
                    $buttons .= "<a class='btn btn-xs btn-block btn-circle blue-steel margin-top-10' href='" . $media->getUrl() . "'>" . trans("general.download") . "</a>";
                    //$buttons .= "<a class='btn btn-xs btn-block btn-circle grey-salsa margin-top-10' target='_blank' href='" . url('Contracts/BPDF/' . $value->id) . "'>" . trans("contracts.send_the_contract_by_e-mail") . "</a>";
                    $buttons .= "<button type='button' par1='" . $value->id . "' class='btn btn-xs btn-block btn-circle purple margin-top-10 SendToHome'>" . trans("contracts.send_an_original_to_your_home") . "</button>";

                }

                $array_data = array();

                $contract_url = trans("contracts.not_yet_attached");

                if (isset($media)) {
                    $contract_url = "<a class='btn btn-xs btn-circle grey-gallery' target='_blank' href='" . url('Contracts/BPDF/' . $value->id) . "'>" . trans("general.review") . "</a>";
                }

                array_push($array_data, $contract_url);

                if (!$this->Only_Invs) {
                    array_push($array_data, $value->Investor->get_Full_name());
                }

                array_push($array_data, isset($value->Field) ? $value->Field->name_ar : '<span class="font-grey-salsa">' . trans("general.undefined") . '</span>');

                array_push($array_data, is_numeric($value->amount) ? '$' . $value->amount : NULL);

                array_push($array_data, $value->date_of_contract);

                array_push($array_data, $buttons);

                $records["data"][] = $array_data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $q = \App\Models\Contract::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

    public function SendToMyHome(Request $request)
    {
        $ReqData = $request->all();
        $contract_id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $investor_account = Auth::user()->investor_account;

        if (!empty($investor_account)) {
            $Contract = \App\Models\Contract::find($contract_id);

            if (!empty($Contract)) {

                if ($Contract->invs_id == $investor_account->id) {


                    $ContractToHome = \App\Models\ContractToHome::where('invs_id',$investor_account->id)->where("contract_id",$contract_id)->first();
                    if(!empty($ContractToHome)){

                        $title = $ContractToHome->GetStateTitle();
                        $content = "طلبك حاليا قيد المعالجة، ستظهر هنا ملاحظات الادارة على الطلب في القريب العاجل...";


                        return json_encode(array(
                            'OnlyRead' => "Yes", //لاخفاء زر تقديم
                            'title' => $title,
                            'content' => view('admin.contracts.SendToMyHomeState')->with("content",$content)->render()
                        ));

                    }else{
                        return json_encode(array(
                            'title' => trans("contracts.send_an_original_to_your_home"),
                            'content' => view('admin.contracts.SendToMyHome')->with('investor_account', $investor_account)->with('par1', $contract_id)->with('Contract', $Contract)->render()
                        ));

                    }



                } else {
                    return json_encode(array(
                        'title' => trans("general.error"),
                        'content' => view('system.errors.show_error_modal')->with('error_description', trans("contracts.this_contract_not_for_your_account"))->render()
                    ));
                }


            } else {
                return json_encode(array(
                    'title' => trans("general.error"),
                    'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.the_requested_item_could_not_be_found"))->render()
                ));
            }
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("investors.:name_is_only_available_to_investors", ["name" => trans("contracts.send_an_original_to_your_home")]))->render()
            ));
        }


    }

    public function SendToMyHomeToDB(Request $request)
    {
        $results = array();


        $ReqData = \Purifier::clean($request->all());
        $contract_id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $investor_account = Auth::user()->investor_account;

        if (!empty($investor_account)) {
            $Contract = \App\Models\Contract::find($contract_id);

            if (!empty($Contract)) {

                if ($Contract->invs_id == $investor_account->id) {

                    $ContractToHome = \App\Models\ContractToHome::where('invs_id',$investor_account->id)->where("contract_id",$contract_id)->first();
                    if(!empty($ContractToHome)){
                        $results['Errors'] = array(
                            'title' => trans('general.error'),
                            'content' => trans("contracts.you_had_been_request_send_this_contract_to_your_home"),
                        );
                    }else{
                        $ReqData['invs_id'] = $investor_account->id;
                        $ReqData['contract_id'] = $contract_id;

                        $Obj = \App\Models\ContractToHome::updateOrCreate(
                            ['invs_id' => $investor_account->id,'contract_id' => $contract_id],
                            $ReqData
                        );

                        if($Obj){
                            $results['Success'] = array(
                                'title' => trans('general.done'),
                                'content' => trans("general.we_had_been_recived_your_request_and_we_will_return_for_you,_thank_you"),
                            );
                        }

                    }


                } else {
                    $results['Errors'] = array(
                        'title' => trans('general.error'),
                        'content' => trans("contracts.this_contract_not_for_your_account"),
                    );

                }


            } else {

                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans("general.the_requested_item_could_not_be_found"),
                );


            }
        } else {

            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans("investors.:name_is_only_available_to_investors", ["name" => trans("contracts.send_an_original_to_your_home")]),
            );

        }



        echo json_encode($results);


    }

}



