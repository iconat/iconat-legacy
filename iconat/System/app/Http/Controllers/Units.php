<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;
use Carbon\Carbon;
use ImageOptimizer;

class Units extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Units'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Units'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Units'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Units'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);


            return $next($request);
        });
    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.units.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }


    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $CurrentObj = NULL;
        if ($par1 == 'add') {
            $title = trans("general.add_name_element", array("name" => trans("units.unit")));
        } else {
            $CurrentObj = \App\Models\Units::find($par1);
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && !empty($CurrentObj))) {
            return json_encode(array(
                'title' => $title,
                'content' => view('admin.units.AE')->with('par1', $par1)->with('CurrentObj', $CurrentObj)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();

        $id = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $results = array();

        if (($this->Edit && !empty(\App\Models\Units::where('id', $id)->count())) || ($this->Add && empty($id))) {

            if (!$request->has('active')) {
                $ReqData['active'] = NULL;
            } else {
                $ReqData['active'] = 1;
            }

            Validator::make($ReqData, [
                'fractions_in_original' => [
                    'integer',
                ],
            ])->validate();


            $Obj = \App\Models\Units::updateOrCreate(
                ['id' => $id],
                $ReqData
            );


            if ($Obj->wasRecentlyCreated) {
                $content = trans("general.successfully_added_new_:name", ['name' => trans("general.element")]);
                $Obj->created_by = Auth::user()->id;
                $Obj->update();

                activity("units.units")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('units.added_new_unit');

            } else {
                $content = trans("general.modified_successfully");
                $Obj->updated_by = Auth::user()->id;
                $Obj->update();

                activity("units.units")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('units.edited_unit');

            }

            if (!empty($Obj->id)) {


                $All_Langs = \Waavi\Translation\Models\Language::orderBY('id')->get();
                foreach ($All_Langs as $Lang) {
                    if (isset($ReqData[$Lang->locale])) {
                        foreach ($ReqData[$Lang->locale] as $k => $v) {
                            $ObjTrans = \App\Models\Translations\UnitsTranslations::updateOrCreate(
                                ['ref_id' => $Obj->id, 'locale' => $Lang->locale],
                                [$k => $v]
                            );
                            if ($ObjTrans->wasRecentlyCreated) {
                                $ObjTrans->created_by = Auth::user()->id;
                                $ObjTrans->update();

                            } else {
                                $ObjTrans->updated_by = Auth::user()->id;
                                $ObjTrans->update();

                            }
                        }
                    }
                }

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => $content,
                );
            }

        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }

        echo json_encode($results);


    }


    public function getdata(Request $request)
    {
        App::setLocale($request->lang);
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = \App\Models\Units::orderBy('id', 'desc');


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["name"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("name", 'LIKE', '%' . $request["name"] . '%');
                    });
                }
                if (!empty($request["fraction"])) {
                    $Objs = $Objs->whereHas('Translations', function ($query) use ($request) {
                        $query->where("fraction", 'LIKE', '%' . $request["fraction"] . '%');
                    });
                }
                if (!empty($request["fractions_in_original"])) {

                    $Objs = $Objs->where("fractions_in_original", $request["fractions_in_original"]);
                }

                if (!empty($request["date_from"]) || !empty($request["date_to"])) {
                    $date_from = isset($request["date_from"]) ? $request["date_from"] : NULL;
                    $date_to = isset($request["date_to"]) ? $request["date_to"] : NULL;
                    $newDateTo = date('Y-m-d', strtotime($date_to.' +1 day'));
                    if($date_from && $date_to){
                        $Objs = $Objs->whereBetween('created_at', [$date_from, $newDateTo]);
                    }
                    elseif ($date_from && (!$date_to)){
                        $Objs = $Objs->where('created_at',">=", $date_from);
                    }
                    elseif ((!$date_from) && $date_to){
                        $Objs = $Objs->where('created_at',"<=", $newDateTo);
                    }
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get();

            foreach ($Objs as $value) {
                $AcState = trans("general.not_active");

                if ($value->active == 1) {
                    $AcState = trans("general.active");
                }


                $buttons = '';
                if ($this->Edit) {
                    $buttons .= '<button par1="' . $value->id . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                }
                if ($this->Delete) {
                    $OTitle = $value->Get_Trans(App::getLocale(), 'name');
                    $buttons .= '<button par1="' . $value->id . '" par2="' . $OTitle . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $OTitle)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';
                }
                $created_at = new Carbon($value->created_at);

                $records["data"][] = array(
                    $value->Get_Trans(App::getLocale(), 'name'),
                    $value->Get_Trans(App::getLocale(), 'fraction'),
                    $value->fractions_in_original,
                    $AcState,
                    $created_at->toDayDateTimeString(),
                    $buttons,
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;
            $par2 = isset($ReqData['par2']) ? mb_strtolower($ReqData['par2']) : NULL;

            $Obj = $q = \App\Models\Units::where('id', '=', $id)->first();

            $q = \App\Models\Units::where('id', '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {

                activity("units.units")
                    ->performedOn($Obj)
                    ->causedBy(Auth::user()->id)
                    ->log('units.deleted_unit');

                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $par2)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



