<?php

namespace App\Http\Controllers\Website;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;

class News extends Controller
{

    public function ShowList()
    {
        $News = \App\Models\News::orderBy('id','desc')->paginate(6);
        return view('website.news.ShowAll')
            ->with('News',$News);
    }

    public function Show(\App\Models\News $post)
    {
            return view('website.news.ShowOne')
                ->with('post',$post);

    }


}
