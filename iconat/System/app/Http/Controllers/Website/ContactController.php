<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Auth;
use Cookie;
use Illuminate\Validation\Rule;
use Validator;
use Image;
use ImageOptimizer;
use Carbon\Carbon;


class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }








    public function store(Request $request)
    {
       

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
  
        $results = array();

            Validator::make($ReqData, [
               'name' => 'required',
                'email' => 'required|email',
               'subject'=> 'required',
               'message'=> 'required',
                
            ])->validate();


            if (empty($results['Errors'])) {

                $Data = array();
                

               
                if (!empty($ReqData['name'])) {
                    $Data['name'] = $ReqData['name'];
                }
                if (!empty($ReqData['email'])) {
                    $Data['email'] = $ReqData['email'];
                }
                
                if (!empty($ReqData['subject'])) {
                    $Data['subject'] = $ReqData['subject'];
                }
                if (!empty($ReqData['message'])) {
                    $Data['message'] = $ReqData['message'];
                }

                $Obj = \App\Models\Contact::create(
                    $Data
                );


                if ($Obj->wasRecentlyCreated) {
                    $redirect = true;
                $content = trans("general.:name,__thank_you_for_your_response__we_will_reply_as_soon_as_possible",['name' => $Obj->name]);


                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans($content),
                        'redirect' => $redirect,
                    );
                }

            }


    

        echo json_encode($results);


    }


        public function subscribe(Request $request)
    {
       

        $ReqData = \Purifier::clean($request->all());
        //$ReqData = $request->all();
  
        $results = array();

            Validator::make($ReqData, [
              
                'email' => 'required|email',
              
                
            ])->validate();


            if (empty($results['Errors'])) {

                $Data = array();
                

               
              
                if (!empty($ReqData['email'])) {
                    $Data['email'] = $ReqData['email'];
                }
                
             

                $Obj = \App\Models\Subscribe::create(
                    $Data
                );


                if ($Obj->wasRecentlyCreated) {
                    $redirect = true;
                $content = trans("general.:name,__thank_you_for_your_response__we_will_reply_as_soon_as_possible",['name' => $Obj->name]);


                    $results['Success'] = array(
                        'title' => trans('general.done'),
                        'content' => trans($content),
                        'redirect' => $redirect,
                    );
                }

            }


    

        echo json_encode($results);


    }


    

   

    

























}
