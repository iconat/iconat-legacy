<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use View;
use Validator;
use App;

class MyAccount extends Controller
{

    protected $thumb_width = 300;
    protected $thumb_height = 300;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {

            View::share('thumb_width', $this->thumb_width);
            View::share('thumb_height', $this->thumb_height);

            return $next($request);
        });


    }


    public function index()
    {
        if (Auth::user()) {
            if (Auth::user()->hasRole(['investor'])) {
                return view('admin.my_account.My_Profile')->with('Obj', Auth::user());
            } else {
                return view('system.errors.show_error')
                    ->with('error_no', '403')
                    ->with('error_title', trans('general.error'))
                    ->with('error_description', trans('business_fields.edit_profile_is_only_available_to_investors'));

            }
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }
    }

    public function Account()
    {

        return view('admin.my_account.My_Account')->with('Obj', Auth::user());
    }


}



