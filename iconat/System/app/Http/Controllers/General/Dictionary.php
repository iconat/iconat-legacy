<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Waavi\Translation\Models\Translation;
use Waavi\Translation\Models\Language;
use Auth;
use Image;
use View;
use Validator;
use Illuminate\Validation\Rule;
use DB;
use App;

class Dictionary extends Controller
{
    protected $Browse = FALSE;
    protected $Add = FALSE;
    protected $Edit = FALSE;
    protected $Delete = FALSE;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->Browse = Auth::user()->ability(array('admin'), array('B_Dictionary'));
            $this->Add = Auth::user()->ability(array('admin'), array('A_Dictionary'));
            $this->Edit = Auth::user()->ability(array('admin'), array('E_Dictionary'));
            $this->Delete = Auth::user()->ability(array('admin'), array('D_Dictionary'));

            View::share('Browse', $this->Browse);
            View::share('Add', $this->Add);
            View::share('Edit', $this->Edit);
            View::share('Delete', $this->Delete);

            return $next($request);
        });


    }


    public function index()
    {
        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            return view('admin.dictionary.view_all');
        } else {
            return view('system.errors.show_error')
                ->with('error_no', '403')
                ->with('error_title', trans('general.error'))
                ->with('error_description', trans('general.You do not have permission to access this content'));
        }


    }

    public function AE(Request $request)
    {
        $ReqData = $request->all();
        $par1 = isset($ReqData['par1']) ? mb_strtolower($ReqData['par1']) : NULL;

        $AllPrev = NULL;
        if ($par1 == 'add') {
            $title = trans('general.add_new_element');
        } else {
            $AllPrev = Translation::where(DB::raw('concat(`group`,".",`item`)'), '=', "$par1")->get();
            $title = trans('general.edit_element');
        }

        if (($this->Add && $par1 == 'add') || ($this->Edit && count($AllPrev) > 0)) {

            return json_encode(array(
                'title' => $title,
                'content' => view('admin.dictionary.AE')->with('par1', $par1)->with('AllPrev', $AllPrev)->render()
            ));
        } else {
            return json_encode(array(
                'title' => trans("general.error"),
                'content' => view('system.errors.show_error_modal')->with('error_description', trans("general.You do not have permission to access this content"))->render()
            ));
        }
    }

    public function store(Request $request)
    {
        App::setLocale($request->lang);

        $results = array();


        $AllLangs = Language::where('deleted_at', '=', NULL)->get();
        $Success_Add_No = 0;
        $Success_Edit_No = 0;

        $Item = mb_strtolower(str_replace(' ', '_', $ReqData = \Purifier::clean($request->input('item'))));
        $Item = mb_strtolower(str_replace('.', '_', $Item));

        foreach ($AllLangs as $Lang) {

            if (!empty($request->input('txt_' . $Lang->locale))) {
                $Ob = array(
                    'namespace' => "*",
                    'locale' => $Lang->locale,
                    'group' => $ReqData = \Purifier::clean($request->input('group')),
                    'item' => $Item,
                    'text' => !empty($ReqData = \Purifier::clean($request->input('txt_' . $Lang->locale))) ? $ReqData = \Purifier::clean($request->input('txt_' . $Lang->locale)) : NULL,
                );

                $Current = Translation::where('locale', $Lang->locale)
                    ->where("group", $request->input('group'))
                    ->where("item", $Item)
                    ->where("namespace", "*")
                    ->first();


                Validator::make($Ob, [
                    'group' => 'required',
                    'item' => [
                        'required',
                        Rule::unique('translator_translations')->where(function ($query) use ($Lang, $request, $Current, $Item) {
                            return $query->where('id', '!=', !empty($Current->id) ? $Current->id : NULL)->where('locale', $Lang->locale)->where('item', $Item)->where('group', $ReqData = \Purifier::clean($request->input('group')));
                        }),
                    ],
                ])->validate();

                $Obj = Translation::updateOrCreate(
                    ['id' => !empty($Current->id) ? $Current->id : NULL],
                    $Ob

                );

                if ($Obj->wasRecentlyCreated) {
                    $Success_Add_No++;

                } else {
                    $Success_Edit_No++;

                }
            }


        }

        if (empty($Success_Add_No) && empty($Success_Edit_No)) {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.no_item_has_been_saved_or_modified'),
            );
        } else {
            $results['Success'] = array(
                'title' => trans('general.done'),
                'content' => trans('general.items_added_and_modified', array('first_no' => $Success_Edit_No, 'second_no' => $Success_Add_No)),
                'token' => $request->input('group') . '.' . $Item,
                'results' => view('admin.dictionary.save_result')->with('Item', $request->input('group') . '.' . $Item)->render()
            );
        }


        echo json_encode($results);


    }


    public function getdata(Request $request)
    {

        App::setLocale($request->lang);

        if ($this->Browse || $this->Add || $this->Edit || $this->Delete) {
            $Objs = Translation::orderBy('item', 'desc')->orderBy("item", "desc");


            //Check if request is serach
            if (isset($request["action"]) && $request['action'] == "filter") {


                if (!empty($request["text"])) {
                    $Objs = $Objs->where("text", 'LIKE', '%' . $request["text"] . '%');


                }

                if (!empty($request["item"])) {
                    $item_search = $request["item"];
                    $Objs = $Objs->where(DB::raw('lower(concat(`group`,".",`item`))'), 'LIKE', '%' . mb_strtolower($item_search) . '%');
                }
            }


            $iTotalRecords = $Objs->count();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $Objs = $Objs->offset($iDisplayStart);
            $Objs = $Objs->limit($iDisplayLength);


            $records = array();
            $records["data"] = array();


            $Objs = $Objs->get()->groupBy(function ($item) {
                return $item->group . '.' . $item->item;
            });

            $AllLangs = Language::where('deleted_at', '=', NULL)->get();
            foreach ($Objs as $key => $value) {

                $ss = array();
                foreach ($AllLangs as $La) {
                    $ss[$La->locale] = '';
                }

                $buttons = '';
                $buttons .= '<button par1="' . $key . '" class="AEObject btn btn-sm btn-outline green"><i class="fa fa-edit"></i> ' . trans("general.edit") . '</button>';
                $buttons .= '<button par1="' . $key . '" Dtitle="' . trans("general.are_you_sure") . '" Dcontent="' . trans("general.are_you_sure_you_want_to_delete_name", array('name' => $key)) . '"  class="Delete btn btn-sm btn-outline red"><i class="fa fa-remove"></i>  ' . trans("general.delete") . '</button>';

                $data = array();

                array_push($data, '<div style="word-wrap: break-word;">' . $key . '</div>');
                foreach ($value as $cc) {
                    if(isset($ss[$cc->locale])){
                        $ss[$cc->locale] = $cc->text;
                    }
                }

                foreach ($ss as $s) {
                    array_push($data, $s);
                }

                array_push($data, $buttons);
                $records["data"][] = $data;
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);


        }
    }


    public function Delete(Request $request)
    {
        App::setLocale($request->lang);

        $results = array();
        if ($this->Delete) {
            $ReqData = $request->all();
            $id = isset($ReqData['id']) ? mb_strtolower($ReqData['id']) : NULL;


            $q = Translation::where(DB::raw('concat(`group`,".",`item`)'), '=', $id)
                ->where('deleted_at', NULL)
                ->update([
                    'deleted_at' => date("Y-m-d H:i:s"),
                    'deleted_by' => Auth::user()->id,
                ]);

            if (!empty($q)) {
                $results['Success'] = array(
                    'title' => trans('general.done'),
                    'content' => trans('general.name_successfully_deleted', array('name' => $id)),
                );
            } else {
                $results['Errors'] = array(
                    'title' => trans('general.error'),
                    'content' => trans('general.the_operation_was_not_done_correctly'),
                );
            }
        } else {
            $results['Errors'] = array(
                'title' => trans('general.error'),
                'content' => trans('general.you_do_not_have_permission_to_do_this'),
            );
        }


        echo json_encode($results);


    }

}



