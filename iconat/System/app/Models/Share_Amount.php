<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
class Share_Amount extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'invs_share_amount';

    protected $fillable = ['field_id','share_amount', 'share_number', 'share_date', 'invs_id','share_amount_need_review','share_number_need_review','share_date_need_review','field_id_need_review'];

    protected $dates = ['deleted_at'];


    public function Share_Amount()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }

    public function Investor()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }
    public function FieldNeeded()
    {
        return $this->belongsTo('App\Models\Classes', 'field_id_need_review');
    }
    public function Field()
    {
        return $this->belongsTo('App\Models\Classes', 'field_id');
    }
    public function get_State($style = NULL)
    {
        $state = '';

        if ($this->state == NULL) {
            if (!empty($style)) {
                $state .= '<span class="font-red-thunderbird">';
            }
            $state .= trans("shares.share_ineffective");
            if (!empty($style)) {
                $state .= '</span>';
            }
        }elseif ($this->state == 1) {
            if (!empty($style)) {
                $state .= '<span class="font-green-jungle">';
            }
            $state .= trans("shares.effective_share");
            if (!empty($style)) {
                $state .= '</span>';
            }
        }
        return $state;
    }


}




