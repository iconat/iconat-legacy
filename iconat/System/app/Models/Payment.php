<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Payment extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'invs_payment';
    protected $fillable = ['img', 'invs_id'];


    public function Payment()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }

    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
}




