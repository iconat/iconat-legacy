<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class GalleryTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_gallery';

    protected $fillable = ['ref_id','locale','name','text'];

    public function Gallery_Single()
    {
        return $this->belongsTo('App\Models\Gallery', 'ref_id');
    }



}







