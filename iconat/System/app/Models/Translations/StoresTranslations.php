<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class StoresTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_stores';

    protected $fillable = ['ref_id','locale','name'];

    public function Store()
    {
        return $this->belongsTo('App\Models\Stores', 'ref_id');
    }



}







