<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class CurrenciesTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_currencies';

    protected $fillable = ['ref_id','locale','name'];

    public function Currency()
    {
        return $this->belongsTo('App\Models\Currencies', 'ref_id');
    }



}







