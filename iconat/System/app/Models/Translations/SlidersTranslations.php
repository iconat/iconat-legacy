<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class SlidersTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_slider';

    protected $fillable = ['ref_id','locale','title','text'];

    public function Slide()
    {
        return $this->belongsTo('App\Models\Sliders', 'ref_id');
    }



}







