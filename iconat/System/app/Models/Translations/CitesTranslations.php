<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class CitesTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_cites';

    protected $fillable = ['ref_id','locale','name'];

    public function City()
    {
        return $this->belongsTo('App\Models\Cites', 'ref_id');
    }



}







