<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class AdsTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_ads';

    protected $fillable = ['ref_id','locale','name','text'];

    public function Ads_Single()
    {
        return $this->belongsTo('App\Models\Ads', 'ref_id');
    }



}







