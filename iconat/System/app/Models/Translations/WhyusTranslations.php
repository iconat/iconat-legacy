<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class WhyusTranslations extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'translations_whyus';

    protected $fillable = ['ref_id','locale','name','text','description'];

    public function Reason(){
        return $this->belongsTo('App\Models\Whyus', 'ref_id');
    }
}







