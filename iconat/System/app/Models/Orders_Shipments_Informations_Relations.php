<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders_Shipments_Informations_Relations extends Model
{

    public $table = 'orders_shipping_info_relations';

    protected $fillable = ['order_id', 'shipping_info_id'];


    public function Shipments_Information()
    {
        return $this->belongsTo('App\Models\Shipments_Informations', 'shipping_info_id');
    }


}







