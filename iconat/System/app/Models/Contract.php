<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
class Contract extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'invs_contract';
    protected $fillable = [ 'invs_id', 'amount',  'date_of_contract','field_id'];

    public function Investor()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }

    public function Field()
    {
        return $this->belongsTo('App\Models\Classes', 'field_id');
    }
    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
}




