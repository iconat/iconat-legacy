<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;
class Products extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'products';

    protected $fillable = ['categorie','customer_price','dealer_price','price_currency','real_price','product_code','unit'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\ProductsTranslations', 'ref_id');
    }

    public function PriceCurrency()
    {
        return $this->belongsTo('App\Models\Currencies', 'price_currency');
    }
    public function Categorie()
    {
        return $this->belongsTo('App\Models\Categories', 'categorie');
    }

    public function Unit()
    {
        return $this->belongsTo('App\Models\Units', 'unit');
    }

    /*
     * Get price of product for customer or dealer
     */
    public function Get_Price(){
        $user = Auth::user();
        if($user && $user->hasRole('dealer')){
            return $this->dealer_price;
        }
        return $this->customer_price;
    }

    /*
     * Get price of product for customer or dealer depend on Currency
     */
    public function Get_Price_Depend_Currency($Price = 0){

        if($this->price_currency != config("Default_currency_id")){
            $Price =  \danielme85\CConverter\Currency::conv($this->PriceCurrency->iso_code, config("Default_currency_Code"), $Price,  2);;
        }

        return $Price;
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'main_product_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }

    public function GetThumb()
    {
        $MediaTag = 'main_product_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getUrl():NULL;
    }
    public function AllImage($Arry_of_Tags = [])
    {
        return $this->getMedia($Arry_of_Tags);
        //return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }





}







