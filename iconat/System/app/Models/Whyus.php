<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;
class Whyus extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'whyus';

    protected $fillable = ['id','order'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\WhyusTranslations', 'ref_id');
    }



    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'about_img_';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }





}







