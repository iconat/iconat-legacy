<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Orders extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'orders';

    protected $fillable = ['delivery_method','order_no','state','total_cost','sub_total_cost'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


//    public function ShippingInformation()
//    {
//        return $this->hasOne('App\Employee','');
//    }


    public function Creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function CreatorName()
    {
     return   (!empty($this->Creator->name)) ? $this->Creator->name : trans("general.guest");
    }


    public function Status()
    {
        return $this->belongsTo('App\Models\OrdersStatus', 'state');
    }

    public function OrdersProducts()
    {
        return $this->hasMany('App\Models\Order_Products', 'order_id');
    }



    public function Shipments_Information_Rel()
    {
        return $this->hasOne('App\Models\Orders_Shipments_Informations_Relations', 'order_id');
    }

    public function Payment_Method_Relation()
    {
        return $this->hasOne('App\Models\Order_Payment_Method_Relations', 'order_id');
    }

    public function Shipment_Information($Filed)
    {
        if(isset($this->Shipments_Information_Rel)){
          return  $this->Shipments_Information_Rel->Shipments_Information->$Filed;
        }
        return NULL;

    }

    public function Payments_Information($Filed)
    {
        if(isset($this->Payment_Method_Relation)){
          return  $this->Payment_Method_Relation->Payment_Information->$Filed;
        }
        return NULL;
    }




}







