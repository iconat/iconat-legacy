<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
class Poll_Options extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'poll_options';

    protected $fillable = ['poll_id','option','additional'];

    protected $dates = ['deleted_at'];



}




