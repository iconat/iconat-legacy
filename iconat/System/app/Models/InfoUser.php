<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class InfoUser extends Authenticatable
{

    use EntrustUserTrait; // add this trait to your user model
    use Notifiable;
    use Mediable;
    use SoftDeletes;
    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = 'user_info';
    protected $fillable = [
        'shopCompanyName', 'city', 'shopAddress','shopTown','shopState','shopPostCode','shopPhone','user_id'
    ];

   

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */




   
}
