<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;

class Ads extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'ads';

    protected $fillable = ['name','active','order','text','shop_link'];


    public function Shops()
    {
        return $this->hasMany('App\Models\Shops', 'ads_single');
    }


     public function ShopsName()
    {
        return $this->belongsTo('App\Models\Shops', 'shop_link');
    }


    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\AdsTranslations', 'ref_id');
    }

        public function CanDeleted()
    {

        // if (!empty($this->Products->count())) {
        //     return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("products.products")]);
        // }
        // if (!empty($this->children->count())) {
        //     return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("categories.subcategories_one")]);
        // }


        return true;
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function viewImage()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl() : NULL;
    }
    public function MainImage()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }

    public function GetThumb()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getUrl():NULL;
    }
    public function AllImage($Arry_of_Tags = [])
    {
        return $this->getMedia($Arry_of_Tags);
        //return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }







      public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }
     public function GetImageUrlXS()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath():NULL;
    }
      public function GetImageUrlSM()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath():NULL;
    }

  public function GetImageUrlMD()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath():NULL;
    }

  public function GetImageUrlLG()
    {
        $MediaTag = 'main_ads_single_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath():NULL;
    }





}







