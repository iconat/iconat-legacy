<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Invs extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'invs';

    protected $fillable = ['firstname', 'secondname', 'thirdname', 'fourthname',  'id_country', 'phone', 'birthday', 'email', 'show_password', 'address', 'city', 'state', 'user_id', 'is_deleted', 'deleted_date', 'deleted_by', 'last_update', 'add_by', 'no_payment_image', 'no_contract_image', 'id_country_live'];


    public function Contracts()
    {
        return $this->hasMany('App\Models\Contract', 'invs_id');
    }

    public function Payments()
    {
        return $this->hasMany('App\Models\Payment', 'invs_id');
    }

    public function Identities()
    {
        return $this->hasMany('App\Models\Identity', 'invs_id');
    }

    public function Share_Amounts()
    {
        return $this->hasMany('App\Models\Share_Amount', 'invs_id');
    }

    public function Country()
    {
        return $this->belongsTo('App\Models\Countries', 'id_country');
    }

    public function CountryLive()
    {
        return $this->belongsTo('App\Models\Countries', 'id_country_live');
    }

    public function UserOwner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public function get_Short_name()
    {

        $name = $this->firstname . ' ' . $this->fourthname;


        return $name;
    }
    /**
     * Accessor for Age.
     */
    public function GetAge()
    {
        return Carbon::parse($this->birthday)->age;
    }

    public function get_Full_name()
    {

        $name = $this->firstname . ' ' . $this->secondname . ' ' . $this->thirdname . ' ' . $this->fourthname;


        return $name;
    }
    public function get_Full_Address()
    {

        $Ad = "";

        if(!empty($this->address)){
            $Ad .= $this->address;
        }
        if(!empty($this->city)){
            $Ad .= ' - '. $this->city;
        }
        if(!empty($this->state)){
            $Ad .= ' - '. $this->state;
        }
        if(!empty($this->CountryLive)){
            $Ad .= ' - '. $this->CountryLive->name_ar;
        }

        return $Ad;
    }
    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
    public function Get_total_investment()
    {
        return $this->Share_Amounts()->where("state",1)->get() ;
    }
}







