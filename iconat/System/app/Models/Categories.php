<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Categories extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'categories';

    protected $fillable = ['importance','active','parent_id'];

    /*
     * return true if user can delete this object
     */

    public function CanDeleted()
    {

        if (!empty($this->Products->count())) {
            return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("products.products")]);
        }
        if (!empty($this->children->count())) {
            return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("categories.subcategories_one")]);
        }


        return true;
    }

    /*
     * hasMany Relations
     */
    public function Products()
    {
        return $this->hasMany('App\Models\Products', 'categorie');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Categories', 'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\Categories', 'parent_id');
    }


    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\CategoriesTranslations', 'ref_id');
    }

    public function Get_Trans($lang, $attr)
    {
        $Translation = $this->Translations->where("locale", $lang)->last();
        if ($Translation) {
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'main_slider_img_thumb_' . config('col');
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl() : NULL;
    }

    public function getParentsAttribute()
    {
        $parents = collect([]);

        $parent = $this->parent;



        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents->reverse();
    }


}







