<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class PayMethods extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'pay_methods';

    protected $fillable = ['active'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\PayMethodsTranslations', 'ref_id');
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }





}







