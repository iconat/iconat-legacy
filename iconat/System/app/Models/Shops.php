<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;
class Shops extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'shops';

    protected $fillable = ['shop_categories','city','shop_code','mobile','video','facebook','instagram','snapchat','twitter','linkedin','gmail','user_id','latitude','longitude','active','order','sale'];

    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\ShopsTranslations', 'ref_id');
    }


    public function Shop_Categories()
    {
        return $this->belongsTo('App\Models\Shop_Categories', 'shop_categories');
    }
    public function Shop_City()
    {
        return $this->belongsTo('App\Models\Cites', 'city');
    }
    public function owner_shop()
    {
        return $this->belongsTo('App\User', 'user_id');
    }



    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }

    public function viewImage($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }

    public function MainImage2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }

    public function GetThumb()
    {
        $MediaTag = 'main_logo_shop_img';
        // return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getUrl():NULL;
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }
    public function AllImage($Arry_of_Tags = [])
    {
        return $this->getMedia($Arry_of_Tags);
        //return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }


      public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }
      public function GetImageUrlXS()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath():NULL;
    }
      public function GetImageUrlSM()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath():NULL;
    }

  public function GetImageUrlMD()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath():NULL;
    }

  public function GetImageUrlLG()
    {
        $MediaTag = 'main_logo_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath():NULL;
    }










    public function GetImageUrlXS2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getDiskPath() : NULL;
    }
    public function GetImageUrlSM2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('sm')->getDiskPath() : NULL;
    }

    public function GetImageUrlMD2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('md')->getDiskPath() : NULL;
    }

    public function GetImageUrlLG2()
    {
        $MediaTag = 'main_shop_img';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('lg')->getDiskPath() : NULL;
    }






}







