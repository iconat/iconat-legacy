<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Currencies extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'currencies';

    protected $fillable = ['icon','active','symbols'];

    /*
     * return true if user can delete this object
     */

    public function CanDeleted()
    {

        if(!empty($this->Products->count())){
            return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name",['name' => trans("products.products")]);
        }

        if(setting('default_currency' , "") == $this->id){
            return trans("currencies.the_system_default_currency_can_not_be_deleted");

        }

        return true;
    }

    /*
     * hasMany Relations
     */
    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\CurrenciesTranslations', 'ref_id');
    }
    public function Products()
    {
        return $this->hasMany('App\Models\Products', 'price_currency');
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }

    public function MainImage()
    {
        $MediaTag = 'main_slider_img_thumb_'.config('col');
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }




}







