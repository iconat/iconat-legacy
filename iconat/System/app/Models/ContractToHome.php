<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
class ContractToHome extends Model
{
    use SoftDeletes;
    use Mediable;
    public $table = 'invs_contract_to_home';
    protected $fillable = [ 'invs_id', 'contract_id',  'notes'];

    public function Investor()
    {
        return $this->belongsTo('App\Models\Invs', 'invs_id');
    }



    public function Contract()
    {
        return $this->belongsTo('App\Models\Contract', 'contract_id');
    }


    public function GetImageURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }
    public function GetStateTitle()
    {
        $title = trans("general.under_proccessing");

        return $title;
    }
}




