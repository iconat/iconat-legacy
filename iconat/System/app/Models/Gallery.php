<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;
use Auth;

class Gallery extends Model
{
    use SoftDeletes;
    use Mediable;

    public $table = 'gallery';

    protected $fillable = ['link_video','order','active','shop_link'];


    public function Shops()
    {
        return $this->hasMany('App\Models\Shops', 'gallery_single');
    }
    public function Translations()
    {
        return $this->hasMany('App\Models\Translations\GalleryTranslations', 'ref_id');
    }

        public function CanDeleted()
    {

        // if (!empty($this->Products->count())) {
        //     return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("products.products")]);
        // }
        // if (!empty($this->children->count())) {
        //     return trans("general.this_item_can_not_be_deleted_because_it_linked_with_:name", ['name' => trans("categories.subcategories_one")]);
        // }


        return true;
    }

    public function Get_Trans($lang,$attr)
    {
        $Translation = $this->Translations->where("locale",$lang)->last();
        if($Translation){
            return $Translation->{$attr};
        }
        return NULL;
    }



    public function viewImage($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getUrl() : NULL;
    }

    public function MainImage()
    {

        $MediaTag = 'main_gallery_img_thumb_lg';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }

    public function GetThumb()
    {
        $MediaTag = 'main_gallery_img_thumb';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia('xs')->getUrl():NULL;
    }
    public function AllImage($Arry_of_Tags = [])
    {
        return $this->getMedia($Arry_of_Tags);
        //return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->lastMedia(config('col'))->getUrl():NULL;
    }


       public function GetvideoURL($VideoTag)
    {
        $MediaTag = 'video';
    return ($this->lastMedia($VideoTag)) ? $this->lastMedia($VideoTag)->getUrl() : NULL;

    }

        public function SecondImage()
    {
        $MediaTag = 'second_gallery_img_thumb';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }
    public function SecondImage2()
    {
        $MediaTag = 'second_gallery_img_thumb2';
        return ($this->lastMedia($MediaTag)) ? $this->lastMedia($MediaTag)->getUrl():NULL;
    }










    public function GetURL($imgTag)
    {
        return ($this->lastMedia($imgTag)) ? $this->lastMedia($imgTag)->getDiskPath() : NULL;
    }






}







