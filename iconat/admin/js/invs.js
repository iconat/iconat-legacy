var InvsFunction = function () {



    var initPickers = function () {

        //init date pickers

        $('.date-picker').datepicker({

            rtl: App.isRTL(),

            autoclose: true

        });

    }



    var InvsValidation = function() {
 
        if($('#invs_form').length){


                   // for more info visit the official plugin documentation: 

            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#invs_form');

            var error1 = $('.alert-danger', form1);

            var success1 = $('.alert-success', form1);



            form1.validate({

                errorElement: 'span', //default input error message container

                errorClass: 'help-block help-block-error', // default input error message class

                focusInvalid: false, // do not focus the last invalid input

                ignore: "",  // validate all fields including form hidden input

                messages: {



                },

                rules: {

                    //  'firstname': {

                    //     minlength: 1,
                    //     maxlength: 255,
                    //     required: true,

                    // },
                    //   'secondname': {

                    //     minlength: 1,
                    //     maxlength: 255,
                    //     required: true,

                    // },
                    //  'thirdname': {

                    //     minlength: 1,
                    //     maxlength: 255,
                    //     required: true,

                    // },
                    //  'fourthname': {

                    //     minlength: 1,
                    //     maxlength: 255,
                    //     required: true,

                    // },
                    // 'profileimg':{

                    //    required: true,
                    // },
                    // 'country':{

                    //    required: true,
                    // },

                    // 'phone':{

                    //     required: true,

                    // },
                    // 'birthday':{

                    //     required: true,
                    // },
                    // 'email':{

                    //     required: true,
                    //     email: true,

                    // },
                    // 'password':{

                    //     required: true,

                    // },
                    // 'password_confirmation':{
                    //     required: true,

                    // },
                      
                    // 'address':{
                    //     required: true,
                    // },

                    // 'city':{
                    //     required: true,

                    // },
                    // 'state':{
                    //     required: true,

                    // },
                    // 'id_type':{
                    //     required: true,

                    // },
                    // 'id_number':{
                    //     required: true,

                    // },
                    // 'id_image_path':{
                    //     required: true,

                    // },
                    // 'share_amount':{
                    //     required: true,

                    // },
                    // 'share_number':{
                    //     required: true,

                    // },
                    // 'share_date':{
                    //     required: true,

                    // },
                    // 'share_image_path':{
                    //     required: true,

                    // },
               
                   


                },



                invalidHandler: function (event, validator) { //display error alert on form submit              

                    success1.hide();

                    error1.show();

                    App.scrollTo(error1, -200);

                },



                errorPlacement: function (error, element) { // render error placement for each input type

                    var cont = $(element).parent('.input-group');

                    if (cont.size() > 0) {

                        cont.after(error);

                    } else {

                        element.after(error);

                    }

                },



                highlight: function (element) { // hightlight error inputs


                    $(element)

                        .closest('.form-group').addClass('has-error'); // set error class to the control group

                    },



                unhighlight: function (element) { // revert the change done by hightlight

                    $(element)

                        .closest('.form-group').removeClass('has-error'); // set error class to the control group

                    },



                    success: function (label) {

                        label

                        .closest('.form-group').removeClass('has-error'); // set success class to the control group

                    },



                    submitHandler: function (form) {


                        success1.show();

                        error1.hide();



                //upload files

                var data = new FormData(form1[0]);

                if(jQuery('.file').length){

                    jQuery.each(jQuery('.file')[0].files, function (i, file) {

                        data.append('file-' + i, file);

                    });

                }

                data.append('id', form1.attr("par1"));

                data.append('par2', form1.attr("par2"));

                $.ajax({

                    type: "POST",

                    url: APP_URL+"/SendDataToDB",

                    data: data,

                    //async: false,

                    cache: false,

                    contentType: false,

                    processData: false,

                    dataType: "JSON",

                    beforeSend: function () {



                        App.blockUI({

                            target: '#invs_form',

                            overlayColor: 'none',

                            cenrerY: true,

                            animate: true

                        });

                    },

                    complete: function () {



                        App.unblockUI('#invs_form');

                    },
                    
                    error: function (data ,xhr, ajaxOptions, thrownError) {


                      //  console.log(data);
                      var res = data.responseJSON;
                      var errors = res.errors;
                      var eee = '';
                      $.each(errors, function (key, value) {
            //$('#' + key).parent().addClass('error');
            eee += ' * ' + value + '</br>';
        });

                      toastr['error']( res.massege + '</br>' + eee)
                      toastr.options = {
                          "closeButton": true,
                          "debug": false,
                          "positionClass": "toast-top-right",
                          "onclick": null,
                          "showDuration": "1000",
                          "hideDuration": "1000",
                          "timeOut": "5000",
                          "extendedTimeOut": "1000",
                          "showEasing": "swing",
                          "hideEasing": "linear",
                          "showMethod": "fadeIn",
                          "hideMethod": "fadeOut"
                      }


                  },

                    success: function (response)

                    {

                        if (response !== null && response.hasOwnProperty("Errors")) {

                            success3.hide();



                            console.log("errors");



                        } else if (response !== null && response.hasOwnProperty("Success")) {

                            console.log("sucsess");

                            var R = response.Success;

/////////////////////

toastr.options = {

  "closeButton": true,

  "debug": false,

  "positionClass": "toast-top-right",

  "onclick": null,

  "showDuration": "1000",

  "hideDuration": "1000",

  "timeOut": "5000",

  "extendedTimeOut": "1000",

  "showEasing": "swing",

  "hideEasing": "linear",

  "showMethod": "fadeIn",

  "hideMethod": "fadeOut",

  "progressBar": true,

}  

toastr.success(R.title, R.content);


//redirect anther page
window.location = APP_URL+"/invs";












                            }

                        }

                    });

}



});


        }

 




}










// show data in table
var CourseTable = function () {
    if($("#datatable_ajax").length){

         var grid = new Datatable();

            grid.init({

        src: $("#datatable_ajax"),

        onSuccess: function (grid, response) {

                // grid:        grid object

                // response:    json object of server side ajax response

                // execute some code after table records loaded

            },

            onError: function (grid) {

                // execute some code on network or other general error  

            },

            onDataLoad: function(grid) {

                // execute some code on ajax data load

            },

            loadingMessage: 'Loading...',

            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                "language": {

                  "url": APP_URL+"/assets/global/plugins/datatables/languages/ar.json"

              },

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout

                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 

                // So when dropdowns used the scrollable div should be removed. 

                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                

                // save datatable state(pagination, sort, etc) in cookie.

                "bStateSave": true, 



                 // save custom filters to the state

                 "fnStateSaveParams":    function ( oSettings, sValue ) {

                    $("#datatable_ajax tr.filter .form-control").each(function() {

                        sValue[$(this).attr('name')] = $(this).val();

                    });



                    return sValue;

                },



                // read the custom filters from saved state and populate the filter inputs

                "fnStateLoadParams" : function ( oSettings, oData ) {

                    //Load custom filters

                    $("#datatable_ajax tr.filter .form-control").each(function() {

                        var element = $(this);

                        if (oData[element.attr('name')]) {

                            element.val( oData[element.attr('name')] );

                        }

                    });

                    

                    return true;

                },



                "lengthMenu": [

                [10, 20, 50, 100, 150, -1],

                    [10, 20, 50, 100, 150, "All"] // change per page values here

                    ],

                    "columnDefs": [ {

                      "targets": '_all',

                      "searchable": false,

                      className: "text-center"

                  } ],

                "pageLength": 50, // default record count per page

                "ajax": {

                    "url": APP_URL+"/getinvs", // ajax source

                },

                "ordering": false,

                "order": [

                [1, "asc"]

                ]// set first column as a default sort by asc

            }

        });



        // handle group actionsubmit button click

        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {

            e.preventDefault();

            var action = $(".table-group-action-input", grid.getTableWrapper());

            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                grid.setAjaxParam("customActionType", "group_action");

                grid.setAjaxParam("customActionName", action.val());

                grid.setAjaxParam("id", grid.getSelectedRows());

                grid.getDataTable().ajax.reload();

                grid.clearAjaxParams();

            } else if (action.val() == "") {

                App.alert({

                    type: 'danger',

                    icon: 'warning',

                    message: 'Please select an action',

                    container: grid.getTableWrapper(),

                    place: 'prepend'

                });

            } else if (grid.getSelectedRowsCount() === 0) {

                App.alert({

                    type: 'danger',

                    icon: 'warning',

                    message: 'No record selected',

                    container: grid.getTableWrapper(),

                    place: 'prepend'

                });

            }

        });



        //grid.setAjaxParam("customActionType", "group_action");

        //grid.getDataTable().ajax.reload();

        //grid.clearAjaxParams();

    }

   




    }

    var delete_row = function (){





        $(document).on('click', '.delete', function () {

            var el = $(this);



            $.confirm({

                title: "هل انت متأكد؟",

                content: "ستتم عملية الحذف حاليا",

                type: 'orange',

                buttons: {

                    confirm: {

                        text: "نعم متأكد",

                        btnClass: 'btn-red',

                        action: function () {



                            console.log(el.attr("par1"));



                            $.ajax({

                                type: "POST",

                                url: APP_URL+"/Delete_Invs",

                                data: {id:el.attr("par1")},

                    //async: false,



                    dataType: "JSON",

                    beforeSend: function () {



                    },

                    complete: function () {





                    },

                    error: function (xhr, ajaxOptions, thrownError) {

                        $.alert({

                            title: "Error",

                            content: xhr.status + " - " + thrownError,

                            type: 'red',

                            rtl: App.isRTL(),

                            closeIcon: true,

                            buttons: {

                                cancel: {

                                    text: "Ok",

                                    action: function () {

                                    }

                                }

                            }

                        });

                    },

                    success: function (response)

                    {

                        if (response !== null && response.hasOwnProperty("Errors")) {

                            success3.hide();



                            console.log("errors");



                        } else if (response !== null && response.hasOwnProperty("Success")) {

                            console.log("sucsess");





                           // $('#ajax').modal('toggle'); //to close modal



                                //refresh table



                                $('#datatable_ajax').DataTable().ajax.reload();

                            }

                        }

                    });

                        }},



                        cancel: {

                            text: "الغاء الامر",





                            keys: ['enter', 'shift'],

                            action: function(){



                            }

                        }

                    }

                });





        });



    }

    return {



        //main function to initiate the module

        init: function () {



            initPickers();

            CourseTable();

           InvsValidation();

            delete_row();

        }



    };



}();



jQuery(document).ready(function() {

    InvsFunction.init();

});